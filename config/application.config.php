<?php

/*
 * Definicoes de locais
*/

try {

	$APPLICATION_ENV = getenv('APPLICATION_ENV');

	if ($APPLICATION_ENV === "desenvolvimento"){
        $environment = "desenvolvimento";
	} elseif ($APPLICATION_ENV === "producao") {
        $environment = "producao";
	} else {
		$environment = "desenvolvimento";
	}
	
	
	return array(
	    'modules' => array(
            'Admin',
	        'Servidor',
            'Base',
            'Site',
		    'DoctrineModule',
		    'DoctrineORMModule',
	        'Acl',
            'Log',
            'ZendDeveloperTools'
	    ),
	    'module_listener_options' => array(
    		'config_glob_paths' => array(
    				"config/autoload/global.php",
    				"config/autoload/{$environment}.local.php",
    		),
	        'module_paths' => array(
	            './module',
	            './vendor',
	        ),
	    ),
	);


} catch (\Exception $e){
	echo "<pre>";
	print_r($e->getMessage());
	print_r($e->getTraceAsString());
	exit;
}