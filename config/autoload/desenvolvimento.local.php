<?php
return array(
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'params' => array(
                    'host'     => 'localhost', # configurações de banco de dados de desenvolvimento
                    'port'		=> '3306',
					'user'		=> 'root',
					'password'	=> '',
					'dbname'	=> 'silva_fundacoes',
					'driverOptions'	=> array(
							PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"
					)
                )
            ),
        ),
    	'configuration' => array(
    		'orm_default' => array(
    				'proxy_dir' => 'data/DoctrineORMModule/Proxy',
    				'proxy_namespace' => 'DoctrineORMModule\Proxy',
    		)
    	)
    )
);