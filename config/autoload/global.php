<?php
return array(
    'module_layouts' => array(
        'Admin'   => 'layout/admin',
        'Site'    => 'layout/layout'
    ),
    'mail' => array(
    	'name' => 'mail.discoverytecnologia.com.br',
    	'host' => 'srv96.prodns.com.br',
    	'port' => 587,
    	'connection_class' => 'login',
    	'connection_config' => array(
    		'username' => 'contato@discoverytecnologia.com.br',
    		'password' => 'dfvb1q2w3e@',
    		'ssl' => 'tls',
    		'from' => 'contato@discoverytecnologia.com.br'
    	)
    ),
	'session' => array(
        'config' => array(
            'class' => 'Zend\Session\Config\SessionConfig',
            'options' => array(
                'name' => 'myzf2app',
            ),
        ),
        'storage' => 'Zend\Session\Storage\SessionArrayStorage',
        'validators' => array(
            'Zend\Session\Validator\RemoteAddr',
            'Zend\Session\Validator\HttpUserAgent',
        )
	),
	'navigation' => array(
        'menu_admin' => array(
            'inicio' => array(
                    'label' => 'Início',
                    'class' => 'fa fa-home',
                    'route' => 'admin-index',
                    'action' => 'index'
            ),
            'empresa' => array(
                    'label' => 'Empresa',
                    'class' => 'fa fa-edit',
                    'route' => 'admin-empresa',
                    'action' => 'edit'
            ),
            'parceiro' => array(
                    'label' => 'Cliente/Parceiro',
                    'class' => 'fa fa-edit',
                    'route' => 'admin-parceiro',
                    'action' => 'index'
            ),
            'servico' => array(
                    'label' => 'Servico',
                    'class' => 'fa fa-edit',
                    'route' => 'admin-servico',
                    'action' => 'index'
            ),
            'portfolio' => array(
                    'label' => 'Portfólio',
                    'class' => 'fa fa-edit',
                    'route' => 'admin-portfolio',
                    'action' => 'index'
            ),
            'suporte' => array(
                    'label' => 'Suporte',
                    'class' => 'fa fa-envelope',
                    'route' => 'admin-suporte',
                    'action' => 'index'
            ),
            'usuarios' => array(
                    'label' => 'Usuários',
                    'class' => 'fa fa-users',
                    'route' => 'admin-usuario',
                    'action' => 'index'
            )/*,
            'regra' => array(
                    'label' => 'Regra',
                    'class' => 'icon-user',
                    'route' => 'admin-regra',
                    'action' => 'index'
            ),
            'recurso' => array(
                    'label' => 'Recurso',
                    'class' => 'icon-user',
                    'route' => 'admin-recurso',
                    'action' => 'index'
            ),
            'previlegio' => array(
                    'label' => 'Previlégio',
                    'class' => 'icon-user',
                    'route' => 'admin-previlegio',
                    'action' => 'index'
            ),*/
        )
	),
);
