<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ACL for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Acl;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\MvcEvent;
use Acl\Auth\AuthAdapter;
use Zend\Session\Container;

class Module implements AutoloaderProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
		    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * @method onBootstrap()
     * @param MvcEvent $e
     */
    public function onBootstrap( MvcEvent $e ) {

    	$eventManager = $e->getApplication()
    	                  ->getEventManager()
    	                  ->getSharedManager()
    	                  ->attach('Zend\Mvc\Controller\AbstractActionController', 
    	                           'dispatch', function( $e ){

    		$auth = new AuthAdapter( $e );
    		$auth->processAuthentication();
    		
    	}, 100);
    	
    	$this->bootstrapSession($e);
    }
    
    
    
    /**
     * Configura a session
     * @param unknown $e
     */
    public function bootstrapSession($e) {
    
    	$session = $e->getApplication()->getServiceManager()->get('Zend\Session\SessionManager');
    	$session->start();
    
    	$container = new Container('initialized');
    	if (!isset($container->init)) {
    		$session->regenerateId(true);
    		$container->init = 1;
    	}
    }
   
}
