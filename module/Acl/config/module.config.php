<?php
use Acl\View\Helper\UsuarioIdentity;

return array(
    'acl' => array(
        'default' => array(
            'role' => 'Visitante'
        ),
        'modules' => array(
            'admin',
            'acl'
        )
    ),
    'controllers' => array(
        'factories' => array(
            'Acl\Controller\Login' => 'Acl\Controller\Factory\LoginControllerFactory'
        )
    ),
    'router' => array(
        'routes' => array(
            'acl-login' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/acesso',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Acl\Controller',
                        'controller'    => 'Login',
                        'action'        => 'login',
                    ),
                ),
            ),
            
            'acl-admin' => array(
            		'type'    => 'Literal',
            		'options' => array(
            				'route'    => '/admin',
            				'defaults' => array(
            						'__NAMESPACE__' => 'Acl\Controller',
            						'controller'    => 'Login',
            						'action'        => 'login',
            				),
            		),
            ),
            
            'acl-logout' => array(
            		'type' => 'Literal',
            		'options' => array(
            				'route' => '/acesso/logout',
            				'defaults' => array(
            				        '__NAMESPACE__' => 'Acl\Controller',
            						'controller'	=> 'Acl\Controller\Login',
            						'action'		=> 'logout'
            				)
            		)
            ),
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'template_map' => array(
        		'layout/login'         	=> __DIR__ . '/../view/layout/login.phtml',
        		'acl/index/index'   	=> __DIR__ . '/../view/control/index/index.phtml'
        ),
        'template_path_stack' => array(
            'Acl' => __DIR__ . '/../view',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            # USUARIO LOGADO ###########################################################
            'Acl\UsuarioLogado' => 'Acl\Service\Factory\UsuarioLogadoServiceFactory',
            # LOJA #####################################################################
            'Acl\Service\Loja' => 'Acl\Service\Factory\LojaServiceFactory',
            # REGRA ####################################################################
            'Acl\Service\Permissao' => 'Acl\Service\Factory\PermissaoServiceFactory',
            # ACL AUTENTICAÇÃO #########################################################
            'Acl\Auth\Adapter' => 'Acl\Service\Factory\AdapterServiceFactory',
            # ACL LOAD #################################################################
            'Acl\Permissions' => 'Acl\Service\Factory\AclServiceFactory',
            # SESSION MANAGER ##########################################################
            'Zend\Session\SessionManager' => 'Acl\Service\Factory\SessionManagerServiceFactory'
        )
    ),
    'view_helpers' => array(
        'invokables' => array(
            'UsuarioIdentity'   => new UsuarioIdentity()
        )
    )
);
