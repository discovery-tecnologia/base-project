<?php
namespace Acl\Auth;

use Doctrine\ORM\EntityManager;
use Servidor\Entity\Entities;
use Servidor\Entity\Loja;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Result;
use Zend\Authentication\Storage\Session as SessionStorage;


class Adapter implements AdapterInterface {
	
	/**
	 * 
	 * @var EntityManager $em
	 */
	protected $em;
	
	/**
	 * 
	 * @var String $email
	 */
	protected $email;
	
	/**
	 * 
	 * @var String $senha;
	 */
	protected $senha;

    /**
     * @var Loja $loja
     */
    protected $loja;

    /**
     * @var string $entity
     */
    protected $entity;
	
	/**
	 * @method __construct()
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em = null) {
		$this->em = $em;
	}
	
	
	/**
	 * @method getEmail()
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @method getSenha()
	 * @return string
	 */
	public function getSenha() {
		return $this->senha;
	}

    /**
     * @method getLoja()
     * @return Loja
     */
    public function getLoja() {
        return $this->loja;
    }

    /**
     * @method getEntity()
     * @return string
     */
    public function getEntity() {
        return $this->entity;
    }
		
	/**
	 * @method setEmail()
	 * @param String $email
	 */
	public function setEmail($email) {
		$this->email = $email;
	}

	
	/**
	 * @method setSenha()
	 * @param String $senha
	 */
	public function setSenha($senha) {
		$this->senha = $senha;
	}

    /**
     * @method setLoja()
     * @param Loja $loja
     */
    public function setLoja(Loja $loja) {
        $this->loja = $loja;
    }

    /**
     * @method setEntity()
     * @param String $entity
     */
    public function setEntity($entity) {
        $this->entity = $entity;
    }

    /**
     * @see \Zend\Authentication\Adapter\AdapterInterface::authenticate()
     */
    public function authenticate() {

        try {

            switch($this->entity) {

                case Entities::ENTITY_USUARIO:

                    $repository = $this->em->getRepository( $this->entity );
                    return $repository->findByEmailAndSenha($this->getEmail(), $this->getSenha());
                    break;

                default:
                    throw new \Exception('Não foi possível encontrar a entidade para efetuar o Login');
            }

        }catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    /**
	 * @return int
	 */
	public function getUserId() {
		
		$sessionStorage = new SessionStorage(AuthAdapter::ACL_CONTAINER);
		$authService = new AuthenticationService();
		$authService->setStorage($sessionStorage);
		
		if($authService->hasIdentity())
			return $authService->getIdentity()->getId();
		
	}

}