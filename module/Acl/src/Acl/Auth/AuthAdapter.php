<?php
namespace Acl\Auth;

use Acl\Controller\Interfaces\LoginAwareInterface;
use Servidor\Interfaces\LoggedAwareInterface;
use Servidor\Entity\Loja;
use Servidor\Entity\Usuario;
use Zend\Mvc\MvcEvent;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use Zend\ServiceManager\ServiceManager;
use Zend\Session\Container;

/**
 * 
 * @author DiegoWagner
 *
 */
class AuthAdapter {
    
    const ACL_CONTAINER  = 'ACL_SESSION';
    const SITE_CONTAINER = 'SITE_SESSION';

    const ROLE_DEFAULT   = 'visitante';

    const ACL_LOGIN      = 'acl-login';

    /**
     * 
     * @var MvcEvent
     */
    private $mvcEvent;

    /**
     * @var ServiceManager
     */
    private $serviceManager;
    
    
    public function __construct( MvcEvent $e ) {    
        $this->mvcEvent = $e;
        $this->serviceManager = $e->getApplication()->getServiceManager();
    }
    
    
    /**
     * Método por processar toda vez em que o evento DISPATCH
     * for executado pra ver se o usuário está logado ou não
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function processAuthentication() {
        
        $controller = $this->mvcEvent->getTarget();

        $controllerClass = get_class( $controller );
        $moduleName = strtolower( substr( $controllerClass, 0, strpos( $controllerClass, '\\' ) ) );

        if (!$controller instanceof LoginAwareInterface) {
            $this->processModules($moduleName);
        }
    }

    /**
     * Processa a autenticação e autorização dos módulos
     *
     * @method processModules($moduleName)
     * @param string $moduleName
     * @return \Zend\Stdlib\ResponseInterface
     * @access private
     * @throws \Exception
     */
    private function processModules($moduleName)
    {
        $config = $this->serviceManager->get('Config');
        if (! isset($config['acl']))
            throw new \Exception('Configurações da ACL não foram encontradas');

        $authentication = new AuthenticationService();
        $authentication->setStorage(new SessionStorage(self::ACL_CONTAINER));

        if ( in_array($moduleName, $config['acl']['modules'])) {

            if ( !$authentication->hasIdentity() ) {
                return $this->processResponse(self::ACL_LOGIN);
            }

            if ( ($authentication->getIdentity()->getRegra()->isMaster() && $moduleName == 'admin')) {
                return $this->processResponse(self::ACL_LOGIN);
            }

            if (!$this->isGranted($authentication->getIdentity())) {
                return $this->mvcEvent->getTarget()->redirect()->toRoute($moduleName . '-forbidden-index');
            }

            return true;
        }
    }

    /**
     * Processa a requisição feita pelo usuário
     * e faz as devidas validações de acesso
     *
     * @method isGranted(LoggedAwareInterface $user)
     * @param LoggedAwareInterface $user
     * @return Boolean
     * @access public
     * @throws \Exception
     */
    private function isGranted( LoggedAwareInterface $user ) {

        if (!($user instanceof LoggedAwareInterface))
            throw new \Exception("Não foi possível recuperar a instância do usuário");

        $acl = $this->serviceManager->get('Acl\Permissions');
        $controllerName = $this->mvcEvent->getTarget()->getEvent()->getRouteMatch()->getParam('controller');
        $controllerName = strtolower(str_replace('\\', '', $controllerName));
        $actionName = $this->mvcEvent->getTarget()->getEvent()->getRouteMatch()->getParam('action');

        $roleUser = $user->getRegra() ? $user->getRegra()->getNome() : self::ROLE_DEFAULT;
        //var_dump($roleUser .' - '. $controllerName .' - '. $actionName);die;
        return $acl->isAllowed( $roleUser, $controllerName, $actionName );
    }


    /**
     * @param $route
     * @return \Zend\Stdlib\ResponseInterface
     */
    private function processResponse($route) {
        
        $url = $this->mvcEvent->getRouter()->assemble(array(), array( 'name' => $route ) );
        $response = $this->mvcEvent->getResponse();
        $response->getHeaders()->addHeaderLine('Location', $url);
        $response->setStatusCode( 403 );
        $response->sendHeaders();
        
        return $response;
    }
}