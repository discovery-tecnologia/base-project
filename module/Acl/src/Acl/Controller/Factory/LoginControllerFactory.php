<?php
/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 05/04/14
 * Time: 16:36
 */

namespace Acl\Controller\Factory;

use Acl\Controller\LoginController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LoginControllerFactory implements FactoryInterface {

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocatorInstance = $serviceLocator->getServiceLocator();
        $lojaService = $serviceLocatorInstance->get('Acl\Service\Loja');
        $permissaoService = $serviceLocatorInstance->get('Acl\Service\Permissao');

        return new LoginController($lojaService, $permissaoService);
    }
}