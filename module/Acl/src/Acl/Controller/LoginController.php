<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Acl for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Acl\Controller;

use Acl\Controller\Interfaces\LoginAwareInterface;
use Acl\Service\LojaService;
use Acl\Service\PermissaoService;
use Base\Enum\FlashMessages;
use Zend\View\Model\ViewModel;

use Servidor\Entity\Entities;

use Zend\Authentication\AuthenticationService,
    Zend\Authentication\Storage\Session as SessionStorage;

use Acl\Form\Login as LoginForm;
use Base\Controller\AbstractCrudController;
use Acl\Auth\AuthAdapter;


class LoginController extends AbstractCrudController implements LoginAwareInterface
{

    /**
     * @var LojaService
     */
    private $lojaService;


    /**
     * @var PermissaoService
     */
    private $permissaoService;


    public function __construct(LojaService $lojaService, PermissaoService $permissaoService) {

        $this->lojaService = $lojaService;
        $this->permissaoService = $permissaoService;
    }

    
    /**
     * @method loginAction
     * Responsavel por gerenciar login de usuarios
     * @return \Zend\View\Model\ViewModel
     */
    public function loginAction() {
    
    	try {
    			
    		$error = false;
    		$form = new LoginForm();

    		$request = $this->getRequest();

    		if($request->isPost()) {
    
    			$form->setData($request->getPost());
    			
    			if($form->isValid()) {
    					
    				$data = $request->getPost()->toArray();

    				// sessão que grava a autenticação
    				$sessionStorage = new SessionStorage(AuthAdapter::ACL_CONTAINER);
    				$auth = new AuthenticationService();
    				$auth->setStorage($sessionStorage);

    				$authAdapter = $this->getServiceLocator()->get('Acl\Auth\Adapter');
                    $authAdapter->setEntity(Entities::ENTITY_USUARIO);
    				$authAdapter->setEmail($data['email']);
    				$authAdapter->setSenha($data['senha']);

    				$result = $auth->authenticate($authAdapter);

    				if($result->isValid()) {

    					$user = $auth->getIdentity();
    					// escreve os dados na sessão
    					$sessionStorage->write( $user );
    					return $this->redirect()->toRoute('admin-index');
    				}
                    else
    					$this->flashMessenger()->addWarningMessage($result->getMessages()[0]);
    					
    			}
    
    		}

    		return new ViewModel(array(
    				'form' => $form,
    				'error'=> $error
    		)
    		);
    
    	} catch( \Exception $e ) {
    		$this->flashMessenger()->addMessage($e->getMessage());
    	}
    }
    
    
    /**
     * @method logoutAction
     * Responsavel por gerenciar logout's de usuarios
     */
    public function logoutAction() {
    
    	$auth = new AuthenticationService();
    	$auth->setStorage(new SessionStorage(AuthAdapter::ACL_CONTAINER));
    	$auth->clearIdentity();
    
    	return $this->redirect()->toRoute('acl-login');
    
    }
}
