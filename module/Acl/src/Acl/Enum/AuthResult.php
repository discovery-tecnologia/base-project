<?php
/**
 * Class AuthResult
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Acl\Enum;

use Zend\Authentication\Result;

/**
 * Esta classe não pode ser abstrata.
 *
 * Class AuthResult
 * @package Acl\Enum
 */
class AuthResult extends Result
{

    const FAILURE_IDENTITY_STATUS = -4;


    const FAILURE_DATA = -5;
}