<?php
namespace Acl\Form\InputFilter;

use Zend\InputFilter\InputFilter;

class LoginFilter extends InputFilter {
	
	public function __construct() {
		
		$this->add(array(
			'name' => 'email',
			'required' => true,
			'filters' => array(
				array('name' => 'StripTags'),
				array('name' => 'StringTrim'),
			),
			'validators' => array(
				array(
                    'name' => 'NotEmpty'
			    )
			)
		));
		
		$this->add(array(
			'name' => 'senha',
			'required' => true,
			'filters' => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
			),
			'validators' => array(
					array(
                        'name' => 'NotEmpty'
			        )
			)
		));
		
	}
	
}