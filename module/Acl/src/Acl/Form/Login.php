<?php
namespace Acl\Form;

use Zend\Form\Element\Password;
use Zend\Form\Element\Text;
use Zend\Form\Form;
use Acl\Form\InputFilter\LoginFilter;

class Login extends Form
{
	public function __construct($name = null, $options = array())
    {
		parent::__construct('login', $options);
		
		$this->setInputFilter(new LoginFilter());

        $this->setAttributes(array(
            'method' => 'post',
            'class'  => 'form'
        ));

        // email *******************EMAIL (TEXT) **************************
        $email = new Text('email');
        $email->setAttributes(
            array(
                'class'			=> 'form-control',
                'data-placement'=> 'left',
                'placeholder'	=> 'Email',
                'title'			=> 'Email cadastrado com a discovery tecnologia.',
                'id'            => 'email',
                'maxlength'		=> '255'
            )
        );
        $this->add($email);

        // senha ****************SENHA (PASSWORD) *************************
        $senha = new Password('senha');
        $senha->setAttributes(
            array(
                'class'			=> 'form-control',
                'data-placement'=> 'left',
                'placeholder'	=> 'Senha',
                'title'			=> 'Sua senha',
                'id'            => 'senha',
                'maxlength'		=> '50'
            )
        );
        $this->add($senha);

        // submit ****************SUBMIT (INPUT) **************************
		$this->add(
            array(
				'name' => 'submit',
				'type' => 'Zend\Form\Element\Submit',
				'attributes' => array(
					'value' => 'Entrar',
					'class' => 'btn bg-olive btn-block'
                )
            )
        );
		
	}
}
