<?php
namespace Acl\Permissions;

use Zend\Permissions\Acl\Acl as ZendAcl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;

class Acl extends ZendAcl {
	
	/**
	 * Regras
	 * @var Regra
	 */
	protected $roles;
	
	
	/**
	 * Recursos
	 * @var Recurso
	 */
	protected $resources;
	
	
	/**
	 * Previlegios
	 * @var Previlegio
	 */
	protected $privileges;
	
	
	public function __construct( array $roles, array $resources, array $privileges ) {
		
		$this->roles = $roles;
		$this->resources = $resources;
		$this->privileges = $privileges;
		
		$this->loadRoles();
		$this->loadResources();
		$this->loadPrivileges();
	}
	
	
	/**
	 * Carrega as Regras
	 * @method loadRole()
	 */
	protected function loadRoles() {
		
		foreach ( $this->roles as $role ) {
			
			if ( $role->getParent() )
				$this->addRole( new Role( $role->getNome() ), new Role( $role->getParent()->getNome() ) );
			else 
				$this->addRole( new Role( $role->getNome() ) );
			
			if ( $role->isMaster() )
				$this->allow( $role->getNome(), array(), array() );
		}
	}
	
	
	/**
	 * Carrega os Recursos
	 * @method loadResources()
	 */
	protected function loadResources() {
	
		foreach ( $this->resources as $resource ) {
				
			$this->addResource( new Resource( $resource->getNome() ) );
		}
	}
	
	
	
	/**
	 * Carrega os Previlegios
	 * @method loadPrivileges()
	 */
	protected function loadPrivileges() {

		foreach ( $this->privileges as $privilege ) {

			$this->allow(
					$privilege->getRegra()->getNome(), 
					$privilege->getRecurso()->getNome(),
                    $privilege->getNome()
	        );
		}

		$this->allow(array(), array(), "populate" );
        $this->allow(array(), "admincontrollerforbidden", array() );
		
	}
	
	
}