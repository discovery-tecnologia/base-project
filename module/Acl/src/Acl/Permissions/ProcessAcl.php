<?php
namespace Control\Permissions;

use Admin\Entity\Usuario;

use Zend\Mvc\MvcEvent;

class ProcessAcl {


	
	private $event;
	
	public function __construct( MvcEvent $e ) {
		
		$this->event = $e;
	}
	
	
	/**
	 * Processa a requisição feita pelo usuário
	 * e faz as devidas validações de acesso
	 * @param Usuario $user
	 */
	public function process( Usuario $user ) {

		$sm  = $this->event->getApplication()->getServiceManager();
    	$acl = $sm->get('Acl\Permissions\Acl');
    	$controllerName = $this->event->getTarget()->getEvent()->getRouteMatch()->getParam('controller');
    	$actionName = $this->event->getTarget()->getEvent()->getRouteMatch()->getParam('action');


        $roleUser = $user->getRegra() ? $user->getRegra()->getNome() : self::ROLE_DEFAULT;

    	if ( !$acl->isAllowed( $roleUser, $controllerName, $actionName ) )
	    	$this->event->getTarget()->redirect()->toRoute('control-forbidden-index');

     }
}