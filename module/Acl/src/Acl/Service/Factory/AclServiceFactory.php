<?php
namespace Acl\Service\Factory;

use Servidor\Entity\Entities;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Acl\Permissions\Acl;

/**
 * Class Factory
 * @author DiegoWagner
 *
 */
class AclServiceFactory implements FactoryInterface {
	
	
	/**
	 * (non-PHPdoc)
	 * @see \Zend\ServiceManager\FactoryInterface::createService()
	 */
	public function createService( ServiceLocatorInterface $serviceLocator ) {
		
		$em = $serviceLocator->get('Doctrine\ORM\EntityManager');
					
		$repRoles = $em->getRepository(Entities::ENTITY_REGRA);
		$roles = $repRoles->findAll();
		
		$repResources = $em->getRepository(Entities::ENTITY_RECURSO);
		$resources = $repResources->findAll();
		
		$repPrivileges = $em->getRepository(Entities::ENTITY_PREVILEGIO);
		$privileges = $repPrivileges->findAll();
		
		return new Acl($roles, $resources, $privileges);
	}
}