<?php
namespace Acl\Service\Factory;

use Acl\Auth\Adapter;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class Factory
 * @author DiegoWagner
 *
 */
class AdapterServiceFactory implements FactoryInterface {
	
	
	/**
	 * (non-PHPdoc)
	 * @see \Zend\ServiceManager\FactoryInterface::createService()
	 */
	public function createService( ServiceLocatorInterface $serviceLocator ) {
		
		return new Adapter( $serviceLocator->get('Doctrine\ORM\EntityManager') );
	}
}