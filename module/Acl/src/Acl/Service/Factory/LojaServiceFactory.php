<?php
/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 05/04/14
 * Time: 16:30
 */

namespace Acl\Service\Factory;


use Acl\Service\LojaService;
use Servidor\Entity\Entities;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LojaServiceFactory implements FactoryInterface {

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $objectRepository = $objectManager->getRepository(Entities::ENTITY_EMPRESA);

        return new LojaService( $objectManager, $objectRepository );
    }
}