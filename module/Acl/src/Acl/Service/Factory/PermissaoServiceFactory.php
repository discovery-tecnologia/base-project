<?php
/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 05/04/14
 * Time: 16:30
 */

namespace Acl\Service\Factory;


use Acl\Service\PermissaoService;
use Servidor\Entity\Entities;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PermissaoServiceFactory implements FactoryInterface {

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $objectRepository = $objectManager->getRepository(Entities::ENTITY_REGRA);

        return new PermissaoService( $objectManager, $objectRepository );
    }
}