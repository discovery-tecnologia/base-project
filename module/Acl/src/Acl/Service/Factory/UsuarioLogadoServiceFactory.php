<?php
/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 19/03/14
 * Time: 21:28
 */

namespace Acl\Service\Factory;

use Servidor\Entity\Entities;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Authentication\AuthenticationService,
    Zend\Authentication\Storage\Session as SessionStorage;
use Acl\Auth\AuthAdapter;

class UsuarioLogadoServiceFactory implements FactoryInterface {


    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $sessionStorage = new SessionStorage( AuthAdapter::ACL_CONTAINER );
        $authService = new AuthenticationService();
        $authService->setStorage($sessionStorage);

        if($authService->hasIdentity()) {
            $em = $serviceLocator->get('Doctrine\ORM\EntityManager');
            $usuario = $em->getReference( Entities::ENTITY_USUARIO, $authService->getIdentity()->getId() );

            if ( !$usuario )
                throw new \Exception('O usuário não pode ser encontrado, talvez não esteja logado');

            return $usuario;

        } else
            return null;
    }
}