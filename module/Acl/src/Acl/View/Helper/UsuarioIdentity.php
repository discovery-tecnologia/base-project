<?php
namespace Acl\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Authentication\AuthenticationService,
	Zend\Authentication\Storage\Session as SessionStorage;
use Acl\Auth\AuthAdapter;

class UsuarioIdentity extends AbstractHelper {

	
	public function __invoke() {
		
		$sessionStorage = new SessionStorage( AuthAdapter::ACL_CONTAINER );
		$authService = new AuthenticationService();
		$authService->setStorage($sessionStorage);
		
		if($authService->hasIdentity())
			return $authService->getIdentity();
		else
			return false;
	}
}