<?php
namespace Admin;

use Acl\Auth\AuthAdapter;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\ModuleRouteListener;

use Zend\Mvc\MvcEvent,
    Zend\Authentication\AuthenticationService,
	Zend\Authentication\Storage\Session as SessionStorage;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;


class Module implements
    AutoloaderProviderInterface,
    ConfigProviderInterface {

	
	/**
	 * Retorna o arquivo de configuração do módulo
	 */
    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    
    /**
     * Configurações  de Autoloader
     * @return multitype:multitype:multitype:string
     */
    public function getAutoloaderConfig() {

        return array(
        		'Zend\Loader\ClassMapAutoloader' => array(
        				__DIR__ . '/autoload_classmap.php',
        		),
        		'Zend\Loader\StandardAutoloader' => array(
        				'namespaces' => array(
        		    // if we're in a namespace deeper than one level we need to fix the \ in the path
        						__NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
        				),
        		),
        );
    }


    /**
     * método init do módulo (inicialização do módulo)
     * @param ModuleManager $moduleManager
     */
    public function init( ModuleManager $moduleManager ) {

        $sharedEvents = $moduleManager->getEventManager()->getSharedManager();

        // responsável por injetar no layout o formulário de pesquisa
        $sharedEvents->attach(__NAMESPACE__,
            array( MvcEvent::EVENT_DISPATCH ),
            function(MvcEvent $e){

                $auth = new AuthAdapter( $e );
                $auth->processAuthentication();

            }, 2);
    }

}
