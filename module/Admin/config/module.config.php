<?php
namespace Admin;

return array(

    /** ROUTES MANAGER **/
    'router' => array(
        'routes' => array(
        		
            ##########################################################################################
            # -- INDEX
            'admin-index' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/admin/inicio',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller' => 'Admin\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'admin-index-gerar-entity' => array(
                    'type' => 'Zend\Mvc\Router\Http\Literal',
                    'options' => array(
                            'route'    => '/admin/gerar/entity',
                            'defaults' => array(
                                    '__NAMESPACE__' => 'Admin\Controller',
                                    'controller' => 'Admin\Controller\Index',
                                    'action'     => 'gerar-entity',
                            ),
                    ),
            ),

            // ########################################################################################
            // -- FORBIDDEN
            'admin-forbidden-index' => array (
                    'type' => 'Zend\Mvc\Router\Http\Literal',
                    'options' => array (
                            'route' => '/admin/not-allowed',
                            'defaults' => array (
                                    'controller' => 'Admin\Controller\Forbidden',
                                    'action'     => 'index'
                            )
                    )
            ),
            'admin-forbidden' => array(
                    'type' => 'Zend\Mvc\Router\Http\Literal',
                    'options' => array(
                            'route'    => '/admin/forbidden',
                            'defaults' => array(
                                    'controller' => 'Admin\Controller\Forbidden',
                                    'action'     => 'forbidden',
                            ),
                    ),
            ),


            ##########################################################################################
            # -- EMPRESA
            'admin-empresa' => array (
                    'type' => 'Literal',
                    'options' => array (
                            'route' => '/admin/empresa',
                            'defaults' => array (
                                    'controller' => 'Admin\Controller\Empresa',
                                    'action' => 'edit'
                            )
                    ),
                    'may_terminate' => true,
                    'child_routes' => array (
                            'default' => array (
                                    'type' => 'Segment',
                                    'options' => array (
                                            'route' => '[/:action[/:id]]',
                                            'constraints' => array (
                                                    'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                    'defaults' => array (
                                                            'controller' => 'Empresa'
                                                    )
                                            )
                                    )
                            )
                    )
            ),


            ##########################################################################################
            # -- PARCEIRO
            'admin-parceiro' => array (
                'type' => 'Literal',
                'options' => array (
                    'route' => '/admin/parceiro',
                    'defaults' => array (
                        'controller' => 'Admin\Controller\Parceiro',
                        'action' => 'index'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array (
                    'default' => array (
                        'type' => 'Segment',
                        'options' => array (
                            'route' => '[/:action[/:id]]',
                            'constraints' => array (
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'defaults' => array (
                                    'controller' => 'Parceiro'
                                )
                            )
                        )
                    )
                )
            ),

            ########################################################################################
            // -- SERVICO
            'admin-servico' => array (
                'type' => 'Literal',
                'options' => array (
                    'route' => '/admin/servico',
                    'defaults' => array (
                        'controller' => 'Admin\Controller\Servico',
                        'action' => 'index'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array (
                    'default' => array (
                        'type' => 'Segment',
                        'options' => array (
                            'route' => '[/:action[/:id]]',
                            'constraints' => array (
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'pet' => '\d+',
                                'defaults' => array (
                                        'controller' => 'Servico'
                                )
                            )
                        )
                    )
                )
            ),

            ########################################################################################
            // -- PORTFOLIO
            'admin-portfolio' => array (
                'type' => 'Literal',
                'options' => array (
                    'route' => '/admin/portfolio',
                    'defaults' => array (
                        'controller' => 'Admin\Controller\Portfolio',
                        'action' => 'index'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array (
                    'default' => array (
                        'type' => 'Segment',
                        'options' => array (
                            'route' => '[/:action[/:id]]',
                            'constraints' => array (
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'defaults' => array (
                                    'controller' => 'Portfolio'
                                )
                            )
                        )
                    )
                )
            ),

            ########################################################################################
            // -- SUPORTE
            'admin-suporte' => array (
                    'type' => 'Literal',
                    'options' => array (
                            'route' => '/admin/suporte',
                            'defaults' => array (
                                    'controller' => 'Admin\Controller\Suporte',
                                    'action' => 'index'
                            )
                    ),
                    'may_terminate' => true,
                    'child_routes' => array (
                            'default' => array (
                                    'type' => 'Segment',
                                    'options' => array (
                                            'route' => '[/:action]', // quando on new o id é o do cliente e nao do pet
                                            'constraints' => array (
                                                    'defaults' => array (
                                                            'controller' => 'Suporte'
                                                    )
                                            )
                                    )
                            )
                    )
            ),


            ########################################################################################
            // -- PREVILEGIO
            'admin-previlegio' => array (
                    'type' => 'Literal',
                    'options' => array (
                            'route' => '/admin/previlegio',
                            'defaults' => array (
                                    'controller' => 'Admin\Controller\Previlegio',
                                    'action' => 'index'
                            )
                    ),
                    'may_terminate' => true,
                    'child_routes' => array (
                            'default' => array (
                                    'type' => 'Segment',
                                    'options' => array (
                                            'route' => '[/:action[/:id]]',
                                            'constraints' => array (
                                                    'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                    'defaults' => array (
                                                            'controller' => 'Previlegio'
                                                    )
                                            )
                                    )
                            )
                    )
            ),


            ##########################################################################################
            # -- RECURSO
            'admin-recurso' => array (
                    'type' => 'Literal',
                    'options' => array (
                            'route' => '/admin/recurso',
                                    'defaults' => array (
                                            'controller' => 'Admin\Controller\Recurso',
                                            'action' => 'index'
                                    )
                            ),
                    'may_terminate' => true,
                    'child_routes' => array (
                            'default' => array (
                                    'type' => 'Segment',
                                    'options' => array (
                                            'route' => '[/:action[/:id][/:upload]]',
                                            'constraints' => array (
                                                    'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                    'defaults' => array (
                                                            'controller' => 'Recurso'
                                                    )
                                            )
                                    )
                            )
                    )
            ),


            ##########################################################################################
            # -- REGRA
            'admin-regra' => array (
                    'type' => 'Literal',
                    'options' => array (
                            'route' => '/admin/regra',
                                    'defaults' => array (
                                            'controller' => 'Admin\Controller\Regra',
                                            'action' => 'index'
                                    )
                            ),
                    'may_terminate' => true,
                    'child_routes' => array (
                            'default' => array (
                                    'type' => 'Segment',
                                    'options' => array (
                                            'route' => '[/:action[/:id][/:upload]]',
                                            'constraints' => array (
                                                    'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                    'defaults' => array (
                                                            'controller' => 'Regra'
                                                    )
                                            )
                                    )
                            )
                    )
            ),


            ##########################################################################################
            # -- USUÁRIO
            'admin-usuario' => array(
                    'type' => 'Literal',
                    'options' => array(
                            'route'    => '/admin/usuario',
                            'defaults' => array(
                                    'controller' => 'Admin\Controller\Usuario',
                                    'action'     => 'index',
                            ),
                    ),
                    'may_terminate' => true,
                    'child_routes' => array(
                            'default' => array(
                                    'type' => 'Segment',
                                    'options' => array(
                                            'route' => '[/:action[/:id]]',
                                            'constraints' => array(
                                                    'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                                            ),
                                            'defaults' => array(
                                                    'controller' => 'Admin\Controller\Usuario'
                                            )
                                    )
                            ),
                    ),
            ),
            'admin-usuario-register' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/admin/usuario/register',
                    'defaults' => array(
                        'controller'=> 'Admin\Controller\Index',
                        'action'	=> 'register',
                    )
                )
            ),
            'admin-usuario-activate' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/usuario/register/activate[/:key]',
                    'defaults' => array(
                        'controller'=> 'Admin\Controller\Index',
                        'action'	=> 'activate',
                    )
                )
            ),
        ),
    ),

    /** CONTROLLERS **/
    'controllers' => array(
        'invokables' => array(
            'Admin\Controller\Index'   	    => 'Admin\Controller\IndexController',
        	'Admin\Controller\Forbidden'    => 'Admin\Controller\ForbiddenController'
        ),
        'factories' => array(
            'Admin\Controller\Empresa'   => 'Admin\Controller\Factory\EmpresaControllerFactory',
            'Admin\Controller\Parceiro'  => 'Admin\Controller\Factory\ParceiroControllerFactory',
            'Admin\Controller\Servico'   => 'Admin\Controller\Factory\ServicoControllerFactory',
            'Admin\Controller\Portfolio' => 'Admin\Controller\Factory\PortfolioControllerFactory',
            'Admin\Controller\Suporte'   => 'Admin\Controller\Factory\SuporteControllerFactory',
            'Admin\Controller\Usuario'   => 'Admin\Controller\Factory\UsuarioControllerFactory'
        ),
        'abstract_factories' => array(
            'Admin\Controller\Factory\AbstractControllerFactory'
        )
    ),

    /** VIEW MANAGERS **/
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        #'exception_template'       => 'admin/error/index',
        'template_map' => array(
            'layout/admin'     => __DIR__ . '/../view/layout/admin.phtml',
            'admin/index/index'=> __DIR__ . '/../view/admin/index/index.phtml',
            'admin/error/index'=> __DIR__ . '/../view/error/index.phtml'
        ),
        'template_path_stack' => array(
            'admin' => __DIR__ . '/../view',
        ),
    	'strategies' => array(
    		'ViewJsonStrategy'
    	)
    ),

    /** SERVICE MANAGERS **/
    'service_manager' => array(
        'factories' => array(
            # NAVIGATION ###############################################################
            'Navigation\Admin'            => 'Admin\View\Helper\AdminNavigationFactory',
            # EMPRESA ##################################################################
            'Admin\Service\Empresa'       => 'Admin\Service\Factory\EmpresaServiceFactory',
            # PARCEIRO #################################################################
            'Admin\Service\Parceiro'      => 'Admin\Service\Factory\ParceiroServiceFactory',
            # SERVICO ##################################################################
            'Admin\Service\Servico'       => 'Admin\Service\Factory\ServicoServiceFactory',
            # PORTFOLIO ################################################################
            'Admin\Service\Portfolio'     => 'Admin\Service\Factory\PortfolioServiceFactory',
            # SUPORTE ##################################################################
            'Admin\Service\Suporte'       => 'Admin\Service\Factory\SuporteServiceFactory',
            # USUARIO ##################################################################
            'Admin\Service\Usuario'       => 'Admin\Service\Factory\UsuarioServiceFactory'
        )
    ),

    /** INPUT FILTERS **/
    'input_filters' => array(
        'factories' => array(
            'Admin\Form\InputFilter\AlterarSenhaFilter' => 'Admin\Form\InputFilter\Factory\AlterarSenhaFilterFactory'
        )
    ),
);
