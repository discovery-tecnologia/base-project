<?php
/**
 * Class LojaController
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Admin\Controller;

use Admin\Service\EmpresaService;
use Base\Controller\AbstractCrudController;
use Base\Enum\FlashMessages;
use Base\Exception\BusinessException;
use Zend\Form\FormInterface;

class EmpresaController extends AbstractCrudController
{

    /**
     * @construct
     */
    public function __construct( FormInterface $form, EmpresaService $empresaService ) {

        $this->form       = $form;
        $this->service 	  = $empresaService;
    }

    /**
     * @override
     * @method editAction()
     * Editar um registro
     * @return \Zend\View\Model\ViewModel
     */
    public function editAction() {

        try {

            $empresa = $this->service->find(1);
            $this->form->bind($empresa);
            $request = $this->getRequest();
            if ( $request->isPost() ) {
                $this->form->setData($request->getPost());
                if ( $this->form->isValid() ) {
                    if ( $this->service->save( $empresa ) ) {
                        $this->flashMessenger()->addSuccessMessage(FlashMessages::SUCESSO_PADRAO_SALVAR);
                        return $this->redirectToIndex();
                    }
                }
            }
            return $this->renderView(['form' => $this->form,'id' => $empresa->getId()]);

        } catch ( BusinessException $b ) {
            $this->flashMessenger()->addInfoMessage($b->getMessage());
            $this->getLogger()->info($b->getMessage());
        } catch( \Exception $e ) {
            $this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
        }
        return $this->redirectToHome();
    }
} 