<?php
/**
 * Class LojaControllerFactory
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Admin\Controller\Factory;


use Admin\Controller\EmpresaController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class EmpresaControllerFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocatorInstance = $serviceLocator->getServiceLocator();

        $formManager = $serviceLocatorInstance->get('FormElementManager');
        $form = $formManager->get('Admin\Form\Empresa');
        $service = $serviceLocatorInstance->get('Admin\Service\Empresa');
        return new EmpresaController( $form, $service );
    }
} 