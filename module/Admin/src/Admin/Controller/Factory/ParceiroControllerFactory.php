<?php
/**
 * Class LojaControllerFactory
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Admin\Controller\Factory;


use Admin\Controller\EmpresaController;
use Admin\Controller\ParceiroController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ParceiroControllerFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocatorInstance = $serviceLocator->getServiceLocator();

        $formManager = $serviceLocatorInstance->get('FormElementManager');
        $form = $formManager->get('Admin\Form\Parceiro');
        $service = $serviceLocatorInstance->get('Admin\Service\Parceiro');
        return new ParceiroController( $form, $service );
    }
} 