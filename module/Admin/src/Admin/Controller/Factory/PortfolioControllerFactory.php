<?php
namespace Admin\Controller\Factory;

use Admin\Controller\PortfolioController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PortfolioControllerFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocatorInstance = $serviceLocator->getServiceLocator();
        // form
        $forms = $serviceLocatorInstance->get('FormElementManager');
        $form  = $forms->get('Admin\Form\Portfolio');
        // produto Service
        $service = $serviceLocatorInstance->get('Admin\Service\Portfolio');
        return new PortfolioController( $form, $service );
    }
}