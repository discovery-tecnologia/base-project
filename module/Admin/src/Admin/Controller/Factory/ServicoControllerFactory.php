<?php
namespace Admin\Controller\Factory;

use Admin\Controller\ServicoController;
use MercadoLivre\ValueObject\Credentials;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class ServicoControllerFactory implements FactoryInterface {

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocatorInstance = $serviceLocator->getServiceLocator();
        // form
        $forms = $serviceLocatorInstance->get('FormElementManager');
        $form  = $forms->get('Admin\Form\Servico');
        // produto Service
        $service = $serviceLocatorInstance->get('Admin\Service\Servico');
        return new ServicoController( $form, $service );
    }
}