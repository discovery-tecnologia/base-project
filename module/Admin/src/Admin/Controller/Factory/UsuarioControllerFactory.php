<?php
namespace Admin\Controller\Factory;
use Admin\Controller\UsuarioController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 15/03/14
 * Time: 00:19
 */
class UsuarioControllerFactory implements FactoryInterface {

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocatorInstance = $serviceLocator->getServiceLocator();

        $formManager = $serviceLocatorInstance->get('FormElementManager');
        $form = $formManager->get('Admin\Form\Usuario');
        $formAlterarSenha = $formManager->get('Admin\Form\AlterarSenha');
        $service = $serviceLocatorInstance->get('Admin\Service\Usuario');

        return new UsuarioController( $form, $formAlterarSenha, $service );
    }
}