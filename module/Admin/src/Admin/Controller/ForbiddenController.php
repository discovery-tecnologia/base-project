<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;

use Base\Controller\AbstractCrudController;


class ForbiddenController extends AbstractCrudController {
	
	
	
	/**
	 * @construct
	 */
	public function __construct() {}

	
	public function indexAction() {
		
		$this->flashMessenger()
			 ->setNamespace( 'Admin' )
			 ->addMessage( array(
			 		'alert' => 'O seu acesso foi negado!',
					'info'  => "Você não possui autorização para acessar esta página!"
			 ) );
		
		return $this->redirect()->toRoute('admin-forbidden');
	}
	
	public function forbiddenAction() {
		
		$messages = $this->flashMessenger()
						 ->setNamespace('Admin')
						 ->getMessages();
		
		return new ViewModel( array( 'messages' => $messages ) );
	}
}