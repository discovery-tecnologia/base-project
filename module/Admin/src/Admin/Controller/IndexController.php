<?php

namespace Admin\Controller;

use Base\Controller\AbstractCrudController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\Tools\EntityGenerator;


class IndexController extends AbstractCrudController {
	
	
	
	/**
	 * @construct
	 */
	public function __construct() {}	
	
	
    public function indexAction() {
    	return new ViewModel();
    }
    
    
    
    /**
     * Cria entidades
     */
    public function gerarEntityAction(){
    
    	try{
    		ini_set("display_errors", "On");
    		$libPath = __DIR__ . '../../../../../../vendor/doctrine/common/lib/Doctrine/Common/'; // Set this to where you have doctrine2 installed
    		// autoloaders
    		 
    		require_once $libPath . 'ClassLoader.php';
    		 
    		$classLoader = new \Doctrine\Common\ClassLoader('Doctrine', $libPath);
    		$classLoader->register();
    		 
    		$classLoader = new \Doctrine\Common\ClassLoader('Entities', __DIR__);
    		$classLoader->register();
    		 
    		$classLoader = new \Doctrine\Common\ClassLoader('Proxies', __DIR__);
    		$classLoader->register();
    		 
    		// config
    		$config = new \Doctrine\ORM\Configuration();
    		$config->setMetadataDriverImpl($config->newDefaultAnnotationDriver(__DIR__ . '/Entities'));
    		$config->setMetadataCacheImpl(new \Doctrine\Common\Cache\ArrayCache);
    		$config->setProxyDir(__DIR__ . '/Proxies');
    		$config->setProxyNamespace('Proxies');
    		 
    		$connectionParams = array(
    				'driver' => 'pdo_mysql',
    				'host' => 'localhost',
    				'port' => '3306',
    				'user' => 'root',
    				'password' => '',
    				'dbname' => '001_encontrepecas',
    				'charset' => 'utf8',
    		);
    		 
    		 
    		$em = \Doctrine\ORM\EntityManager::create($connectionParams, $config);
    		 
    		// custom datatypes (not mapped for reverse engineering)
    		$em->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('set', 'string');
    		$em->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    		 
    		// fetch metadata
    		$driver = new \Doctrine\ORM\Mapping\Driver\DatabaseDriver(
    				$em->getConnection()->getSchemaManager()
    		);
    		$em->getConfiguration()->setMetadataDriverImpl($driver);
    		$cmf = new \Doctrine\ORM\Tools\DisconnectedClassMetadataFactory($em);
    		$cmf->setEntityManager($em);
    		$classes = $driver->getAllClassNames();
    		 
    		$metadata = $cmf->getAllMetadata();
    		$generator = new EntityGenerator();
    		$generator->setUpdateEntityIfExists(true);
    		$generator->setGenerateStubMethods(true);
    		$generator->setGenerateAnnotations(true);
    		$generator->generate($metadata, __DIR__ . '/../Entity');
    
    		echo 'Criado com sucesso!';
    		die;
    		$this->flashMessenger()
    		->setNamespace( 'Admin' )
    		->addMessage( array('success' => 'Entidades geradas com sucesso!') );
    			
    		$this->flashMessenger()
    		->setNamespace( 'Admin' )
    		->addMessage( array('info' => 'Código está comentado!') );
    
    
    		return $this->redirect()->toRoute(
    				$this->route,
    				array( 'controller' => $this->controller
    				) );
    			
    	} catch(\Exception $e){
    		 
    		echo "<pre>";
    		print_r($e->getMessage());
    		print_r($e->getTraceAsString());
    		exit;
    		 
    	}
    }
}
