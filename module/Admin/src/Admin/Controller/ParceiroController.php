<?php
/**
 * Class LojaController
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Admin\Controller;

use Admin\Service\ParceiroService;
use Base\Controller\AbstractCrudController;
use Zend\Form\FormInterface;

class ParceiroController extends AbstractCrudController
{

    /**
     * @construct
     */
    public function __construct( FormInterface $form, ParceiroService $parceiroService ) {

        $this->form       = $form;
        $this->service 	  = $parceiroService;
    }
} 