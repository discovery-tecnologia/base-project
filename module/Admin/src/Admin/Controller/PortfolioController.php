<?php
namespace Admin\Controller;

use Admin\Service\PortfolioService;
use Base\Controller\AbstractCrudController;
use Zend\Form\FormInterface;

class PortfolioController extends AbstractCrudController {


    public function __construct(FormInterface $form, PortfolioService $portfolioService) {

        $this->form    = $form;
        $this->service = $portfolioService;
    }
}