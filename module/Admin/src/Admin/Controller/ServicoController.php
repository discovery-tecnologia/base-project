<?php
namespace Admin\Controller;

use Base\Controller\AbstractCrudController;
use Zend\Form\FormInterface;
use Admin\Service\ServicoService;

class ServicoController extends AbstractCrudController {


    public function __construct(FormInterface $form, ServicoService $produtoService) {

        $this->form    = $form;
        $this->service = $produtoService;
    }
}