<?php
/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 27/03/14
 * Time: 19:16
 */

namespace Admin\Controller;

use Base\Enum\FlashMessages;
use Zend\Form\FormInterface;
use Base\Controller\AbstractCrudController;
use Zend\View\Model\ViewModel;
use Admin\Service\SuporteService;

class SuporteController extends AbstractCrudController {


    public function __construct( FormInterface $form, SuporteService $service ) {

        $this->form = $form;
        $this->service = $service;
    }

    /**
     * @override
     * @method editAction()
     * Editar um registro
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction() {

        try {
            $request = $this->getRequest();
            if ( $request->isPost() ) {
                $this->form->setData($request->getPost());
                if ( $this->form->isValid() ) {
                    $dados = $request->getPost()->toArray();

                    if ( $this->service->sendMail( $dados ) ) {
                        $this->flashMessenger()->addSuccessMessage('Email enviado com sucesso');
                        return $this->redirectToIndex();
                    }
                }
            }
            return new ViewModel(['form' => $this->form]);

        } catch( \Exception $e ) {
            $this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
        }
        return $this->redirectToIndex();
    }
} 