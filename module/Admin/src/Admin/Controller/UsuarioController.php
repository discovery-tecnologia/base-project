<?php
namespace Admin\Controller;

use Base\Enum\FlashMessages;
use Base\Controller\AbstractCrudController;
use Admin\Service\UsuarioService;
use Base\Exception\BusinessException;
use Base\Exception\InvalidInstanceException;
use Servidor\Interfaces\ObjectEntity;
use Zend\Form\FormInterface;
use Zend\View\Model\ViewModel;

class UsuarioController extends AbstractCrudController {

    /**
     * @var FormInteraface
     */
    private $formAlterarSenha;

    public function __construct(FormInterface $form, FormInterface $formAlterarSenha, UsuarioService $service) {

        $this->form       = $form;
        $this->formAlterarSenha = $formAlterarSenha;
        $this->service 	  = $service;
    }

	/**
	 * @method indexAction()
	 * Responsável por fazer listagem de dados.
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 * @access public
	 */
	public function indexAction() {
	
		try {
            $list = $this->service->findAllUsers();
			return new ViewModel(['data' => $list]);

		} catch( \Exception $e ) {
			$this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
			return $this->redirectToIndex();
		}
	}

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function editarAction()
    {
        try {
            $id = $this->params()->fromRoute('id', 0);
            $usuario = $this->service->find( $id );
            if (!($usuario instanceof ObjectEntity))
                throw new InvalidInstanceException('Não foi possível recuperar os dados deste usuário');

            $this->form->bind($usuario);
            $this->form->setValidationGroup(array(
                'usuario' => array(
                    'nome',
                    'email',
                    'ativo'
                )
            ));
            $request = $this->getRequest();
            if ( $request->isPost() ) {
                $this->form->setData($request->getPost());
                if ( $this->form->isValid() ) {
                    $usuario = $this->form->getData();
                    if ( $this->service->save( $usuario ) ) {
                        $this->flashMessenger()->addSuccessMessage(FlashMessages::SUCESSO_PADRAO_SALVAR);
                        return $this->redirectToIndex();
                    }
                }
            }
            return new ViewModel(['form' => $this->form,'id' => $id]);

        } catch (InvalidInstanceException $i) {
            $this->flashMessenger()->addInfoMessage($i->getMessage());
            $this->getLogger()->info($i->getMessage());
        } catch ( BusinessException $b ) {
            $this->flashMessenger()->addInfoMessage($b->getMessage());
            $this->getLogger()->info($b->getMessage());
        } catch( \Exception $e ) {
            $this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
        }
        return $this->redirectToIndex();
    }


    public function alterarSenhaAction()
    {
        try {
            $id = $this->params()->fromRoute('id', 0);
            $usuario = $this->service->find( $id );
            if (!($usuario instanceof ObjectEntity))
                throw new InvalidInstanceException(FlashMessages::INSTANCIA_INVALIDA);

            $this->formAlterarSenha->bind($usuario);
            $request = $this->getRequest();

            if ( $request->isPost() ) {

                $this->formAlterarSenha->setData($request->getPost());

                if ( $this->formAlterarSenha->isValid() ) {
                    $usuario = $this->formAlterarSenha->getData();
                    if ( $this->service->save( $usuario ) ) {
                        $this->flashMessenger()->addSuccessMessage(FlashMessages::SUCESSO_PADRAO_SALVAR);
                        return $this->redirectToIndex();
                    }
                }
            }
            return new ViewModel(['form' => $this->formAlterarSenha,'id' => $id]);

        } catch ( BusinessException $b ) {
            $this->flashMessenger()->addInfoMessage($b->getMessage());
            $this->getLogger()->info($b->getMessage());
        } catch( \Exception $e ) {
            $this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
            $this->getLogger()->err($e->getMessage());
        }
        return $this->redirectToIndex();
    }
}
