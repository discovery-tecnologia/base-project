<?php
/**
 * Class AlterarSenhaForm
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Admin\Form;

use Servidor\Entity\Entities;
use Zend\Form\Element\Csrf;
use Zend\Form\Form;

class AlterarSenha extends Form
{

    public function init()
    {
        parent::__construct('alterar_senha');

        $this->setAttributes(array(
            'method' => 'post',
            'class'  => 'form'
        ));

        // senha **************** ALTERAR SENHA (Fieldset) **********************
        $this->add(array(
            'type'     => 'Admin\Form\Fieldset\AlterarSenhaFieldset',
            'name'     => 'usuario',
            'hydrator' => 'DoctrineModule\Stdlib\Hydrator\DoctrineObject',
            'object'   => Entities::ENTITY_USUARIO,
            'options' => array(
                'use_as_base_fieldset' => true
            )
        ));

        // security ***************** CSRF (Csrf) ***************************
        $csrf = new Csrf('security');
        $csrf->setOptions( array (
            'csrf_options'  =>  array (
                'timeout'  =>  900
            )) );
        $this->add($csrf);
    }
} 