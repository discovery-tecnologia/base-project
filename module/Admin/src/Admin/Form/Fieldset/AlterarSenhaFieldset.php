<?php
/**
 * Class AlterarSenhaFieldset
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Admin\Form\Fieldset;

use Zend\Form\Element\Password;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class AlterarSenhaFieldset extends Fieldset implements InputFilterProviderInterface
{


    public function init()
    {
        // senha atual ***********SENHA ATUAL (PASSWORD) *******************
        $senhaAtual = new Password('senhaAtual');
        $senhaAtual->setLabel('Senha Atual: ')->setLabelAttributes ( array (
            'class' => 'control-label required'
        ))->setAttributes(array(
                'class'			=> 'tool_tip',
                'data-placement'=> 'left',
                'placeholder'	=> 'Senha Atual',
                'title'			=> 'Senha atual',
                'id'            => 'senha-atual',
                'maxlength'		=> '50'
            ));
        $this->add($senhaAtual);

        // senha ****************SENHA (PASSWORD) *************************
        $senha = new Password('senha');
        $senha->setLabel('Senha: ')->setLabelAttributes ( array (
            'class' => 'control-label required'
        ))->setAttributes(array(
                'class'			=> 'tool_tip',
                'data-placement'=> 'left',
                'placeholder'	=> 'Senha',
                'title'			=> 'Senha que será utilizada para acessar o carrinho',
                'id'            => 'senha',
                'maxlength'		=> '50'
            ));
        $this->add($senha);

        // confirmção **********CONFIRMAÇAO (PASSWORD) ********************
        $confirmacao = new Password('confirmacao');
        $confirmacao->setLabel('Confirmação: ')->setLabelAttributes ( array (
            'class' => 'control-label required'
        ))->setAttributes(array(
                'class'			=> 'tool_tip',
                'data-placement'=> 'left',
                'placeholder'	=> 'Confirme a senha',
                'title'			=> 'Confirme a senha',
                'id'            => 'confirmacao',
                'maxlength'		=> '50'
            ));
        $this->add($confirmacao);
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array('type' => 'Admin\Form\InputFilter\AlterarSenhaFilter');
    }

} 