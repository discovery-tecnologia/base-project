<?php
namespace Admin\Form\Fieldset;

use Zend\Form\Fieldset;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Text;
use Zend\Form\Element\Textarea;
use Zend\InputFilter\InputFilterProviderInterface;

class EmpresaFieldset extends Fieldset implements InputFilterProviderInterface
{

    public function init()
    {
        // id *************************** ID (HIDDEN) **********************
        $id = new Hidden( 'id' );
        $this->add ( $id );
        
        // nome ************************* NOME (TEXT) **********************
        $nome = new Text('nome');
        $nome
            ->setLabel('Nome')
            ->setLabelAttributes(
                array (
                    'class' => 'control-label required'
                )
            )
            ->setAttributes(
                array (
        		    'class'          => 'form-control',
        		    'data-placement' => 'bottom',
        		    'placeholder'    => 'Nome',
        		    'title'          => 'Nome do cliente',
        		    'id'             => 'nome',
        		    'maxlength'      => '70'
        ) );
        $this->add ( $nome );

        // slogan ********************* SLOGAN (TEXT) *********************
        $slogan = new Text('slogan');
        $slogan
            ->setLabel('Slogan: ')
            ->setLabelAttributes(
                array(
                    'class' => 'control-label required'
                )
            )
            ->setAttributes(
                array (
                    'class'          => 'form-control',
                    'data-placement' => 'bottom',
                    'placeholder'    => 'Slogan',
                    'title'          => 'Slogan da Empresa',
                    'id'             => 'slogan',
                    'maxlength'      => '255'
                )
            );
        $this->add($slogan);

        // cep ************************* CEP (TEXT) **********************
        $cep = new Text('cep');
        $cep->setLabel('CEP: ')
            ->setLabelAttributes(
                array(
                    'class' => 'control-label'
                )
            )
            ->setAttributes(
                array(
                    'class'          => 'form-control',
                    'data-placement' => 'bottom',
                    'placeholder'    => 'Cep',
                    'title'          => 'Cep',
                    'id'             => 'cep',
                    'maxlength'      => '9'
                )
            );
        $this->add($cep);

        // logradouro **************** LOGRADOURO (TEXT) *****************
        $logradouro = new Text('logradouro');
        $logradouro
            ->setLabel('Logradouro: ')
            ->setLabelAttributes(
                array ('class' => 'control-label'
                )
            )
            ->setAttributes(
                array (
                    'class'          => 'form-control',
                    'data-placement' => 'bottom',
                    'placeholder'    => 'Logradouro',
                    'title'          => 'Logradouro',
                    'id'             => 'logradouro',
                    'maxlength'      => '200'
                )
            );
        $this->add ( $logradouro );

        // numero ****************** NUMERO (TEXT) **********************
        $numero = new Text('numero');
        $numero
            ->setLabel('Número: ')
            ->setLabelAttributes(
                array(
                    'class' => 'control-label'
                )
            )
            ->setAttributes(
                array (
                    'class'          => 'form-control',
                    'data-placement' => 'bottom',
                    'placeholder'    => 'Numero',
                    'title'          => 'Numero',
                    'id'             => 'numero',
                    'maxlength'      => '6'
                )
            );
        $this->add($numero);

        // bairro ****************** BAIRRO (TEXT) **********************
        $bairro = new Text('bairro');
        $bairro
            ->setLabel('Bairro: ')
            ->setLabelAttributes(
                array(
                    'class' => 'control-label'
                )
            )
            ->setAttributes(
                array (
                    'class'          => 'form-control',
                    'data-placement' => 'bottom',
                    'placeholder'    => 'Bairro',
                    'title'          => 'Bairro',
                    'id'             => 'bairro',
                    'maxlength'      => '60'
                )
            );
        $this->add ( $bairro );

        // cidade ****************** CIDADE (TEXT) **********************
        $cidade = new Text('cidade');
        $cidade
            ->setLabel('Cidade: ')
            ->setLabelAttributes(
                array(
                    'class' => 'control-label'
                )
            )
            ->setAttributes(
                array (
                    'class'          => 'form-control',
                    'data-placement' => 'bottom',
                    'placeholder'    => 'Cidade',
                    'title'          => 'Cidade',
                    'id'             => 'cidade',
                    'maxlength'      => '60'
                )
            );
        $this->add ( $cidade );

        // uf ************************* UF (TEXT) **********************
        $uf = new Text('uf');
        $uf->setLabel('UF: ')
            ->setLabelAttributes(
                array(
                    'class' => 'control-label'
                )
            )
            ->setAttributes(
                array (
                    'class'          => 'form-control',
                    'data-placement' => 'bottom',
                    'placeholder'    => 'UF',
                    'title'          => 'UF',
                    'id'             => 'uf',
                    'maxlength'      => '2'
                )
            );
        $this->add ( $uf );

        // email ******************* EMAIL (TEXT) **********************
        $email = new Text('email');
        $email
            ->setLabel('E-mail: ')
            ->setLabelAttributes(
                array(
                    'class' => 'control-label'
                )
            )
            ->setAttributes(
                array (
                    'class'          => 'form-control',
                    'data-placement' => 'bottom',
                    'placeholder'    => 'E-mail',
                    'title'          => 'E-mail',
                    'id'             => 'email',
                    'maxlength'      => '70'
                )
            );
        $this->add ( $email );

        // ddd ******************** DDD (TEXT) ************************
        $ddd = new Text('ddd');
        $ddd->setLabel('DDD:')
            ->setLabelAttributes(
                array('class' => 'control-label')
            )
            ->setAttributes(
                array (
                    'class'          => 'form-control',
                    'data-placement' => 'bottom',
                    'placeholder'    => 'DDD',
                    'title'          => 'DDD',
                    'id'             => 'ddd',
                    'maxlength'      => '2'
                )
            );
        $this->add($ddd);

        // telefone *************** TELEFONE (TEXT) *******************
        $telefone = new Text('telefone');
        $telefone
            ->setLabel('Telefone:')
            ->setLabelAttributes(
                array(
                    'class' => 'control-label'
                )
            )
            ->setAttributes (
                array (
                    'class'          => 'form-control',
                    'data-placement' => 'bottom',
                    'placeholder'    => 'Telefone',
                    'title'          => 'Telefone',
                    'id'             => 'telefone',
                    'maxlength'      => '25'
                )
            );
        $this->add($telefone);

        // analytics *************** ANALYTICS (TEXT) *****************
        $analytics = new Text('analytics');
        $analytics
            ->setLabel('Google Analytics:')
            ->setLabelAttributes(
                array (
                    'class' => 'control-label'
                )
            )
            ->setAttributes(
                array (
                    'class'          => 'form-control',
                    'data-placement' => 'bottom',
                    'placeholder'    => 'Google Analytics',
                    'title'          => 'Google Analytics',
                    'id'             => 'analytics',
                    'maxlength'      => '20'
                )
            );
        $this->add ( $analytics );

        // quemSomos ********** QUEM SOMOS (TEXTAREA) *****************
        $quemSomos = new Textarea('quemSomos');
        $quemSomos
            ->setLabel('Quem Somos:')
            ->setLabelAttributes(
                array (
                    'class' => 'control-label required'
                )
            )
            ->setAttributes(
                array (
                    'placeholder' => 'Quem somos',
                    'class'       => 'jmce form-control'
                )
            );
        $this->add ( $quemSomos );

        // textoContato ***** TEXTO CONTATO (TEXTAREA) ****************
        $textoContato = new Textarea( 'textoContato' );
        $textoContato
            ->setLabel('Texto contato:')
            ->setLabelAttributes(
                array(
                    'class' => 'control-label required'
                )
            )
            ->setAttributes(
                array(
                    'placeholder' => 'Texto contato',
                    'class'       => 'textarea-editor form-control',
                    'rows'        => 5
                )
            );
        $this->add($textoContato);

        // textoRodape ******** TEXTO RODAPE (TEXTAREA) ****************
        $textoRodape = new Textarea('textoRodape');
        $textoRodape
            ->setLabel('Texto rodapé:')
            ->setLabelAttributes(
                array (
                    'class' => 'control-label required'
                )
            )
            ->setAttributes(
                array (
                    'placeholder' => 'Texto do Rrodapé',
                    'class'       => 'textarea-editor form-control',
                    'rows'        => 5
        ) );
        $this->add ( $textoRodape );

        // mapaLocalizacao ******* MAPA LOCALIZACAO (TEXTAREA) *********
        $mapaLocalizacao = new Textarea('mapaLocalizacao');
        $mapaLocalizacao
            ->setLabel('Mapa Localização:' )
            ->setLabelAttributes(
                array(
                    'class' => 'control-label'
                )
            )
            ->setAttributes(
                array(
                    'placeholder' => 'Mapa de localização',
                    'class'       => 'form-control',
                    'rows'        => 5
                )
            );
        $this->add ( $mapaLocalizacao );

        // facebook ************* FACEBOOK (TEXT) **********************
        $facebook = new Text('facebook');
        $facebook
            ->setLabel('Facebook: ')
            ->setLabelAttributes(
                array(
                    'class' => 'control-label'
                )
            )
            ->setAttributes(
                array (
        		    'class'          => 'form-control',
        		    'data-placement' => 'bottom',
        		    'placeholder'    => 'Facebook da empresa',
        		    'title'          => 'Facebook da empresa',
        		    'id'             => 'facebook',
        		    'maxlength'      => '255'
                )
            );
        $this->add ( $facebook );

        // facebookPlugin ********** FACEBOOK PLUGIN (TEXTAREA) ********
        $facebookPlugin = new Textarea('facebookPlugin');
        $facebookPlugin
            ->setLabel('Plugin Facebook:')
            ->setLabelAttributes(
                array (
                    'class' => 'control-label'
                )
            )
            ->setAttributes(
                array (
                    'placeholder' => 'Plugin Facebook',
                    'class'       => 'form-control',
                    'rows'        => 5
                )
            );
        $this->add($facebookPlugin);

        // twitter ***************** TWITTER (TEXT) ********************
        $twitter = new Text('twitter');
        $twitter
            ->setLabel('Twitter:')
            ->setLabelAttributes(
                array(
                    'class' => 'control-label'
                )
            )
            ->setAttributes(
                array (
                    'class'          => 'form-control',
                    'data-placement' => 'bottom',
                    'placeholder'    => 'Twitter da empresa',
                    'title'          => 'Twitter da empresa',
                    'id'             => 'twitter',
                    'maxlength'      => '255'
                )
            );
        $this->add($twitter);

        // twitterPlugin ********* TWITTER PLUGIN (TEXTAREA) ***********
        $twitterPlugin = new Textarea('twitterPlugin' );
        $twitterPlugin
            ->setLabel('Plugin Twitter:')
            ->setLabelAttributes(
                array (
                    'class' => 'control-label'
                )
            )
            ->setAttributes(
                array (
                    'placeholder' => 'Plugin Twitter',
                    'class'       => 'form-control',
                    'rows'        => 5
                )
            );
        $this->add ( $twitterPlugin );

        // youtube **************** TWITTER (TEXT) **********************
        $youtube = new Text( 'youtube' );
        $youtube
            ->setLabel('You Tube:')
            ->setLabelAttributes(
                array(
                    'class' => 'control-label'
                )
            )
            ->setAttributes(
                array (
                    'class'          => 'form-control',
                    'data-placement' => 'bottom',
                    'placeholder'    => 'Canal You Tube da empresa',
                    'title'          => 'Canal You Tube da empresa',
                    'id'             => 'twitter',
                    'maxlength'      => '255'
                )
            );
        $this->add($youtube);

        // googlemais ************ GOOGLE MAIS (TEXT) *******************
        $googlemais = new Text('googleMais');
        $googlemais
            ->setLabel('Google +: ')
            ->setLabelAttributes(
                array(
                    'class' => 'control-label'
                )
            )
            ->setAttributes(
                array (
                    'class'          => 'form-control',
                    'data-placement' => 'bottom',
                    'placeholder'    => 'Google + da empresa',
                    'title'          => 'Google + da empresa',
                    'id'             => 'googlemais',
                    'maxlength'      => '255'
                )
            );
        $this->add($googlemais);
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array('type' => 'Admin\Form\InputFilter\EmpresaFilter');
    }
}
