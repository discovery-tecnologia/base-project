<?php
namespace Admin\Form\Fieldset;

use Zend\Form\Element\Checkbox;
use Zend\Form\Fieldset;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Text;
use Zend\Form\Element\Textarea;
use Zend\InputFilter\InputFilterProviderInterface;

class ParceiroFieldset extends Fieldset implements InputFilterProviderInterface
{

    public function init() {
        
        // id *************************** ID (HIDDEN) **********************
        $id = new Hidden( 'id' );
        $this->add ( $id );
        
        // nome ************************* NOME (TEXT) **********************
        $nome = new Text( 'nome' );
        $nome->setLabel ( 'Nome: ' )
            ->setLabelAttributes ( array ('class' => 'control-label required') )
            ->setAttributes ( array (
        		'class' => 'tool_tip',
        		'data-placement' => 'bottom',
        		'placeholder' => 'Nome',
        		'title' => 'Nome do cliente',
        		'id' => 'nome',
        		'maxlength' => '70'
        ) );
        $this->add ( $nome );

        // link ************************* LINK (TEXT) **********************
        $link = new Text( 'link' );
        $link->setLabel ( 'Link: ' )
            ->setLabelAttributes ( array ('class' => 'control-label required') )
            ->setAttributes ( array (
                'class' => 'tool_tip',
                'data-placement' => 'bottom',
                'placeholder' => 'Link',
                'title' => 'Link',
                'id' => 'nome',
                'maxlength' => '200'
            ) );
        $this->add ( $link );

        // cliente ********************* CLIENTE (CHECKBOX) ****************
        $cliente = new Checkbox('cliente');
        $cliente->setOptions(array(
            'label'			=>'É Cliente?'
        ));
        $cliente->setAttributes(array(
            'class'			=> 'tool_tip',
            'data-placement'=> 'bottom',
            'title'			=>'Seta se o parceiro é cliente',
            'value'         => 1
        ));
        $this->add( $cliente );

        // parceiro **************** PARCEIRO (CHECKBOX) ******************
        $parceiro = new Checkbox('parceiro');
        $parceiro->setOptions(array(
            'label'			=>'É Parceiro?'
        ));
        $parceiro->setAttributes(array(
            'class'			=> 'tool_tip',
            'data-placement'=> 'bottom',
            'title'			=>'Seta se é parceiro',
            'value'         => 1
        ));
        $this->add( $parceiro );
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification() {
        return array('type' => 'Admin\Form\InputFilter\ParceiroFilter');
    }
}
