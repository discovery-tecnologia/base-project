<?php
namespace Admin\Form\Fieldset;

use Servidor\Entity\Entities;
use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Select;
use Zend\Form\Element\Text;
use Zend\Form\Element\Textarea;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class ServicoFieldset extends Fieldset implements InputFilterProviderInterface {


    public function init() {

        // id *************************** ID (HIDDEN) **********************
        $id = new Hidden( 'id' );
        $this->add ( $id );

        // nome ************************* NOME (TEXT) **********************
        $nome = new Text( 'nome' );
        $nome->setLabel ( 'Nome: ' )
            ->setLabelAttributes ( array ('class' => 'control-label required') )
            ->setAttributes ( array (
                'class' => 'tool_tip',
                'data-placement' => 'bottom',
                'placeholder' => 'Nome',
                'title' => 'Nome do item',
                'id' => 'nome',
                'maxlength' => '100'
            ) );
        $this->add ( $nome );

        // ativo ********************* ATIVO (CHECKBOX) **********************
        $ativo = new Checkbox('ativo');
        $ativo->setOptions(array(
                'label'			=>'Ativo:'
            ));
        $ativo->setAttributes(array(
                'class'			=> 'tool_tip',
                'data-placement'=> 'bottom',
                'title'			=>'Ativa ou desativa o produto',
                'value'         => 1
            ));
        $this->add( $ativo );

        // descricao **************** DESCRICAO (TEXTAREA) **************
        $descricao = new Textarea( 'descricao' );
        $descricao->setLabel ( 'Descrição:' )
            ->setLabelAttributes ( array ('class' => 'control-label required') )
            ->setAttributes ( array (
                'placeholder' => 'Descrição',
                'class' => 'jmce tool_tip'
            ) );
        $this->add ( $descricao );
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array('type' => 'Admin\Form\InputFilter\ServicoFilter');
    }
}
