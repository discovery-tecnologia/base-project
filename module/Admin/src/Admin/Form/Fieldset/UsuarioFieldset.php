<?php
/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 28/03/14
 * Time: 19:36
 */

namespace Admin\Form\Fieldset;


use Servidor\Entity\Entities;
use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Password;
use Zend\Form\Element\Text;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class UsuarioFieldset extends Fieldset implements InputFilterProviderInterface {


    public function init() {

        // id
        $id = new Hidden('id');
        $this->add($id);

        // nome
        $nome = new Text('nome');
        $nome->setLabel('Nome: ')
            ->setLabelAttributes ( array ('class' => 'control-label required') )
            ->setAttributes(array(
                'class'			=> 'tool_tip',
                'data-placement'=> 'bottom',
                'placeholder'	=> 'Nome do usuário',
                'title'			=> 'Nome do usuário',
                'maxlength'		=> '45'
            ));
        $this->add($nome);

        // email
        $email = new Text('email');
        $email->setLabel('Email: ')
            ->setLabelAttributes ( array ('class' => 'control-label required') )
            ->setAttributes(array(
                'class'			=> 'tool_tip',
                'data-placement'=> 'bottom',
                'placeholder'	=> 'Email',
                'title'			=> 'Email que será utilizado para o login e recuperação de senha',
                'maxlength'		=> '70'
            ));
        $this->add($email);

        // ativo ************************* ATIVO (CHECKBOX) **********************
        $ativo = new Checkbox('ativo');
        $ativo
            ->setOptions(array(
                'label'			=>'Ativo:'
            ));
        $ativo
            ->setAttributes(array(
                'class'			=> 'tool_tip',
                'data-placement'=> 'bottom',
                'title'			=>'Ativa ou desativa o produto',
                'value'         => 1
            ));
        $this->add( $ativo );

        // senha
        $senha = new Password('senha');
        $senha->setLabel('Senha: ')
            ->setLabelAttributes ( array ('class' => 'control-label required') )
            ->setAttributes(array(
                'class'			=> 'tool_tip',
                'data-placement'=> 'bottom',
                'placeholder'	=> 'Senha',
                'title'			=> 'Senha de acesso ao painel',
                'maxlength'		=> '50',
                'id'            => 'senha'
            ));
        $this->add($senha);

        // confirmção
        $confirmacao = new Password('confirmacao');
        $confirmacao->setLabel('Confirmação: ')
            ->setLabelAttributes ( array ('class' => 'control-label required') )
            ->setAttributes(array(
                'class'			=> 'tool_tip',
                'data-placement'=> 'bottom',
                'placeholder'	=> 'Confirme a senha',
                'title'			=> 'Confirme a senha',
                'maxlength'		=> '50'
            ));
        $this->add($confirmacao);

// SERÁ COLOCADO DEPOIS QUE TIVER TODAS AS REGRAS CORRETAS
        // regra *************************** REGRA (SELECT) *************************
//        $this->add(array(
//            'type'       => 'DoctrineModule\Form\Element\ObjectSelect',
//            'name'       => 'regra',
//            'options'    => array(
//                'disable_inarray_validator' => true,
//                'label'              => 'Categoria:',
//                'empty_option'       => 'Selecione o papel',
//                'target_class'       => Entities::ENTITY_REGRA,
//                'label_generator'    => function ($entity) {
//                        return $entity->getNome();
//                 },
//                'find_method'     => array(
//                    'name'   => 'findBy',
//                    'params' => array(
//                        'criteria' => array(
//                            'master' => 0,
//                        ),
//                    )
//                ),
//            ),
//            'attributes' => array(
//                'placeholder' => '',
//                'title'       => 'Papel do usuário',
//                'class'       => 'tool_tip',
//                'id'          => 'regra'
//            ),
//        ));
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array('type' => 'Admin\Form\InputFilter\UsuarioFilter');
    }
}