<?php
/**
 * Class AlterarSenhaFilter
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Admin\Form\InputFilter;

use Doctrine\Common\Persistence\ObjectRepository;
use Servidor\Entity\Usuario;
use Zend\InputFilter\InputFilter;

class AlterarSenhaFilter extends InputFilter
{

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    private $objectRepository;

    /**
     * @var \Servidor\Entity\Usuario
     */
    private $usuario;


    public function __construct(ObjectRepository $repository, Usuario $usuario)
    {
        $this->objectRepository = $repository;
        $this->usuario = $usuario;
    }

    /**
     * @method init
     */
    public function init() {

        // senha atual ################# SENHA ATUAL (TEXT) ####################
        $this->add(array(
            'name' => 'senhaAtual',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                ),
                array(
                    'name' => 'Admin\Validator\SenhaUsuarioExists',
                    'options' => array(
                        'object_repository' => $this->objectRepository,
                        'fields'  => array('senha'),
                        'usuario' => $this->usuario
                    )
                )
            )
        ));

        // senha ####################### SENHA (TEXT) ##########################
        $this->add(array(
            'name' => 'senha',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                )
            )
        ));

        // confirmacao ################ COFIRMACAO (TEXT) #######################
        $this->add(array(
            'name' => 'confirmacao',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                ),
                array(
                    'name' => 'identical',
                    'options' => array(
                        'token' => 'senha'
                    )
                )
            )
        ));
    }
} 