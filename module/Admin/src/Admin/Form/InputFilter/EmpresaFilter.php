<?php
namespace Admin\Form\InputFilter;

use Zend\InputFilter\InputFilter;

class EmpresaFilter extends InputFilter
{
	
	public function init() {
		
		// nome ########################## NOME (TEXT) ################################
		$this->add(array(
            'name' => 'nome',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                            'encoding' => 'UTF-8',
                            'max'      => 70
                    )
                ),
                array(
                    'name' => 'NotEmpty'
                )
            )
		));
		
		// slogan #################### SLOGAN (TEXT) #################################
		$this->add(array(
            'name' => 'slogan',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                            'encoding' => 'UTF-8',
                            'max'      => 255
                    )
                ),
                array(
                    'name' => 'NotEmpty'
                )
            )
		));

        $this->add(array(
            'name' => 'cep',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max'      => 9
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'logradouro',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max'      => 200
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'numero',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max'      => 6
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'bairro',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max'      => 60
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'cidade',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max'      => 60
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'uf',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max'      => 2
                    )
                )
            )
        ));

        // email ######################### EMAIL (TEXT) ################################
        $this->add(array(
            'name' => 'email',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array('name' => 'NotEmpty'),
                array(
                    'name'    => 'StringLength',
                    'options' => array(
                        'max'      => 70
                    )
                ),
                array('name' => 'EmailAddress')
            )
        ));

        $this->add(array(
            'name' => 'ddd',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags')
            ),
            'validators' => array(
                array('name'    => 'Digits')
            )
        ));

        $this->add(array(
            'name' => 'telefone',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max'      => 25
                    )
                )
            )
        ));

        // analytics #################### ANALYTICS (TEXT) #################################
        $this->add(array(
            'name' => 'analytics',
            'required' => false,
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max'      => 20
                    )
                )
            )
        ));

        // quem_somos #################### QUEM SOMOS (TEXTAREA) #################################
        $this->add(array(
            'name' => 'quemSomos',
            'required' => true
        ));

        // texto_contato ############# TEXTO CONTATO (TEXTAREA) #################################
        $this->add(array(
            'name' => 'textoContato',
            'required' => true
        ));

        // texto_rodape ############# TEXTO RODAPE (TEXTAREA) #################################
        $this->add(array(
            'name' => 'textoRodape',
            'required' => true
        ));

        // facebook #################### FACEBOOK (TEXT) #################################
        $this->add(array(
            'name' => 'facebook',
            'required' => false,
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max'      => 255
                    )
                )
            )
        ));

        // twitter #################### TWITTER (TEXT) #################################
        $this->add(array(
            'name' => 'twitter',
            'required' => false,
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max'      => 255
                    )
                )
            )
        ));

        // youtube #################### YOUTUBE (TEXT) #################################
        $this->add(array(
            'name' => 'youtube',
            'required' => false,
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max'      => 255
                    )
                )
            )
        ));

        // googlemais #################### GOOGLE MAIS (TEXT) #################################
        $this->add(array(
            'name' => 'googlemais',
            'required' => false,
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max'      => 255
                    )
                )
            )
        ));

	}
	
}