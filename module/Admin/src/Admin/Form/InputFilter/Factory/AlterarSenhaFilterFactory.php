<?php
/**
 * Class AlterarSenhaFilterFactory
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Admin\Form\InputFilter\Factory;

use Servidor\Entity\Entities;
use Admin\Form\InputFilter\AlterarSenhaFilter;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AlterarSenhaFilterFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocator = $serviceLocator->getServiceLocator();
        $objectManager   = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $objectRepository= $objectManager->getRepository(Entities::ENTITY_USUARIO);

        $router = $serviceLocator->get('Router');
        $request = $serviceLocator->get('Request');
        $routerMatch = $router->match($request);

        $usuario = $objectRepository->find($routerMatch->getParam('id'));
        return new AlterarSenhaFilter( $objectRepository, $usuario );
    }
} 