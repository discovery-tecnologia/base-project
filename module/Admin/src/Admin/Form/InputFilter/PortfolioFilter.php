<?php
namespace Admin\Form\InputFilter;

use Zend\InputFilter\InputFilter;

class PortfolioFilter extends InputFilter
{
	
	public function init() {
		
		// titulo ########################## TITULO (TEXT) ################################
		$this->add(array(
            'name' => 'titulo',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                            'encoding' => 'UTF-8',
                            'max'      => 70
                    )
                ),
                array(
                    'name' => 'NotEmpty'
                )
            )
		));

        // descricao #################### DESCRICAO (TEXTAREA) #################################
        $this->add(array(
            'name' => 'descricao',
            'required' => true
        ));
	}
	
}