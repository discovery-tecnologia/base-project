<?php
namespace Admin\Form\InputFilter;

use Zend\InputFilter\InputFilter;

class UsuarioFilter extends InputFilter {
	
	public function init() {

        // nome ######################## NOME (TEXT) #############################
		$this->add(array(
            'name' => 'nome',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                )
            )
		));

        $this->add(array(
            'name'     => 'ativo',
            'required' => false
        ));

        // email ######################## EMAIL (TEXT) ############################
		$this->add(array(
            'name' => 'email',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                ),
                array(
                    'name' => 'EmailAddress'
                )
            )
		));

        // nome ######################## SENHA (PASSWORD) ##########################
		$this->add(array(
            'name' => 'senha',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                )
            )
		));

        // confirmacao ################# CONFIRMACAO (TEXT) #########################
		$this->add(array(
            'name' => 'confirmacao',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                ),
                array(
                    'name' => 'identical',
                    'options' => array(
                        'token' => 'senha'
                    )
                )
            )
		));

        // SERÁ COLOCADO DEPOIS QUE TIVER TODAS AS REGRAS CORRETAS
		// roles ################### ROLES (SELECT) #################################
//		$this->add(array(
//            'name' => 'regra',
//            'required' => true,
//            'validators' => array(
//                array(
//                    'name' => 'NotEmpty',
//                    'break_chain_on_failure' => true
//                ),
//            ),
//		));
		
	}
	
}