<?php
namespace Admin\Form;

use Zend\Form\Form;
use Servidor\Entity\Entities;
use Zend\Form\Element\Csrf;

class Portfolio extends Form {
	
    
   	public function init() {
      
        parent::__construct('cadastro-portfolio');
        $this->setAttribute('method', 'post');
        
        $this->add(array(
        		'type'     => 'Admin\Form\Fieldset\PortfolioFieldset',
        		'name'     => 'portfolio',
        		'hydrator' => 'DoctrineModule\Stdlib\Hydrator\DoctrineObject',
        		'object'   => Entities::ENTITY_PORTFOLIO,
        		'options' => array(
        				'use_as_base_fieldset' => true
        		)
        ));

       /* $csrf = new Csrf('security');
        $csrf->setOptions( array (
            'csrf_options'  =>  array (
                'timeout'  =>  900
            )) );
        $this->add($csrf);*/

    }
		
}