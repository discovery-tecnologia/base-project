<?php
namespace Admin\Form;

use Servidor\Entity\Entities;
use Zend\Form\Element\Csrf;
use Zend\Form\Form;

class Servico extends Form
{

	public function init() {
	    
		parent::__construct('cadastro-servico');
		$this->setAttribute('method', 'post');

		$this->add(array(
				'type'     => 'Admin\Form\Fieldset\ServicoFieldset',
				'name'     => 'servico',
				'hydrator' => 'DoctrineModule\Stdlib\Hydrator\DoctrineObject',
				'object'   => Entities::ENTITY_SERVICO,
				'options' => array(
						'use_as_base_fieldset' => true
				)
		));


        $csrf = new Csrf('security');
        $csrf->setOptions( array (
            'csrf_options'  =>  array (
                'timeout'  =>  900
            )) );
        $this->add($csrf);
	}

}