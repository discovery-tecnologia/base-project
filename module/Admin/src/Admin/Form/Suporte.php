<?php
namespace Admin\Form;

use Admin\Form\InputFilter\SuporteFilter;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Text;
use Zend\Form\Element\Textarea;
use Zend\Form\Form;

class Suporte extends Form {


    public function init() {

        parent::__construct('suporte');
        $this->setAttribute('method', 'post');

        $this->setInputFilter(new SuporteFilter());

        // assunto ************************* ASSUNTO (TEXT) **********************
        $assunto = new Text( 'assunto' );
        $assunto->setLabel ( 'Assunto: ' )->setLabelAttributes ( array (
            'class' => 'control-label required'
        ) )->setAttributes ( array (
                'class' => 'tool_tip',
                'data-placement' => 'left',
                'placeholder' => 'Assunto da mensagem',
                'title' => 'Assunto da mensagem',
                'id' => 'nome',
                'maxlength' => '150'
            ) );
        $this->add ( $assunto );

        // mensagem **************** MENSAGEM (TEXTAREA) **************
        $mensagem = new Textarea( 'mensagem' );
        $mensagem->setLabel ( 'Mensagem:' )->setLabelAttributes ( array (
            'class' => 'control-label required'
        ) )->setAttributes ( array (
                'placeholder' => 'Mensagem',
                'class' => 'jmce tool_tip'
            ) );
        $this->add ( $mensagem );

        $csrf = new Csrf('security');
        $csrf->setOptions( array (
            'csrf_options'  =>  array (
                'timeout'  =>  900
            )) );
        $this->add($csrf);
    }

}