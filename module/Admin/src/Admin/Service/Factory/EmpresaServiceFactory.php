<?php
/**
 * Class LojaServiceFactory
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Admin\Service\Factory;

use Admin\Service\EmpresaService;
use Servidor\Entity\Entities;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class EmpresaServiceFactory implements FactoryInterface
{

    /**
     * (non-PHPdoc)
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService( ServiceLocatorInterface $serviceLocator ) {

        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $objectRepository = $objectManager->getRepository(Entities::ENTITY_EMPRESA);

        return new EmpresaService( $objectManager, $objectRepository );
    }
}