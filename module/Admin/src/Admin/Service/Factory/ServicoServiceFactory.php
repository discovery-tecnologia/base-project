<?php
/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 19/03/14
 * Time: 19:07
 */

namespace Admin\Service\Factory;

use Admin\Service\ServicoService;
use Servidor\Entity\Entities;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ServicoServiceFactory implements FactoryInterface {

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $objectManager    = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $objectRepository = $objectManager->getRepository(Entities::ENTITY_SERVICO);

        return new ServicoService( $objectManager, $objectRepository );
    }
}