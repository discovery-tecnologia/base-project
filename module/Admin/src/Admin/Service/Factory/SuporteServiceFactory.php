<?php
/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 27/03/14
 * Time: 20:15
 */

namespace Admin\Service\Factory;

use Admin\Service\SuporteService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class SuporteServiceFactory
 * @package Admin\Service\Factory
 *
 * @author DiegoWagner <desenvolvimento@discoverytecnologia.com.br>
 */
class SuporteServiceFactory implements FactoryInterface {

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new SuporteService(
            $serviceLocator->get('Base\Mail\Transport'),
            $serviceLocator->get('View')
        );
    }
}