<?php
namespace Admin\Service;

use Base\Service\AbstractService;

/**
 * Class PortfolioService
 * @package Admin\Service
 *
 * @author DiegoWagner <desenvolvimento@discoverytecnologia.com.br>
 */
class PortfolioService extends AbstractService {}