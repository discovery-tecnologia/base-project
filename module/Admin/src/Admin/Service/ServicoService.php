<?php
namespace Admin\Service;

use Base\Service\AbstractService;

/**
 * Class ServicoService
 * @package Admin\Service
 *
 * @author DiegoWagner <desenvolvimento@discoverytecnologia.com.br>
 */
class ServicoService extends AbstractService {}