<?php
/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 27/03/14
 * Time: 20:14
 */

namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\Common\Persistence\ObjectManager;
use Servidor\Entity\Loja;
use Doctrine\ORM\EntityManager;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Base\Mail\Mail;


/**
 * Class SuporteService
 * Responsavel por criar o servico de envio de email no ADMIN para os administradores do sistema para efetuar suportes
 * @package Admin\Service
 *
 * @author DiegoWagner <desenvolvimento@discoverytecnologia.com.br>
 */
class SuporteService {


    /**
     *
     * @var SmtpTransport
     */
    protected $transport;

    /**
     *
     * @var ViewModel
     */
    protected $view;


    /**
     * __construct()
     * @param EntityManager $em
     * @param SmtpTransport $transport
     * @param ViewModel $view
     */
    public function __construct(SmtpTransport $transport, $view) {

        $this->transport     = $transport;
        $this->view          = $view;
    }


    /**
     * Envia um email para os administradores do sistema para suporte
     *
     * @method sendMail()
     * @param Loja $loja
     * @param array $dados
     * @return bool
     * @throws \Exception
     *
     */
    public function sendMail( array $data ) {

        if ( !empty($data) ) {
            try {

                $mail = new Mail($this->transport, $this->view, 'suporte');
                $mail->setSubject('Email de contato do admin')
                     ->setTo(Mail::MAIL_ADMIN)
                     ->setData($data)
                     ->prepare()
                     ->send();   // envia o email.

                return true;

            } catch(\Exception $e) {

                throw $e;
            }

        } else
            throw new \Exception('Não foi possivel recuperar os dados para enviar o email');
    }

} 