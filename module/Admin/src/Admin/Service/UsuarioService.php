<?php
/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 19/03/14
 * Time: 19:03
 */

namespace Admin\Service;

use Acl\Auth\Adapter;
use Base\Service\AbstractService;
use Servidor\Entity\Entities;
use Servidor\Entity\Usuario;
use Servidor\Entity\Loja;
use Servidor\Entity\Regra;
use Servidor\Interfaces\ObjectEntity;

/**
 * Class UsuarioService
 * @package Admin\Service
 *
 * @author DiegoWagner <desenvolvimento@discoverytecnologia.com.br>
 */
class UsuarioService extends AbstractService {


    /**
     * Retorna o objeto referência do usuário Logado
     *
     * @method getUsuarioLogado()
     * @return mixed
     */
    public function getUsuarioLogado()
    {
        $adapter = new Adapter();
        return $this->getReference(Entities::ENTITY_USUARIO, $adapter->getUserId());
    }

    /**
     * Retorna todos os usuários a serem iterados
     * Exclui o usuario desenvolvimento@discoverytecnologia.com.br
     * @return mixed
     */
    public function findAllUsers() {
        return $this->repository->findAllUsers();
    }

    /**
     * Persiste o usuário e salva como padrão o papel administrador
     *
     * @override
     * @method save(Usuario $user)
     * @param Usuario $user
     * @return \Base\Service\AbstractEntity
     */
    public function save( ObjectEntity $user )
    {
        $regra = $this->getReference(Entities::ENTITY_REGRA, Regra::ADMINISTRADOR );
        $user->setRegra($regra);

        return parent::save($user);
    }
} 