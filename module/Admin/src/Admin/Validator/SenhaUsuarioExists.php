<?php
/**
 * Class SenhaUsuarioExists
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Admin\Validator;

use DoctrineModule\Validator\NoObjectExists;
use Zend\Validator\Exception\InvalidArgumentException;

class SenhaUsuarioExists extends NoObjectExists
{

    /**
     * Error constants
     */
    const ERROR_OBJECT_FOUND    = 'objectFound';

    /**
     * @var array Message templates
     */
    protected $messageTemplates = array(
        self::ERROR_OBJECT_FOUND  => "Senha incorreta",
    );

    /**
     * @var \Servidor\Entity\Usuario
     */
    private $usuario;


    public function __construct(array $options)
    {
        if(! isset($options['usuario']))
            throw new InvalidArgumentException("Não foi possível recuperar a instância do Usuário");

        $this->usuario = $options['usuario'];
        parent::__construct($options);
    }

    /**
     * {@inheritDoc}
     */
    public function isValid($value)
    {
        $value = $this->cleanSearchValue($value);
        $value['id']   = $this->usuario;
        $value['senha']= $this->usuario->encryptSenha($value['senha']);

        $match = $this->objectRepository->findOneBy($value);

        if (is_object($match)) {
            return true;
        }
        $this->error(self::ERROR_OBJECT_FOUND, $value);
        return false;
    }
}
