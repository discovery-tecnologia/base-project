<?php
namespace Base;

use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

class Module {

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    
    public function getViewHelperConfig() {
    	return array(
    		'factories' => array(
    			'flashMessage' => function($sm) {
    
    				$flashmessenger = $sm->getServiceLocator()
					    				 ->get('ControllerPluginManager')
					    				 ->get('flashmessenger');
    
    				$messages = new View\Helper\FlashMessages();
    				$messages->setFlashMessenger($flashmessenger); 
    						
    				return $messages;

    			},
    			'fileUpload' => function($sm) {
    				
    				$fileUpload = new View\Helper\FileUpload();
    				return $fileUpload;
    			}
    		),
    	);
    }
    
    
    
    public function getServiceConfig() {
    	return array(
    			'factories' => array(
    					'Base\Mail\Transport' => function($sm) {
    						
    						$config = $sm->get('Config');
    					
    						$transport = new SmtpTransport;
    						$options = new SmtpOptions($config['mail']);
    						$transport->setOptions($options);
    					
    						return $transport;
    					}
    			)
    	);
    }
}
