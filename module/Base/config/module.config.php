<?php
namespace Base;

use Base\View\Helper\HBooleanToStatus;
use Base\View\Helper\HCommonMask;
use Base\View\Helper\HImages;
use Base\View\Helper\HIntToAlertStatus;
use Base\View\Helper\HStringToKeyWords;
use Base\View\Helper\HStringToSlug;
use Base\View\Helper\HUrlYouTubeToId;
use Base\View\Helper\HTipoPreco;
use Base\View\Helper\HEstadoProduto;
use Base\View\Helper\HPreco;
use Base\View\Helper\FormButton;


return array(
    'view_helpers' => array(
        'factories' => array(
            'HMenuAtivo'     => 'Base\View\Helper\Factory\HMenuAtivoFactory',
            'HAbsoluteUrl'   => 'Base\View\Helper\Factory\HAbsoluteUrlFactory',
            'HEnderecoByCep' => 'Base\View\Helper\Factory\HEnderecoByCepFactory',
            'HMaskCpfCnpj'   => 'Base\View\Helper\Factory\HMaskCpfCnpjFactory',
            'HMaskTelefone'  => 'Base\View\Helper\Factory\HMaskTelefoneFactory',
            'alertMessage'   => 'Base\View\Helper\Factory\AlertMessageFactory'
        ),
        'invokables' => array(
            'HStringToSlug'		=> new HStringToSlug(),
            'HStringToKeyWords'	=> new HStringToKeyWords(),
            'HImages' 			=> new HImages(),
            'HCommonMask'		=> 'Base\View\Helper\HCommonMask',
            'HBooleanToStatus'	=> new HBooleanToStatus(),
            'HIntToAlertStatus'	=> new HIntToAlertStatus(),
            'HTipoPreco'	    => new HTipoPreco(),
            'HEstadoProduto'    => new HEstadoProduto(),
            'HPreco'	    	=> new HPreco(),
            'form_button'       => new FormButton()
        )
    ),
    /** VIEW MANAGERS **/
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'exception_template'       => 'base/error/index',
        'template_map' => array(
            'layout/base'      => __DIR__ . '/../view/layout/base.phtml',
            'base/error/index' => __DIR__ . '/../view/error/index.phtml',
            'base/partial/upload' => __DIR__ . '/../view/partial/file-upload.phtml'
        ),
        'template_path_stack' => array(
            'base' => __DIR__ . '/../view',
        )
    ),
);
