<?php
namespace Base\Controller;

use Zend\View\Exception\RuntimeException;
use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel;
use Zend\Form\Form;


abstract class AbstractSiteController extends AbstractActionController {
	
	protected $em;
	protected $service;
	protected $entity;
	protected $form;
	protected $route;
	protected $controller;
	protected $namespace;
	protected $pasteView;
	protected $imageFolder;

	/**
	 * @method indexAction()
	 * Responsável por fazer listagem de dados.
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 * @access public
	 */
	public function indexAction() {
		
		try {
			
			$list = $this->getEntityManager()
			             ->getRepository( $this->entity )
			             ->findAll();
			
			
			$messages = $this->flashMessenger()
			 				 ->setNamespace($this->namespace)
			 				 ->getMessages();
			
			try {
				
				return new ViewModel(array(
						               'data' => $list,
						               'messages' => $messages
						             )
						    );
			} catch ( RuntimeException $r ) {
				
				$this->flashMessenger()
					 ->setNamespace( $this->namespace )
					 ->addMessage( array(
							'error' => $r->getMessage(),
							'info'  => $r->getTraceAsString()
					 ) );
					 
				return $this->redirect()->toRoute(
										 		$this->route,
										 		array( 'controller' => $this->controller
				 		                  ) );
			}
		
		} catch( \Exception $e ) {
				
		       $this->flashMessenger()
					->setNamespace( $this->namespace )
					->addMessage( array(
							'error' => $e->getMessage(),
							'info' => $e->getTraceAsString()
					) );
					
			   return $this->redirect()->toRoute(
												$this->route,
												array( 'controller' => $this->controller
										 ) );
		}
		
	}
	
		
	
	/**
	 * @method viewAction()
	 * Visualiza um registro
	 * @return \Zend\View\Model\ViewModel
	 * @access public
	 */
	public function viewAction() {
	
		try {
			
			$id = $this->params()->fromRoute('id',0);
			$form = new $this->form();
			
			$this->disableFormFields( $form );
		
			if($id) {
								
				$entity = $this->getEntityManager()->getReference($this->entity, $id);

                if ( $entity )
                    $form->setData( $entity->toArray() );
				
				try {						
				
					return $this->renderView( array(
							'form' => $form
					) );
				
				} catch ( RuntimeException $r ) {
				
					$this->flashMessenger()
						 ->setNamespace( $this->namespace )
						 ->addMessage( array(
								'error' => $r->getMessage(),
								'info'  => $r->getTraceAsString()
					) );
						 
				    return $this->redirect()->toRoute(
											 		$this->route,
											 		array( 'controller' => $this->controller
				 		                      ) );
				}		
			}
		
		} catch( \Exception $e ) {
		
			$fm = $this->flashMessenger()
					   ->setNamespace( $this->namespace )
					   ->addMessage( array(
								'error' => $e->getMessage(),
								'info' => $e->getTraceAsString()
					   ) );
					   
		    return $this->redirect()->toRoute(
									   		$this->route,
									   		array( 'controller' => $this->controller
		   		                       ) );
		}
	}
	
	
		
	/**
	 * Seta o script.phtml que está em outro lugar.
	 * @return ViewModel
	 * @access protected
	 */
	protected function renderView( Array $vars, $disableLayout = null ) {
	
		$view = new ViewModel();
	
		$rotulos = array('new' =>'Novo Registro',
				         'edit'=>'Editar Registro');
	
		$actionName = $this->getEvent()->getRouteMatch()->getParam('action', 'NA');
		$view->setTemplate( $this->pasteView . '/' . $this->controller . '/crud' );
		
		if($disableLayout) // desabilita o layout
			$view->setTerminal( true );
	
		$vars['action']	 = $actionName;
		$vars['rotuloAction']	= $rotulos[$actionName];
	
		return $view->setVariables($vars);
	
	}

	/**
	 * Seta os campos do formulário como desabilitado
	 * @param Form $form
	 * @access protected
	 */
	protected function disableFormFields( Form $form ) {
		
		$elements = $form->getElements();
			
		foreach ( $elements as $element ) {

			$element->setAttributes(array ( 'readonly' => true, 'disabled' => true ) );
		}
	}
}
