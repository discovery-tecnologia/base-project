<?php
/**
 * Class InvalidInstanceException
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Base\Exception;

/**
 * Class InvalidInstanceException
 * Esta classe irá concentrar todas as exceptions de falhas de instâncias de Objetos
 *
 * @package Base\Exception
 * @author DiegoWagner <diegowagner4@gmail.com>
 *
 * @see \Exception
 * @throws \Excetion
 */
class NotImplementedException extends \Exception
{
} 