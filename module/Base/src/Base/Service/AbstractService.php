<?php
namespace Base\Service;

use Base\Exception\NotImplementedException;
use Base\Util\Files;

use Acl\Auth\Adapter;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Servidor\Interfaces\ObjectEntity;
use Zend\Stdlib\Hydrator\ClassMethods;

/**
 * @class AbstractService
 * 
 * @package Base\Service
 * @author Diego
 *
 */
abstract class AbstractService {
	
	/**
	 * @var ObjectManager
	 */
	protected $objectManager;

    /**
     * @var ObjectRepository
     */
    protected $repository;
	
	
	/**
	 * Dependency Injection
	 * @param EntityManager $objectManager
	 */
	public function __construct( ObjectManager $objectManager, ObjectRepository $objectRepository ) {

		$this->objectManager = $objectManager;
        $this->repository = $objectRepository;
	}


    /**
     * Retorna as entidades a serem iteradas
     *
     * @method findAll()
     * @return array Object
     */
    public function findAll() {
        return $this->repository->findAll();
    }


    /**
     * Retorna um object pelos criterios passados por parametro
     *
     * @method findOneBy( array $criteria )
     * @param array $criteria
     * @return object
     */
    public function findOneBy( array $criteria ) {
        return $this->repository->findOneBy( $criteria );
    }


    /**
     * Retorna apenas uma entidade pelo seu ID
     *
     * @method find($id)
     * @param $id
     * @return object
     */
    public function find($id) {
        return $this->repository->find($id);
    }


    /**
     * Retorna a referencia do objeto passado por parâmetro
     *
     * @method getReference()
     * @param string $entityClass
     * @param int $id
     * @return mixed
     */
    public function getReference($entityClass, $id) {
        return $this->objectManager->getReference($entityClass, $id);
    }
	
	
	/**
	 * @method save()
	 * Método da classe abstrata para inserido ou alteração de entidades
	 * @param array $data
	 * @return AbstractEntity $entity
	 */
	public function save( ObjectEntity $entity ) {

        // BEGIN TRANSACTION
        $this->objectManager->getConnection()->beginTransaction();

        try {

            if ( null == $entity->getId() )
                $this->objectManager->persist($entity);

            $this->objectManager->flush();
            // COMMIT
            $this->objectManager->getConnection()->commit();

            // renomeia a pasta de imagens
            $this->renameImageFolder($entity);

            return $entity;

        } catch ( \Exception $e ) {

            // ROLLBACK
            $this->objectManager->getConnection()->rollback();
            $this->objectManager->close();
            throw new \Exception("ERRO: Ao salvar os dados no banco \r" . $e->getMessage());
        }
	}
		
	
	
	
	/**
	 * @method delete()
	 * Método da classe abstrata para excluir entidades
	 * @param $entity
	 * @return int $id
	 */
	public function remove( ObjectEntity $entity ) {

        // BEGIN TRANSACTION
        $this->objectManager->getConnection()->beginTransaction();

        try {

            $id = $entity->getId();
            // remove
            $this->objectManager->remove($entity);
            $this->objectManager->flush();
            // COMMIT
            $this->objectManager->getConnection()->commit();

            // remove as imagens
            $dir = $this->imagesPath . $id;

            if( is_dir($dir) ){

                $files = new Files();
                $files->removeDirectory($dir);
            }

            return $id;

        } catch ( \Exception $e ) {

            // ROLLBACK
            $this->objectManager->getConnection()->rollback();
            $this->objectManager->close();
            throw new \Exception("ERRO: Ao remover os dados no banco \r" . $e->getMessage());
        }


	}
	
	
	
	/**
	 * renameImageFolder
     *
	 * @param $entity
	 */
	protected function renameImageFolder( ObjectEntity $entity ) {

        if ( method_exists ($entity , 'getImageFolders') ) {

            // renomeia a pasta de imagens para o caminho definitivo
            $adapter = new Adapter();

            $lastFolderCapa = 'new_' . $adapter->getUserId() . '_capa';
            $lastFolder = 'new_' . $adapter->getUserId();

            $folders = $entity->getImageFolders();

            foreach ($folders as $folder) {

                //var_dump($folder.$lastFolder);
                //var_dump(is_dir($folder.$lastFolder));die;
                // verifica se a pasta existe
                if(is_dir($folder.$lastFolderCapa)){
                    // renomeia a pasta de imagens
                    rename($folder.$lastFolderCapa, $folder.$entity->getId().'_capa');
                }

                // verifica se a pasta existe
                if(is_dir($folder.$lastFolder)){
                    // renomeia a pasta de imagens
                    rename($folder.$lastFolder, $folder.$entity->getId());
                }

            }

        } else {
            throw new NotImplementedException('Método getImageFolders não implementado na entidade');
        }


	}
}
?>