<?php

namespace Base\Validator;

use Zend\Validator\AbstractValidator;
use DateTime;
use Zend\Stdlib\StringUtils;
use Zend\Stdlib\StringWrapper\StringWrapperInterface as StringWrapper;

class IdadeValidator extends AbstractValidator
{
    const INVALID        = 'dateInvalid';
    const INVALID_DATE   = 'dateInvalidDate';
    const FALSEFORMAT    = 'dateFalseFormat';
    const EXCEEDED_AGE   = 'exceededAge';
    const LOWER_AGE      = 'lowerAge';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $messageTemplates = array(
        self::INVALID        => "Tipo de dado inválido. É esperado String, integer, matriz ou DateTime.",
        self::INVALID_DATE   => "Insira uma data válida.",
        self::FALSEFORMAT    => "Insira uma data válida no formato '%format%'.",
        self::EXCEEDED_AGE   => "É preciso ter no máximo '%max%' anos.",
        self::LOWER_AGE     => "É preciso ter no mínimo '%min%' anos."
    );

    /**
     * @var array
     */
    protected $messageVariables = array(
        'format'  => array('options' => 'format'),
        'min'     => array('options' => 'min'),
        'max'     => array('options' => 'max'),
    );

    protected $options = array(
        'format'   => 'd/m/Y',
        'min'      => 0,       // Minimum length
        'max'      => null,    // Maximum length, null if there is no length limitation
    );

    /**
     * Optional format
     *
     * @var string|null
     */
    protected $format;

    /**
     * Sets validator options
     *
     * @param  string|array|Traversable $options OPTIONAL
     */
    public function __construct($options = array()) {

        if (!is_array($options)) {
            $options     = func_get_args();

            $temp['format'] = array_shift($options);

            if (array_key_exists('format', $options)) {
                $this->setFormat($options['format']);
            }

            if (!empty($options)) {
                $temp['max'] = array_shift($options);
            }

            $options = $temp;
        }

       //var_dump($options);

        parent::__construct($options);
    }

    /**
     * Returns the format option
     *
     * @return string|null
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Sets the format option
     *
     * @param  string $format
     * @return Date provides a fluent interface
     */
    public function setFormat($format = null)
    {
        $this->format = $format;
        return $this;
    }

    /**
     * Returns true if $value is a valid date of the format YYYY-MM-DD
     * If optional $format is set the date format is checked
     * according to DateTime
     *
     * @param  string|array|int|DateTime $value
     * @return bool
     */
    public function isValid($value) {

        if (!is_string($value)
            && !is_array($value)
            && !is_int($value)
            && !($value instanceof DateTime)
        ) {
            $this->error(self::INVALID);
            return false;
        }

        $this->setValue($value);

        $format = $this->getFormat();

        if ($value instanceof DateTime) {

            return $this->verificarIdade($value);

        } elseif (is_int($value)
            || (is_string($value) && null !== $format)
        ) {
            $date = (is_int($value))
                ? date_create("@$value") // from timestamp
                : DateTime::createFromFormat($format, $value);

            // Invalid dates can show up as warnings (ie. "2007-02-99")
            // and still return a DateTime object
            $errors = DateTime::getLastErrors();

            if ($errors['warning_count'] > 0) {
                $this->error(self::INVALID_DATE);
                return false;
            }
            if ($date === false) {
                $this->error(self::INVALID_DATE);
                return false;
            }
        } else {
            if (is_array($value)) {
                $value = implode('-', $value);
            }

            if (!preg_match('/^\d{4}-\d{2}-\d{2}$/', $value)) {
                $this->format = 'Y-m-d';
                $this->error(self::FALSEFORMAT);
                $this->format = null;
                return false;
            }

            list($year, $month, $day) = sscanf($value, '%d-%d-%d');

            if (!checkdate($month, $day, $year)) {
                $this->error(self::INVALID_DATE);
                return false;
            }
        }

        return $this->verificarIdade($value);

    }



    private function verificarIdade($value) {

        $dataValue = explode('/', $value);

        $dd 	= $dataValue[0];
        $mm 	= $dataValue[1];
        $aaaa   = $dataValue[2];

        $dataAtual = explode('/', date('d/m/Y'));

        $dia 	= $dataAtual[0];
        $mes 	= $dataAtual[1];
        $ano    = $dataAtual[2];

        if($this->min){
            // verifica a idade minima permitida
            if($ano - $aaaa < $this->min){
                $this->error(self::LOWER_AGE);
                return false;
            }
            if($ano - $aaaa == $this->min && $mm > $mes){
                $this->error(self::LOWER_AGE);
                return false;
            }
            if($ano - $aaaa == $this->min && $mm == $mes && $dd > $dia){
                $this->error(self::LOWER_AGE);
                return false;
            }
        }

        if($this->max){
            // verifica a idade maxima permitida
            if($ano - $aaaa > $this->max){
                $this->error(self::EXCEEDED_AGE);
                return false;
            }
            if($ano - $aaaa == $this->max && $mm > $mes){
                $this->error(self::EXCEEDED_AGE);
                return false;
            }
            if($ano - $aaaa == $this->max && $mm == $mes && $dd < $dia){
                $this->error(self::EXCEEDED_AGE);
                return false;
            }
        }

        return true;

    }


}
