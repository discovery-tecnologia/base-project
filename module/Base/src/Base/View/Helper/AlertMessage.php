<?php
/**
 * Class AlertMessage
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Base\View\Helper;

use Zend\Form\View\Helper\AbstractHelper;
use Zend\View\Helper\FlashMessenger;
use Zend\Mvc\Controller\Plugin\FlashMessenger as FlashMessage;

class AlertMessage extends AbstractHelper
{
    private $flashMessenger;

    public function __construct(FlashMessenger $flashMessenger)
    {
        $this->flashMessenger = $flashMessenger;
    }

    public function __invoke($includeCurrentMessages = true)
    {
        $messages = array(
            FlashMessage::NAMESPACE_ERROR   => array(),
            FlashMessage::NAMESPACE_SUCCESS => array(),
            FlashMessage::NAMESPACE_WARNING => array(),
            FlashMessage::NAMESPACE_INFO    => array(),
            FlashMessage::NAMESPACE_DEFAULT => array()
        );

        foreach ($messages as $ns => &$m) {
            $m = $this->flashMessenger->getMessagesFromNamespace($ns);
            if ($includeCurrentMessages) {
                $m = array_merge($m, $this->flashMessenger->getCurrentMessagesFromNamespace($ns));
                $this->flashMessenger->clearCurrentMessagesFromNamespace($ns);
            }
        }

        $html = null;
        foreach ($messages as $type => $typeMessages) {

            $icon = '';
            switch($type){
                case 'error':
                    $icon = "<i class='fa fa-ban'></i>";
                    break;
                case 'alert':
                    $icon = "<i class='fa fa-info'></i>";
                    break;
                case 'info':
                    $icon = "<i class='fa fa-warning'></i>";
                    break;
                case 'success':
                    $icon = "<i class='fa fa-check'></i>";
                    break;
            }

            foreach ($typeMessages as $message) {
                $html .= sprintf("<div class='alert alert-%s alert-dismissable'>%s<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> %s </div>", $type, $icon, $message);
            }
        }
        return $html;
    }
}
