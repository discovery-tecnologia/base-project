<?php 
namespace Base\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Http\Request;

/**
 * est ViewHelper retorna a URL absoluta
 * 
 * @author AndréLuiz
 *
 */
class HAbsoluteUrl extends AbstractHelper {       

protected $request;
 
    public function __construct(Request $request) {
    	
        $this->request = $request;
        
    }    
 
    public function __invoke() {
    	
        return $this->request->getUri()->normalize();
        
    } 	
    
       
}