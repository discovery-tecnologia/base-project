<?php
/**
 * Class HCommonMask
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Base\View\Helper;

use Zend\View\Helper\AbstractHelper;

class HCommonMask extends AbstractHelper
{

    /**
     * @param $value
     * @param $inputPattern
     * @param $outputPattern
     * @return string
     */
    public function __invoke($value, $inputPattern, $outputPattern)
    {
        return $this->mask($value, $inputPattern, $outputPattern);
    }

    /**
     * @param $value
     * @param $inputPattern
     * @param $outputPattern
     * @return string
     */
    public function mask($value, $inputPattern, $outputPattern)
    {
        if (!$value === true) {
            return $value;
        }

        return vsprintf($outputPattern, sscanf($value, $inputPattern));
    }
} 