<?php
namespace Base\View\Helper;

use Zend\View\Helper\AbstractHelper;

class HEstadoProduto extends AbstractHelper {

	
	public function __invoke($char) {
	
	    if($char == 'N')
	       $estado = 'Novo';
	    else 
	        $estado = 'Usado';
	        
		return $estado;
	}
	
	
}