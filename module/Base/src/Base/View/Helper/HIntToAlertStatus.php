<?php
namespace Base\View\Helper;

use Zend\View\Helper\AbstractHelper;

class HIntToAlertStatus extends AbstractHelper {
	
	public function __invoke($int) {
		
		if($int == 1) {
			return '<div class="grid_status green" title="Em dia"><span>Verde</span></div>';
		} elseif ($int == 2) {
			return '<div class="grid_status yelow" title="Vencendo"><span>Amarelo</span></div>';		
		} elseif ($int == 3) {
			return '<div class="grid_status red" title="Vencido"><span>Vermelho</span></div>';
		}	
	}	
	
}