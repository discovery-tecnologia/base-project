<?php
/**
 * Class HMaskCpfCnpj
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Base\View\Helper;

use Zend\View\Helper\AbstractHelper;

class HMaskCpfCnpj extends AbstractHelper
{

    /**
     * @var CommonMask
     */
    private $commonMask;


    function __construct(HCommonMask $commonMask)
    {
        $this->commonMask = $commonMask;
    }


    public function __invoke($number)
    {
        if (!$number === true) {
            return null;
        }

        if (strlen($number) == 14) {
            return $this->commonMask->mask($number, '%2s%3s%3s%4s%2s', '%2s.%3s.%3s/%4s-%2s');
        }

        return $this->commonMask->mask($number, '%3s%3s%3s%2s', '%3s.%3s.%3s-%2s');
    }
} 