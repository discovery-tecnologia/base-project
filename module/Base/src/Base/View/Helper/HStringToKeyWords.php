<?php
namespace Base\View\Helper;

use Zend\View\Helper\AbstractHelper;

class HStringToKeyWords extends AbstractHelper {


    /**
     * Retorna as palavras com maior numero de ocorrencias da estring passada por parametro.
     *
     * @param $str = texto da pagina, como a concatenação de titulo, subtitulo e descrição.
     * @param int $minWordLen = tamanho mínimo das palavras retornadas.
     * @param int $minWordOccurrences = Quantidade mínima de ocorrencias para ser considerada uma keyword.
     * @param int $maxWords = numero máximo de keywords retornada.
     * @return string = keywords separadas por virgula.
     */
    public function __invoke($str, $minWordLen = 3, $minWordOccurrences = 2, $maxWords = 10) {

        //var_dump($str.'<br>');
        $str = strip_tags($str);

        mb_internal_encoding('UTF-8');
        $str = mb_strtolower($str);

        //var_dump($str.'<br>');

        $str = str_replace(array("?","!","(",")",":","[","]","-","'",",",";","."), " ", $str);
        $str = str_replace(array("\n","\r"), " ", $str);
        //$str = preg_replace("/;/", "", $str);
        // $str = preg_replace("/     /", " ", $str);
        //$str = str_replace(array("  ", "   ", "    "), " ", $str);
        $str = trim(preg_replace('/\s*$/','', $str)); // remove espaços de sobra

        //var_dump($str.'<br>');

        $words = explode(" ", $str);

        //Debug::dump($words);

        // Tags are returned based on any word in text
        // remove common words from array
        //Full list of common words in the downloadable code
        $commonWords = array('a','as','e','o','os','da','das','de','do','dos','em','sem','com','so','só',
                             'pra','para', 'por', 'isso','sua','isto','esse','esses','no','na','nos','nas','uma',
                             'foi','que','então','também','seus','seu','ser','ter','esta','está','estar',
                             'eu','tu','ele','nos','nós','vos','vós','eles','era','como' );

        $words = array_udiff($words, $commonWords,'strcasecmp');
         //Debug::dump($words);

        $keywords = array();

        while(($word = array_shift($words)) !== null) {

            if(strlen($word) < $minWordLen) continue;

            if(array_key_exists($word, $keywords))
                $keywords[$word] ++;
            else
                $keywords[$word] = 1;

        }

        // ordena o array em desc pelo valor(qtd de ocorrencias)
        arsort($keywords);

        $returnKeywords = array();

        foreach ($keywords as $word => $occurrences) {

            if($occurrences < $minWordOccurrences) break;
            $returnKeywords[] = $word;
        }

        //Debug::dump($returnKeywords);

        $returnKeywords = array_slice($returnKeywords, 0, $maxWords);
        $returnKeywords = implode(', ', $returnKeywords);

        return $returnKeywords;
    }

	
}