<?php
namespace Base\View\Helper;

use Zend\View\Helper\AbstractHelper;

class HStringToSlug extends AbstractHelper {

	
	public function __invoke($slug) {
	
		setlocale(LC_ALL, 'pt_BR.UTF8');
		return $this->toAscii($slug);
	}
	
	
	/**
	 * Retorna a string no formato SLUG
	 * @param String $str
	 * @param string $delimiter
	 * @return String
	 */
	private function toAscii($str, $delimiter='-') {
		
		$clean = iconv ('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower( trim($clean, '-') );
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
		
		return $clean;
	}
	
}