<?php
return [
    'logger' => [
        'options' => [
            'path'     => './data/logs/',
            'filename' => 'log_'.date('mY').'.txt'
        ],
        'writer' => [
            'name' => 'Zend\Log\Writer\Stream'
        ],
        'formatter' => [
            'name' => 'Zend\Log\Formatter\Simple'
        ]
    ],
    'service_manager' => [
        'factories' => [
            'Log\Service\LoggerService' => 'Log\Service\Factory\LoggerServiceFactory'
        ]
    ],
];