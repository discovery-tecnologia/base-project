<?php
/**
 * Class LoggerConfigException
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Log\Exception;

use Exception;

class LoggerConfigException extends Exception
{
} 