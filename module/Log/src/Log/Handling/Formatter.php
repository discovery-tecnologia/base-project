<?php
/**
 * Class Formatter
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Log\Handling;

use Log\Exception\LoggerConfigException;
use Zend\Log\Formatter\Simple;

class Formatter extends LoggerConfig
{
    /**
     * @return \Zend\Log\Formatter\FormatterInterface|Simple
     * @throws \Log\Exception\LoggerConfigException
     */
    public function getFormatter()
    {
        // Writer default
        if (!isset($this->config[static::CONFIG]['formatter'])) {
            return new Simple(Simple::DEFAULT_FORMAT);
        }
        $config = $this->config[static::CONFIG];
        if (! isset($config['formatter']['name'])) {
            throw new LoggerConfigException('Não foi possível recuperar o nome do Formatter');
        }
        $class = $config['formatter']['name'];
        $options = $this->getOptions($config);
        /** @var \Zend\Log\Formatter\FormatterInterface $formatter */
        $formatter = new $class($options);
        return $formatter;
    }
} 