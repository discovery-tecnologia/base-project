<?php
/**
 * Class AbstractLogger
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Log\Handling;

use Log\Exception\LoggerConfigException;

abstract class LoggerConfig {

    const CONFIG = 'logger';

    protected $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param $config
     * @return string
     * @throws LoggerConfigException
     */
    protected function getOptions($config)
    {
        if (isset($config['options'])) {
            $options = $config['options'];
            if (!isset($options['path']) || !isset($options['filename'])) {
                throw new LoggerConfigException('Não foi possível recuperar o PATH e o FILENAME');
            }
        } else {
            $options['path']     = Logger::LOG_PATH;
            $options['filename'] = Logger::LOG_FILENAME . Logger::LOG_EXTENSION;
        }
        return $options;
    }
} 