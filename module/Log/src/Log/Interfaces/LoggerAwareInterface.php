<?php
/**
 * Class LogAwareInterface
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Log\Interfaces;

use Log\Service\LoggerService;

interface LoggerAwareInterface {

    /**
     * @param LoggerService $loggerService
     * @return mixed
     */
    public function setLogger(LoggerService $loggerService);

    /**
     * @return LoggerService
     */
    public function getLogger();
} 