<?php
/**
 * Trait LoggerAwareTrait
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Log\Traits;

use Log\Service\LoggerService;

trait LoggerAwareTrait
{
    /**
     * @var LoggerService
     */
    protected $logger;
    /**
     * @param LoggerService $logger
     * @return mixed
     */
    public function setLogger(LoggerService $logger)
    {
        $this->logger = $logger;
        return $this;
    }

    /**
     * @return LoggerService
     */
    public function getLogger()
    {
        return $this->logger;
    }

} 