<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Servidor\Controller\Servidor' => 'Servidor\Controller\ServidorController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'servidor' => array(
                'type'    => 'Literal',
                'options' => array(
                    // Change this to something specific to your module
                    'route'    => '/api/servidor',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'Servidor\Controller',
                        'controller'    => 'Servidor',
                        'action'        => 'gerar-entity',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
    		'ViewJsonStrategy'
    	)
    ),
    'doctrine' => array(
        'eventmanager' => array(
            'orm_default' => array(
                'subscribers' => array(
                    'Gedmo\Timestampable\TimestampableListener',
                    'Gedmo\Sluggable\SluggableListener'
                ),
            ),
        ),
        'driver' => array(
            'Servidor_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/Servidor/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Servidor\Entity' => 'Servidor_driver'
                )
            )
        )
    ),
);
