<?php
namespace Servidor\Entity;

use Doctrine\ORM\Mapping as ORM;
use Servidor\Interfaces\ObjectEntity;

/**
 * Empresa
 *
 * @ORM\Table(name="empresa")
 * @ORM\Entity
 */
class Empresa implements ObjectEntity
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=70, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="slogan", type="string", length=255, nullable=true)
     */
    private $slogan;

    /**
     * @var string
     *
     * @ORM\Column(name="ddd", type="string", length=2, nullable=true, options={"fixed"=true})
     */
    private $ddd;

    /**
     * @var string
     *
     * @ORM\Column(name="telefone", type="string", length=25, nullable=true)
     */
    private $telefone;

    /**
     * @var string
     * @ORM\Column(name="cep", type="string", length=9, nullable=false)
     */
    private $cep;

    /**
     * @var string
     *
     * @ORM\Column(name="logradouro", type="string", length=200, nullable=false)
     */
    private $logradouro;

    /**
     * @var string
     *
     * @ORM\Column(name="bairro", type="string", length=60, nullable=false)
     */
    private $bairro;

    /**
     * @var string
     *
     * @ORM\Column(name="cidade", type="string", length=60, nullable=false)
     */
    private $cidade;

    /**
     * @var string
     *
     * @ORM\Column(name="uf", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private $uf;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=6, nullable=false)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=70, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="analytics", type="string", length=20, nullable=true)
     */
    private $analytics;

    /**
     * @var string
     *
     * @ORM\Column(name="quem_somos", type="text", nullable=true)
     */
    private $quemSomos;

    /**
     * @var string
     *
     * @ORM\Column(name="mapa_localizacao", type="text", nullable=true)
     */
    private $mapaLocalizacao;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_plugin", type="text", nullable=true)
     */
    private $facebookPlugin;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter", type="string", length=255, nullable=true)
     */
    private $twitter;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter_plugin", type="text", nullable=true)
     */
    private $twitterPlugin;

    /**
     * @var string
     *
     * @ORM\Column(name="youtube", type="string", length=255, nullable=true)
     */
    private $youtube;

    /**
     * @var string
     *
     * @ORM\Column(name="googlemais", type="string", length=255, nullable=true)
     */
    private $googleMais;

    /**
     * @var string
     *
     * @ORM\Column(name="texto_contato", type="text", nullable=true)
     */
    private $textoContato;

    /**
     * @var string
     *
     * @ORM\Column(name="texto_rodape", type="text", nullable=true)
     */
    private $textoRodape;

    /**
     * @param string $analytics
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setAnalytics($analytics)
    {
        $this->analytics = $analytics;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getAnalytics()
    {
        return $this->analytics;
    }

    /**
     * @param string $cep
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param string $bairro
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * @param string $cidade
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * @param string $logradouro
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setLogradouro($logradouro)
    {
        $this->logradouro = $logradouro;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getLogradouro()
    {
        return $this->logradouro;
    }

    /**
     * @param string $uf
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setUf($uf)
    {
        $this->uf = $uf;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * @param string $ddd
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setDdd($ddd)
    {
        $this->ddd = $ddd;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getDdd()
    {
        return $this->ddd;
    }

    /**
     * @param string $email
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $facebook
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @param string $facebookPlugin
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setFacebookPlugin($facebookPlugin)
    {
        $this->facebookPlugin = $facebookPlugin;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getFacebookPlugin()
    {
        return $this->facebookPlugin;
    }

    /**
     * @param string $googleMais
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setGoogleMais($googleMais)
    {
        $this->googleMais = $googleMais;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getGoogleMais()
    {
        return $this->googleMais;
    }

    /**
     * @return int
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $mapaLocalizacao
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setMapaLocalizacao($mapaLocalizacao)
    {
        $this->mapaLocalizacao = $mapaLocalizacao;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getMapaLocalizacao()
    {
        return $this->mapaLocalizacao;
    }

    /**
     * @param string $nome
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $numero
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param string $quemSomos
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setQuemSomos($quemSomos)
    {
        $this->quemSomos = $quemSomos;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getQuemSomos()
    {
        return $this->quemSomos;
    }

    /**
     * @param string $slogan
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getSlogan()
    {
        return $this->slogan;
    }

    /**
     * @param string $telefone
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @param string $twitter
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * @param string $twitterPlugin
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setTwitterPlugin($twitterPlugin)
    {
        $this->twitterPlugin = $twitterPlugin;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getTwitterPlugin()
    {
        return $this->twitterPlugin;
    }

    /**
     * @param string $youtube
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * @param string $textoContato
     */
    public function setTextoContato($textoContato)
    {
        $this->textoContato = $textoContato;
        return $this;
    }

    /**
     * @return string
     */
    public function getTextoContato()
    {
        return $this->textoContato;
    }

    /**
     * @param string $textoRodape
     */
    public function setTextoRodape($textoRodape)
    {
        $this->textoRodape = $textoRodape;
        return $this;
    }

    /**
     * @return string
     */
    public function getTextoRodape()
    {
        return $this->textoRodape;
    }

    /**
     * Get getImageFolders
     * Retorna um array com o caminho das pastas de imagens desta entidade,
     * este metodo é utilizado no servico para renomear as pastas quando é
     * criado um novo registro. (/new_x -> /x).
     *
     * @return Array
     */
    public function getImageFolders()
    {
        $folders = Array();
        $folders[] = 'public_html/uploads/files/quemsomos/';
        return $folders;
    }

}
