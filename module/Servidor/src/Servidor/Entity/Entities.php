<?php
namespace Servidor\Entity;

final class Entities {

    const ENTITY_EMPRESA = 'Servidor\Entity\Empresa';
    
    const ENTITY_SERVICO = 'Servidor\Entity\Servico';

    const ENTITY_PARCEIRO = 'Servidor\Entity\Parceiro';

    const ENTITY_PORTFOLIO = 'Servidor\Entity\Portfolio';

    const ENTITY_PREVILEGIO = 'Servidor\Entity\Previlegio';

    const ENTITY_RECURSO = 'Servidor\Entity\Recurso';
    
    const ENTITY_REGRA = 'Servidor\Entity\Regra';
    
    const ENTITY_USUARIO = 'Servidor\Entity\Usuario';

    
}