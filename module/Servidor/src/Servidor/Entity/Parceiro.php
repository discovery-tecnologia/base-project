<?php
namespace Servidor\Entity;

use Doctrine\ORM\Mapping as ORM;
use Servidor\Interfaces\ObjectEntity;

/**
 * Parceiro
 *
 * @ORM\Table(name="parceiro")
 * @ORM\Entity(repositoryClass="Servidor\Repository\ParceiroRepository")
 */
class Parceiro implements ObjectEntity
{

    const CLIENTE = 'c';
    const PARCEIRO = 'p';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=70, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=200, nullable=false)
     */
    private $link;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_cliente", type="smallint", nullable=false, options={"default"=0})
     */
    private $cliente = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_parceiro", type="smallint", nullable=false, options={"default"=0})
     */
    private $parceiro = '0';

    /**
     * @param boolean $cliente
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setCliente($cliente)
    {
        $this->cliente = $cliente;
        return $this;
    }

    /**
     * @return boolean
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * @param string $link
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @return int
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $nome
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param boolean $parceiro
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setParceiro($parceiro)
    {
        $this->parceiro = $parceiro;
        return $this;
    }

    /**
     * @return boolean
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getParceiro()
    {
        return $this->parceiro;
    }

    /**
     * Get getImageFolders
     * Retorna um array com o caminho das pastas de imagens desta entidade,
     * este metodo é utilizado no servico para renomear as pastas quando é
     * criado um novo registro. (/new_x -> /x).
     *
     * @return Array
     */
    public function getImageFolders()
    {
        $folders = Array();
        $folders[] = 'public_html/uploads/files/parceiro/';
        return $folders;
    }
}
