<?php
/**
 * Class Portfolio
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Servidor\Entity;

use Doctrine\ORM\Mapping as ORM;
use Servidor\Interfaces\ObjectEntity;

/**
 * Class Portfolio
 * @package Servidor\Entity
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 *
 * @ORM\Table(name="portfolio")
 * @ORM\Entity(repositoryClass="Servidor\Repository\PortfolioRepository")
 */
class Portfolio implements ObjectEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=100, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="text", nullable=false)
     */
    private $descricao;

    /**
     * @param string $descricao
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @return int
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $titulo
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Get getImageFolders
     * Retorna um array com o caminho das pastas de imagens desta entidade,
     * este metodo é utilizado no servico para renomear as pastas quando é
     * criado um novo registro. (/new_x -> /x).
     *
     * @return Array
     */
    public function getImageFolders()
    {
        $folders = Array();
        $folders[] = 'public_html/uploads/files/portfolio_capa/';
        $folders[] = 'public_html/uploads/files/portfolio/';
        return $folders;
    }
} 