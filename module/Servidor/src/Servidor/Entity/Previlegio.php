<?php
namespace Servidor\Entity;

use Doctrine\ORM\Mapping as ORM;
use Servidor\Interfaces\ObjectEntity;

/**
 * Previlegio
 *
 * @ORM\Table(name="previlegio",
 *      indexes={
 *          @ORM\Index(name="fk_previlegio_regra", columns={"regra_id"}),
 *          @ORM\Index(name="fk_previlegio_recurso", columns={"recurso_id"})
 *      })
 * @ORM\Entity(repositoryClass="Servidor\Repository\PrevilegioRepository")
 */
class Previlegio implements ObjectEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=45, nullable=false)
     */
    private $nome;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="criado_em", type="datetime", nullable=false)
     */
    private $criadoEm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="atualizado_em", type="datetime", nullable=false)
     */
    private $atualizadoEm;

    /**
     * @var \Regra
     *
     * @ORM\ManyToOne(targetEntity="Regra")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="regra_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $regra;

    /**
     * @var \Recurso
     *
     * @ORM\ManyToOne(targetEntity="Recurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="recurso_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $recurso;

    
    public function __construct( $options = array() ) {
    	 
    	$this->criadoEm = new \DateTime("now");
    	$this->atualizadoEm = new \DateTime("now");
    }
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return Previlegio
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string 
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set criadoEm
     *
     * @param \DateTime $criadoEm
     * @return Previlegio
     */
    public function setCriadoEm($criadoEm)
    {
        $this->criadoEm = $criadoEm;

        return $this;
    }

    /**
     * Get criadoEm
     *
     * @return \DateTime 
     */
    public function getCriadoEm()
    {
        return $this->criadoEm;
    }

    /**
     * Set atualizadoEm
     * @ORM\PrePersist
     * @return Previlegio
     */
    public function setAtualizadoEm($atualizadoEm)
    {
        $this->atualizadoEm = $atualizadoEm;

        return $this;
    }

    /**
     * Get atualizadoEm
     *
     * @return \DateTime 
     */
    public function getAtualizadoEm()
    {
        return $this->atualizadoEm;
    }

    /**
     * Set regra
     *
     * @param \Regra $regra
     * @return Previlegio
     */
    public function setRegra( Regra $regra = null)
    {
        $this->regra = $regra;

        return $this;
    }

    /**
     * Get regra
     *
     * @return \Regra 
     */
    public function getRegra()
    {
        return $this->regra;
    }

    /**
     * Set recurso
     *
     * @param \Recurso $recurso
     * @return Previlegio
     */
    public function setRecurso( Recurso $recurso = null)
    {
        $this->recurso = $recurso;

        return $this;
    }

    /**
     * Get recurso
     *
     * @return \Recurso 
     */
    public function getRecurso()
    {
        return $this->recurso;
    }
}
