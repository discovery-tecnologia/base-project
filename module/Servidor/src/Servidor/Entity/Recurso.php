<?php
namespace Servidor\Entity;

use Doctrine\ORM\Mapping as ORM;
use Servidor\Interfaces\ObjectEntity;

/**
 * Recurso
 *
 * @ORM\Table(name="recurso")
 * @ORM\Entity(repositoryClass="Servidor\Repository\RecursoRepository")
 */
class Recurso implements ObjectEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=45, nullable=false, unique=true)
     */
    private $nome;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="criado_em", type="datetime", nullable=false)
     */
    private $criadoEm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="atualizado_em", type="datetime", nullable=false)
     */
    private $atualizadoEm;


    
    public function __construct( $options = array() ) {
    	 
    	$this->criadoEm = new \DateTime("now");
    	$this->atualizadoEm = new \DateTime("now");
    }
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return Recurso
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string 
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set criadoEm
     *
     * @param \DateTime $criadoEm
     * @return Recurso
     */
    public function setCriadoEm($criadoEm)
    {
        $this->criadoEm = $criadoEm;

        return $this;
    }

    /**
     * Get criadoEm
     *
     * @return \DateTime 
     */
    public function getCriadoEm()
    {
        return $this->criadoEm;
    }

    /**
     * Set atualizadoEm
     * @ORM\PrePersist
     * @return Recurso
     */
    public function setAtualizadoEm($atualizadoEm)
    {
        $this->atualizadoEm = $atualizadoEm;

        return $this;
    }

    /**
     * Get atualizadoEm
     *
     * @return \DateTime 
     */
    public function getAtualizadoEm()
    {
        return $this->atualizadoEm;
    }
}
