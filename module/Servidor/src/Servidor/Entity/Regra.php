<?php
namespace Servidor\Entity;

use Doctrine\ORM\Mapping as ORM;
use Servidor\Interfaces\ObjectEntity;

/**
 * Regra
 *
 * @ORM\Table(name="regra")
 * @ORM\Entity(repositoryClass="Servidor\Repository\RegraRepository")
 */
class Regra implements ObjectEntity
{

    const VISITANTE     = 2;
    const REGISTRADO    = 3;
    const ADMINISTRADOR = 4;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=45, nullable=false, unique=true)
     */
    protected $nome;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_master", type="smallint", nullable=true, options={"default"=0})
     */
    protected $master;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="criado_em", type="datetime", nullable=false)
     */
    protected $criadoEm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="atualizado_em", type="datetime", nullable=false)
     */
    protected $atualizadoEm;

    /**
     * @var \Regra
     *
     * @ORM\OneToOne(targetEntity="Regra")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    protected $parent;
    
    
    public function __construct( $options = array() ) {
    	
    	$this->criadoEm = new \DateTime("now");
    	$this->atualizadoEm = new \DateTime("now");
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return Regra
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string 
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set setMaster
     *
     * @param boolean $isMaster
     * @return Regra
     */
    public function setMaster($isMaster)
    {
        $this->master = $isMaster;

        return $this;
    }

    /**
     * Get isMaster
     *
     * @return boolean 
     */
    public function isMaster()
    {
        return $this->master;
    }

    /**
     * Set criadoEm
     *
     * @return Regra
     */
    public function setCriadoEm()
    {
        $this->criadoEm = new \DateTime("now");

        return $this;
    }

    /**
     * Get criadoEm
     *
     * @return \DateTime 
     */
    public function getCriadoEm()
    {
        return $this->criadoEm;
    }

    /**
     * Set atualizadoEm
     * @ORM\PrePersist
     * @return Regra
     */
    public function setAtualizadoEm()
    {
        $this->atualizadoEm =  new \DateTime("now");

        return $this;
    }

    /**
     * Get atualizadoEm
     *
     * @return \DateTime 
     */
    public function getAtualizadoEm()
    {
        return $this->atualizadoEm;
    }

    /**
     * Set parent
     * @param \Regra $parent
     * @return Regra
     */
    public function setParent( $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Regra 
     */
    public function getParent()
    {
        return $this->parent;
    }
}
