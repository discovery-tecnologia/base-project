<?php
namespace Servidor\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Servidor\Interfaces\ObjectEntity;

/**
 * Produto
 *
 * @ORM\Table(name="servico")
 * @ORM\Entity(repositoryClass="Servidor\Repository\ServicoRepository")
 */
class Servico implements ObjectEntity
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="text", nullable=false)
     */
    private $descricao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ativo", type="smallint", nullable=false, options={"default"=1})
     */
    private $ativo = '1';

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="criado_em", type="datetime", nullable=false)
     */
    private $criadoEm;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="atualizado_em", type="datetime", nullable=false)
     */
    private $atualizadoEm;

    /**
     * @param boolean $ativo
     * @return Servico
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
        return $this;
    }

    /**
     * @return boolean
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * @param string $descricao
     * @return Servico
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @return int
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $nome
     * @return Servico
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return string
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @return \DateTime
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getAtualizadoEm()
    {
        return $this->atualizadoEm;
    }

    /**
     * @return \DateTime
     * @author DiegoWagner <diegowagner4@gmail.com>
     */
    public function getCriadoEm()
    {
        return $this->criadoEm;
    }

    /**
     * Get getImageFolders
     * Retorna um array com o caminho das pastas de imagens desta entidade,
     * este metodo é utilizado no servico para renomear as pastas quando é
     * criado um novo registro. (/new_x -> /x).
     *
     * @return Array
     */
    public function getImageFolders()
    {
        $folders = Array();
        $folders[] = 'public_html/uploads/files/servico_capa/';
        $folders[] = 'public_html/uploads/files/servico/';
        return $folders;
    }

}
