<?php
namespace Servidor\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

use Servidor\Interfaces\ObjectEntity;
use Servidor\Interfaces\LoggedAwareInterface;
use Zend\Math\Rand,
    Zend\Crypt\Key\Derivation\Pbkdf2;


/**
 * Usuario
 *
 * @ORM\Table(name="usuario", indexes={@ORM\Index(name="fk_usuario_regra", columns={"regra_id"})})
 * @ORM\Entity(repositoryClass="Servidor\Repository\UsuarioRepository")
 */
class Usuario implements ObjectEntity, LoggedAwareInterface
{
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=45, nullable=false)
     */
    protected $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="senha", type="string", length=50, nullable=false)
     */
    protected $senha;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=false)
     */
    protected $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=70, nullable=false)
     */
    protected $email;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ativo", type="smallint", nullable=true, options={"default"=1})
     */
    protected $ativo = 1;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="criado_em", type="datetime", nullable=false)
     */
    private $criadoEm;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="atualizado_em", type="datetime", nullable=false)
     */
    private $atualizadoEm;
    
    /**
     * @var \Regra
     *
     * @ORM\ManyToOne(targetEntity="Servidor\Entity\Regra", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="regra_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $regra;

	public function __construct(array $options = array()) {
    
    	$this->criadoEm = new \DateTime("now");
    	$this->atualizadoEm = new \DateTime("now");
    
    	$this->salt = base64_encode(Rand::getBytes(8, true));
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return Usuario
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    
        return $this;
    }

    /**
     * Get nome
     *
     * @return string 
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set senha
     *
     * @param string $senha
     * @return Usuario
     */
    public function setSenha($senha)
    {
        $this->senha = $this->encryptSenha($senha);
    	return $this;
    }

    /**
     * Get senha
     *
     * @return string 
     */
    public function getSenha()
    {
        return $this->senha;
    }
    
    /**
     * @method encryptPassword()
     * @param  string $password
     * @return string
     */
    public function encryptSenha($password)
    {
    	return base64_encode(Pbkdf2::calc('sha256', $password, $this->salt, 10000, strlen($password*2)));
    }
    

    /**
     * Set salt
     *
     * @param string $salt
     * @return Usuario
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Usuario
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set ativo
     *
     * @param boolean $ativo
     * @return Usuario
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    
        return $this;
    }

    /**
     * Get ativo
     *
     * @return boolean 
     */
    public function isAtivo()
    {
        return $this->ativo;
    }

    /**
     * Get criadoEm
     *
     * @return \DateTime 
     */
    public function getCriadoEm()
    {
        return $this->criadoEm;
    }

    /**
     * Get atualizadoEm
     *
     * @return \DateTime 
     */
    public function getAtualizadoEm()
    {
        return $this->atualizadoEm;
    }
    
    
    /**
     * Get regra
     *
     * @return \Regra 
     */
    public function getRegra() {
    	
    	return $this->regra;
    }
    
    
    /**
     * Set regra
     *
     * @param \Regra $regra
     * @return Usuario
     */
    public function setRegra( Regra $regra) {
    	
    	$this->regra = $regra;
    	
    	return $this;
    }

    /**
     * Get getImageFolders
     * Retorna um array com o caminho das pastas de imagens desta entidade,
     * este metodo é utilizado no servico para renomear as pastas quando é
     * criado um novo registro. (/new_x -> /x).
     *
     * @return Array
     */
    public function getImageFolders()
    {
        $folders = Array();
        $folders[] = 'public_html/uploads/files/usuario/';
        return $folders;
    }

}
