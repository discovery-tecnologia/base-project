<?php
/**
 * Class LoggedAwareInterface
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Servidor\Interfaces;


interface LoggedAwareInterface {

    /**
     * @return String
     */
    public function getEmail();

    /**
     * @param $email
     * @return String
     */
    public function setEmail($email);

    /**
     * @return String
     */
    public function getSenha();

    /**
     * @param $senha
     * @return String
     */
    public function setSenha($senha);

    /**
     * @return String
     */
    public function getSalt();

    /**
     * @param $salt
     * @return String
     */
    public function setSalt($salt);
} 