<?php
namespace Servidor\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Servidor\Entity\Empresa;
use Servidor\Entity\Entities;

class LojaRepository extends EntityRepository {
	
    
    /**
     * Retorna todas as lojas em forma de array
     * @return multitype:NULL
     */
    public function fetchAll() {
    
    	$entities = $this->findAll();
    	$array = array();
    
    	foreach ( $entities as $entity ) {
    		$array[$entity->getId()] = $entity->getNome();
    	}
    
    	return $array;
    }

}