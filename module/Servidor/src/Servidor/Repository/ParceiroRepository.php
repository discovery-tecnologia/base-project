<?php
namespace Servidor\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class ParceiroRepository extends EntityRepository {
	
    
    /**
     * @method search()
     * Retorna uma lista baseada na pesquisa de uma string.
     * @return Ambigous <multitype:, \Doctrine\ORM\mixed, \Doctrine\DBAL\Driver\Statement, \Doctrine\Common\Cache\mixed>
     */
    public function search($string, $tipo = null) {

    	$query = $this->createQueryBuilder('n');
    	$query->select(array('n'))
    	->where('n.nome like :string');

        if( $tipo == 'p' ) {
            $query->andWhere('n.parceiro = true');
        }

        if ( $tipo == 'c' ){
            $query->andWhere('n.cliente = true');
        }

    	$query->orderBy('n.id', 'desc');
    
    	$query->setParameter('string', '%'.$string.'%');
    
    	$data = $query->getQuery()->getResult();

        //var_dump($query->getQuery()->getSQL());die;
    
    	return $data;
    }


}