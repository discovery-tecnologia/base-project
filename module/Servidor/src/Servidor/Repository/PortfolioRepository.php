<?php
namespace Servidor\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class PortfolioRepository extends EntityRepository {
	
    
    /**
     * @method search()
     * Retorna uma lista baseada na pesquisa de uma string.
     * @return Ambigous <multitype:, \Doctrine\ORM\mixed, \Doctrine\DBAL\Driver\Statement, \Doctrine\Common\Cache\mixed>
     */
    public function search($string) {
    
    	$query = $this->createQueryBuilder('n');
    	$query->select(array('n'))
    	->from($this->getEntityName(), 'notice')
    	->where('n.titulo like ?1 OR n.descricao like ?2')
    	->orderBy('n.id', 'desc');
    
    	$query->setParameter(1, '%' . $string . '%')
    	->setParameter(2, '%' . $string . '%');
    
    	$data = $query->getQuery()->getResult();
    
    	return $data;
    }
}