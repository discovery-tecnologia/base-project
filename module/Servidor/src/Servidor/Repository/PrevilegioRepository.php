<?php

namespace Servidor\Repository;

use Doctrine\ORM\EntityRepository;

class PrevilegioRepository extends EntityRepository {
	
	
	
	/**
	 * @method getMaxId()
	 * Retorna o último id + 1
	 * Este método é usado principalmente no Service Manager para inserir um novo ID
	 * para inserção de imagens dos cases ( Plugin File Upload )
	 * @return int $id;
	 */
	public function getLastId() {
	
		$query = $this->createQueryBuilder('n')
					  ->select('MAX(n.id) + 1 as id')
					  ->from($this->getEntityName(),'notice');
			
		$id = $query->getQuery()->getSingleScalarResult();
			
		return $id;
	}
	
	
	
	/**
	 * @method findOneRandom()
	 * Retorna um registro randômico.
	 * @return Ambigous <multitype:, \Doctrine\ORM\mixed, \Doctrine\DBAL\Driver\Statement, \Doctrine\Common\Cache\mixed>
	 */
	public function findOneRandom() {

		$sql = 'SELECT * FROM publicidade ORDER BY RAND() LIMIT 2';
		$stmt = $this->getEntityManager()->getConnection()->prepare($sql);
		$stmt->execute();
		$publicidades = $stmt->fetchAll();
		
		return $publicidades;
	}
}