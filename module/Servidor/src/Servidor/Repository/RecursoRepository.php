<?php

namespace Servidor\Repository;

use Doctrine\ORM\EntityRepository;

class RecursoRepository extends EntityRepository {
	
	
	/**
	 * Retorna todas as regras em forma de array
	 * @return multitype:NULL
	 */
	public function fetchAllResources() {
	
		$entities = $this->findAll();
		$array = array();
	
		foreach ( $entities as $entity ) {
			$array[$entity->getId()] = $entity->getNome();
		}
	
		return $array;
	}
	
}