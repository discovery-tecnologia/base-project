<?php
namespace Servidor\Repository;

use Acl\Enum\AuthResult;
use Doctrine\ORM\EntityRepository;
use Servidor\Entity\Loja;


class UsuarioRepository extends EntityRepository {
	

    /**
     * Efetua o login do usuário
     *
     * @method findByEmailAndSenha ($email, $senha)
     * @param $email
     * @param $senha
     * @return AuthResult
     * @author Diego Wagner <diegowagner4@gmail.com>
     * @access public
     * @throws \Exception
     */
    public function findByEmailAndSenha( $email, $senha )
    {
        try {

            $user = $this->findOneByEmail( $email );
            if (! is_null($user)) {

                $hashSenha = $user->encryptSenha( $senha );

                if ($user->getSenha() == $hashSenha) {
                    if ($user->isAtivo()) {
                        return new AuthResult(AuthResult::SUCCESS, $user, array('Autenticado com sucesso'));
                    } else {
                        return new AuthResult(AuthResult::FAILURE_IDENTITY_STATUS, null, array('Este cadastro está inativo'));
                    }
                } else {
                    return new AuthResult(AuthResult::FAILURE_CREDENTIAL_INVALID, null, array('Senha inválida.'));
                }

            } else {
                return new AuthResult(AuthResult::FAILURE_IDENTITY_NOT_FOUND, null, array('Usuário inválido.'));
            }

        } catch(\Exception $e) {
            throw $e;
        }
    }
	
	/**
	 * Retorna todos os usuários ativos diferentes de desenvolvedor
	 * @return \Servidor\Entity\Usuario
	 */
	public function findAllUsers() {

		$query = $this->createQueryBuilder('u')
					  ->select(array('u'))
					  ->andwhere("u.nome <> ?1");
	
		$query->setParameter( 1, "Desenvolvedor" ); // sempre exclui o usuário desenvolvedor
		//var_dump($query->getDQL());die;
		$result = $query->getQuery()->getResult();
		return $result;
	}
	
	
	
	/**
	 * @method getMaxId()
	 * Retorna o último id + 1
	 * Este método é usado principalmente no Service Manager para inserir um novo ID 
	 * para inserção de imagens do usuário ( Plugin File Upload )
	 * @return int $id;
	 */
	public function getLastId () {
		
		$query = $this->createQueryBuilder('u')
		              ->select('MAX(u.id) + 1 as id')
		              ->from($this->getEntityName(),'user');
		 
		$id = $query->getQuery()->getSingleScalarResult();
		 
		return $id;
	}
}