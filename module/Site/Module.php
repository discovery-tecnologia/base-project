<?php
namespace Site;

use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\ModuleManager;
use Site\EventFactory\SiteEvent;
use Site\EventFactory\BuscaEventFactory;

class Module implements AutoloaderProviderInterface {

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
		            // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }



    public function getConfig() {

        return include __DIR__ . '/config/module.config.php';

    }


    
    /**
     * método init do módulo (inicialização do módulo)
     * @param ModuleManager $moduleManager
     */
    public function init( ModuleManager $moduleManager ) {

     	$sharedEvents = $moduleManager->getEventManager()->getSharedManager();

    	// responsável por injetar no layout o formulário de pesquisa
    	$sharedEvents->attach(__NAMESPACE__,
                			array( MvcEvent::EVENT_DISPATCH ),
                			function(MvcEvent $e){

			$eventFactory = new BuscaEventFactory();
			$eventFactory->createForm($e);

        }, 2);
    }



    /**
     * @param $e
     */
    public function onBootstrap($e) {

        $application  = $e->getApplication();
        //Tradutor de mensagens de erro do form
        $translator = $application->getServiceManager()->get('MvcTranslator');
        $translator->addTranslationFile(
            'phpArray', 'vendor/zendframework/zendframework/resources/languages/pt_BR/Zend_Validate.php', 'default'
        );
        $translator->addTranslationFile(
            'phpArray', 'vendor/zendframework/zendframework/resources/languages/pt_BR/Zend_Captcha.php', 'default'
        );

        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
    }
    
}