<?php
namespace Site;

return array(
    'router' => array(
        'routes' => array(
            #index ############################################
            'site-index' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Site\Controller\Index',
                        'action'     => 'index'
                    )
                )
            ),
            #quem somos #######################################
            'site-quem-somos' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/quem-somos',
                    'defaults' => array(
                        'controller' => 'Site\Controller\QuemSomos',
                        'action'     => 'index'
                    )
                )
            ),
            #portfolio #######################################
            'site-portfolio' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/portfolio',
                    'defaults' => array(
                        'controller' => 'Site\Controller\Portfolio',
                        'action'     => 'index'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(

                    'view' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '[/:slug]',
                            'defaults' => array(
                                'controller' => 'Site\Controller\Portfolio',
                                'action'     => 'view'
                            )
                        )
                    ),
                    'filter' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route'    => '[/busca/:busca]',
                            'defaults' => array(
                                'controller'    => 'Site\Controller\Portfolio',
                                'action'        => 'index',
                                'busca'         => null
                            ),
                            'constraints' => array(
                                'controller'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'        => '[a-zA-Z][a-zA-Z0-9_-]*'
                            )
                        ),
                    ),
                )
            ),
            #cliente #######################################
            'site-cliente' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/cliente',
                    'defaults' => array(
                        'controller' => 'Site\Controller\Cliente',
                        'action'     => 'index'
                    )
                )
            ),
            #parceiro #######################################
            'site-parceiro' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/parceiro',
                    'defaults' => array(
                        'controller' => 'Site\Controller\Parceiro',
                        'action'     => 'index'
                    )
                )
            ),
            #servico #######################################
            'site-servico' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/servico',
                    'defaults' => array(
                        'controller' => 'Site\Controller\Servico',
                        'action'     => 'index'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(

                    'view' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '[/:slug]',
                            'defaults' => array(
                                'controller' => 'Site\Controller\Servico',
                                'action'     => 'view'
                            )
                        )
                    ),
                    'filter' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route'    => '[/busca/:busca]',
                            'defaults' => array(
                                'controller'    => 'Site\Controller\Servico',
                                'action'        => 'index',
                                'busca'         => null
                            ),
                            'constraints' => array(
                                'controller'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'        => '[a-zA-Z][a-zA-Z0-9_-]*'
                            )
                        )
                    )
                )
            ),
            # contato #####################################
            'site-contato' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/contato',
                    'defaults' => array(
                        'controller' => 'Site\Controller\Contato',
                        'action'     => 'index',
                        'css-class' => 'contato'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(

                    'mail' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/mail',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'Site\Controller\Contato',
                                'action'     => 'mail',
                            )
                        )
                    )
                )
            )
        )
    ),
    'controllers' => array(
        'factories' => array(
            'Site\Controller\Index' 	    => 'Site\Controller\Factory\IndexControllerFactory',
            'Site\Controller\QuemSomos' 	=> 'Site\Controller\Factory\QuemSomosControllerFactory',
            'Site\Controller\Portfolio' 	=> 'Site\Controller\Factory\PortfolioControllerFactory',
            'Site\Controller\Cliente' 	    => 'Site\Controller\Factory\ClienteControllerFactory',
            'Site\Controller\Parceiro' 	    => 'Site\Controller\Factory\ParceiroControllerFactory',
            'Site\Controller\Servico' 	    => 'Site\Controller\Factory\ServicoControllerFactory',
            'Site\Controller\Contato' 	    => 'Site\Controller\Factory\ContatoControllerFactory'
        )
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'site/error/404',
        #'exception_template'       => 'site/error/index',
        'template_map' => array(
            'layout/site'                       => __DIR__ . '/../view/layout/layout.phtml',
            'site/error/404'                    => __DIR__ . '/../view/error/404-site.phtml',
            'site/error/index'                  => __DIR__ . '/../view/error/index.phtml',
            'site/paginator-incremental-top'    => __DIR__ . '/../view/partial/paginator-incremental-top.phtml',
            'site/paginator-incremental-bottom' => __DIR__ . '/../view/partial/paginator-incremental-bottom.phtml',
        ),
        'template_path_stack' => array(
            'site' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy'
        ),
        'translator' => array (
            'locale' => 'pt_BR'
        ),
    ),
    'service_manager' => array (
        'factories' => array(
            'Site\Service\Mail'      => 'Site\Service\Factory\MailServiceFactory',
            'Site\Service\Empresa'   => 'Site\Service\Factory\EmpresaServiceFactory',
            'Site\Service\Portfolio' => 'Site\Service\Factory\PortfolioServiceFactory',
            'Site\Service\Parceiro'  => 'Site\Service\Factory\ParceiroServiceFactory',
            'Site\Service\Servico'   => 'Site\Service\Factory\ServicoServiceFactory',
            'Site\Service\Categoria' => 'Site\Service\Factory\CategoriaServiceFactory'
        ),
        'invokables' => array(
            'Zend\Authentication\AuthenticationService' => 'Zend\Authentication\AuthenticationService'
        ),
    ),
    'view_helpers' => array(
        'factories' => array(
            'HEmpresa'          => 'Site\View\Helper\Factory\HEmpresaFactory',
            'HPortfolio'        => 'Site\View\Helper\Factory\HPortfolioFactory',
            'HServico'          => 'Site\View\Helper\Factory\HServicoFactory'
        ),
        'invokables' => array(
            'HHtmlAbreviate'  => new View\Helper\HHtmlAbreviate()
        )
    )
);