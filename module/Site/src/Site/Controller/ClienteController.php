<?php
namespace Site\Controller;


use Servidor\Entity\Parceiro;
use Site\Service\ParceiroService;
use Zend\View\Model\ViewModel;
use Base\Controller\AbstractSiteController;


class ClienteController extends AbstractSiteController {


    /**
     * @construct
     */
    public function __construct( ParceiroService $service )
    {
        $this->service = $service;
    }



    /**
     * @method indexAction()
     * Responsável por fazer listagem de dados.
     * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
     */
    public function indexAction() {

        try {

            $request = $this->getRequest();

            // Se for uma chamada normal ****************************************
            if($request->isGet()) {

                $busca = $this->params()->fromRoute('busca', '');

                // clientes
                $clientes = $this->service->search($busca, Parceiro::CLIENTE);

                $messages = $this->flashMessenger()
                    ->setNamespace($this->namespace)
                    ->getMessages();

                return new ViewModel( array(
                        'clientes' => $clientes
                    )
                );

            }


        } catch(\Exception $e) {

            $this->flashMessenger()
                ->setNamespace($this->namespace)
                ->addMessage(array(
                    'error' => $e->getMessage(),
                    'info' => $e->getTraceAsString()
                ));
        }

    }



    /**
     * @method viewAction()
     * Visualiza um registro
     * @return \Zend\View\Model\ViewModel
     */
    public function viewAction()
    {
        try {
            $slug = $this->params()->fromRoute('slug',0);

            if($slug)
            {
                $arr = explode('-', $slug);
                $id = (int)$arr[0];
            }

            if($id)
            {
                // produto
                $cliente = $this->service->find($id);

                return new ViewModel(array(
                    'cliente'   => $cliente
                ));

            }

        } catch( \Exception $e ) {

            $this->flashMessenger()
                ->setNamespace( $this->namespace )
                ->addMessage( array(
                    'error' => $e->getMessage(),
                    'info' => $e->getTraceAsString()
                ) )->getMessages();

        }
    }

}