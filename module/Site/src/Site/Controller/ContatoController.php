<?php
namespace Site\Controller;

use Base\Controller\AbstractSiteController;
use Site\Service\EmpresaService;
use Site\Service\MailService;
use Zend\Form\FormInterface;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;


class ContatoController extends AbstractSiteController {

    private $serviceMail;


    /**
     * @construct
     */
    public function __construct(
        FormInterface $form,
        EmpresaService $service,
        MailService $mail
    ) {

        $this->form         = $form;
        $this->service      = $service;
        $this->serviceMail  = $mail;
    }

	
	
	/**
	 * @method indexAction()
	 * Criar um novo registro
	 * @return \Zend\View\Model\ViewModel
	 */
	public function indexAction() {

        try {

            // deleta a imagem do captcha
            $this->deleteCaptchaImages();

            $this->flashMessenger()
                ->setNamespace($this->namespace)
                ->getMessages();


            return new ViewModel(array(
                'form' => $this->form,
            ));

        } catch( \Exception $e ) {

            $this->addMessages( array(
                'error' => $e->getMessage(),
                'info' => $e->getTraceAsString()
            ) );

        }
	
	}



    /**
     * @method mailAction
     * @return \Zend\View\Model\JsonModel
     */
    public function mailAction() {

        try {

            $request = $this->getRequest();
            $arrayReturn = array();

            // Se for uma chamada ajax
            if ($request->isXmlHttpRequest()) {


                $this->form->setData($request->getPost());

                if($this->form->isValid()) {

                    $data = $request->getPost()->toArray();

                    $idLoja = $this->form->get('loja')->getValue();

                    $loja = $this->service->find($idLoja);

                    $destinatarios = array();
                    $destinatarios[] = $loja->getEmail();

                    $this->serviceMail->sendMail('contato', $data, $destinatarios, 'Email de contato do site CBI');

                    $arrayReturn['codigo_retorno'] = 1;

                } else {

                    $errors = $this->form->getMessages();
                    foreach($errors as $key=>$row) {

                        if (!empty($row) && $key != 'submit') {
                            foreach($row as $keyer => $rower) {
                                // cria um array com os erros de cada elemento
                                $formMessages[$key][] = $rower;
                            }
                        }
                    }

                    $arrayReturn['form_messages'] = $formMessages;
                    $arrayReturn['codigo_retorno'] = 0;
                }

            }

        } catch (\Exception $e) {

            $arrayReturn['error_messages'] = $e->getMessage();
            $arrayReturn['codigo_retorno'] = -1;

        }

        $jsonModel = new JsonModel($arrayReturn);
        $jsonModel->setTerminal(true);

        return $jsonModel;

    }
	
	
	
	/**
	 * @method deleteCaptchaImages()
	 * Este método limpa a imagem CAPTCHA na pasta
	 * @author Diego Wagner
	 */
	private function deleteCaptchaImages(){
		
		$dir = __DIR__ . '/../../../../../public_html/images/captcha/';
		
		if( is_dir($dir) ) {
			// abre o diretório
			$dh = opendir ( $dir );
			// faz a leitura do mesmo
			while ( $file = readdir ( $dh ) ) {
				if ( $file != "." && $file != ".." ) {
					$fullpath = $dir . "/" . $file;
					// verifica se é arquivo ou diretório
					if ( ! is_dir ( $fullpath ) ) {
						unlink ( $fullpath );
					}
				}
			}
			// fecha o diretório
			closedir ( $dh );
		}
	}
    
    
}