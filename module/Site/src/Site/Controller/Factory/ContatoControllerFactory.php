<?php
namespace Site\Controller\Factory;


use Site\Controller\ContatoController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ContatoControllerFactory implements FactoryInterface {


    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        $serviceLocatorInstance = $serviceLocator->getServiceLocator();

        // form
        $formManager = $serviceLocatorInstance->get('FormElementManager');
        $form = $formManager->get('Site\Form\Contato');

        // service empresa
        $serviceEmpresa = $serviceLocatorInstance->get('Site\Service\Empresa');

        // service Mail
        $serviceMail = $serviceLocatorInstance->get('Site\Service\Mail');


        return new ContatoController( $form, $serviceEmpresa, $serviceMail );
    }
}