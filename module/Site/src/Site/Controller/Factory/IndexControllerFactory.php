<?php
namespace Site\Controller\Factory;


use Site\Controller\IndexController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class IndexControllerFactory implements FactoryInterface {


    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        $serviceLocatorInstance = $serviceLocator->getServiceLocator();
        // service produto
        //$produtoService = $serviceLocatorInstance->get('Site\Service\Produto');
        return new IndexController();
    }
}