<?php
namespace Site\Controller\Factory;

use Site\Controller\ParceiroController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class ParceiroControllerFactory implements FactoryInterface {


    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocatorInstance = $serviceLocator->getServiceLocator();

        // service
        $service = $serviceLocatorInstance->get('Site\Service\Parceiro');

        return new ParceiroController( $service );

    }
}