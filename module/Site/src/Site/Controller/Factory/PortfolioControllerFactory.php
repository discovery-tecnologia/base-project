<?php
namespace Site\Controller\Factory;

use Site\Controller\PortfolioController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class PortfolioControllerFactory implements FactoryInterface {


    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocatorInstance = $serviceLocator->getServiceLocator();

        // service
        $portfolioService = $serviceLocatorInstance->get('Site\Service\Portfolio');

        return new PortfolioController( $portfolioService );

    }
}