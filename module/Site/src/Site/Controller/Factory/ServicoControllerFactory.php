<?php
namespace Site\Controller\Factory;

use Site\Controller\ServicoController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class ServicoControllerFactory implements FactoryInterface {


    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocatorInstance = $serviceLocator->getServiceLocator();

        // service
        $service = $serviceLocatorInstance->get('Site\Service\Servico');

        return new ServicoController( $service );

    }
}