<?php
namespace Site\Controller;

use Base\Controller\AbstractSiteController;
use Base\Enum\FlashMessages;
use Log\Traits\LoggerAwareTrait;
use Site\Service\ProdutoService;
use  Zend\View\Model\ViewModel;


class IndexController extends AbstractSiteController
{

    /**
     * @method indexAction()
     * Responsável por fazer listagem de dados.
     * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
     */
    public function indexAction() {

        try {

            $request = $this->getRequest();
            // Se for uma chamada normal ****************************************
            if($request->isGet()) {

                return new ViewModel(

                );
            }

        } catch(\Exception $e) {
            $this->flashMessenger()->addErrorMessage(FlashMessages::ERRO_INESPERADO);
        }

    }

}