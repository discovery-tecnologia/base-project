<?php
/**
 * Interface ClienteAwareInterface
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Site\Controller\Interfaces;

use Servidor\Entity\Cliente;

interface ClienteAwareInterface
{

    /**
     * @param Cliente $cliente
     * @return mixed
     */
    public function setCliente(Cliente $cliente);

    /**
     * @return Cliente
     */
    public function getCliente();
} 