<?php
/**
 * Interface SessionCookieAwareInterface
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Site\Controller\Interfaces;


interface SessionCookieAwareInterface {

    /**
     * @return array()
     */
    public function getCookie();

    /**
     * @param array $cookieValue
     */
    public function setCookie($cookieValue);
} 