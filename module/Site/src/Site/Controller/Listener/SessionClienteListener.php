<?php
/**
 * Class SessionClienteListener
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Site\Controller\Listener;

use Acl\Auth\AuthAdapter;
use Servidor\Entity\Cliente;
use Site\Service\ClienteService;
use Zend\Authentication\AuthenticationService;
use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\MvcEvent;
use Zend\Authentication\Storage\Session as SessionStorage;

class SessionClienteListener extends AbstractListenerAggregate
{

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authenticationService;

    /**
     * @var \Site\Service\ClienteService
     */
    private $clienteService;

    public function __construct(AuthenticationService $authenticationService, ClienteService $clienteService)
    {
        $this->authenticationService = $authenticationService;
        $this->clienteService = $clienteService;
    }

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH, array($this, 'getClientInSession'), 99);
    }

    public function getClientInSession(EventInterface $event)
    {
        $controller = $event->getTarget();
        $sessionStorage = new SessionStorage(AuthAdapter::SITE_CONTAINER);

        $this->authenticationService->setStorage($sessionStorage);
        if (! $this->authenticationService->hasIdentity()) {
            return $controller->redirect()->toRoute('site-login');
        }

        $cliente = $this->clienteService->find($this->authenticationService->getIdentity()->getId());

        if (! ($cliente instanceof Cliente)) {
            return $controller->redirect()->toRoute('site-login');
        }
        $controller->setCliente($cliente);
    }
} 