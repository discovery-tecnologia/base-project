<?php
/**
 * Class SessionCookieListener
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Site\Controller\Listener;


use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;

class SessionCookieListener extends AbstractListenerAggregate
{

    private $container;

    public function __construct()
    {
        $this->container = new Container('Site');
    }

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('cookie.create', array($this, 'setCookieInSession'), 99);
    }


    public function setCookieInSession(EventInterface $e)
    {
        $value = $e->getParam('value');
        $controller = $e->getTarget();

        if ( $this->container->offsetExists($controller::COOKIE_SESSION) ) {
            // recupera o valor do cookie
            $valueCookie = $this->container->offsetGet($controller::COOKIE_SESSION);
            // tranforma em um array.
            $arr = explode(';', $valueCookie);
            // verifica se o valor já contém no array de valores.
            if(!in_array($value, $arr)){
                // máximo de elementos gravados no cookie de itens é 20
                $qtd = sizeof($arr);
                if($qtd >= 20){
                    // elimina o ultimo elemento do array
                    array_pop($arr);
                    $items = '';
                    for ($i = 0;$i < count($arr); $i++){

                        if($i != 18){
                            $items .= $arr[$i].";";
                        }else{
                            $items .= $arr[$i];
                        }
                    }
                    // seta os valores novamente, para que sejam inseridos no Coookie
                    $newValue = $value.";".$items;
                }else{
                    // concatena os valores com o caracter ";".
                    $newValue = $value.";".$valueCookie;
                }
                // seta o valor no cookie novamente.
                $this->container->offsetSet($controller::COOKIE_SESSION, $newValue);
            }

        } else {
            // seta o valor no cookie novamente.
            $this->container->offsetSet($controller::COOKIE_SESSION, $value);
        }
        $this->container->getManager()->rememberMe(2592000); // 1 mês

        $controller->setCookie(explode(';', $this->container->offsetGet($controller::COOKIE_SESSION)));
    }
} 