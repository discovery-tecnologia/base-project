<?php
namespace Site\Controller;


use Site\Service\PortfolioService;
use Zend\View\Model\ViewModel;
use Base\Controller\AbstractSiteController;


class PortfolioController extends AbstractSiteController {


    /**
     * @construct
     */
    public function __construct( PortfolioService $portfolioService )
    {
        $this->service = $portfolioService;

    }



    /**
     * @method indexAction()
     * Responsável por fazer listagem de dados.
     * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
     */
    public function indexAction() {

        try {

            $request = $this->getRequest();

            // Se for uma chamada normal ****************************************
            if($request->isGet()) {

                $busca = $this->params()->fromRoute('busca', '');

                // produtos
                $portfolios = $this->service->search($busca);


                $messages = $this->flashMessenger()
                    ->setNamespace($this->namespace)
                    ->getMessages();

                return new ViewModel( array(
                        'portfolios' => $portfolios
                    )
                );

            }


        } catch(\Exception $e) {

            $this->flashMessenger()
                ->setNamespace($this->namespace)
                ->addMessage(array(
                    'error' => $e->getMessage(),
                    'info' => $e->getTraceAsString()
                ));
        }

    }



    /**
     * @method viewAction()
     * Visualiza um registro
     * @return \Zend\View\Model\ViewModel
     */
    public function viewAction()
    {
        try {
            $slug = $this->params()->fromRoute('slug',0);

            if($slug)
            {
                $arr = explode('-', $slug);
                $id = (int)$arr[0];
            }

            if($id)
            {
                // produto
                $portifolio = $this->service->find($id);

                return new ViewModel(array(
                    'portfolio'   => $portifolio
                ));

            }

        } catch( \Exception $e ) {

            $this->flashMessenger()
                ->setNamespace( $this->namespace )
                ->addMessage( array(
                    'error' => $e->getMessage(),
                    'info' => $e->getTraceAsString()
                ) )->getMessages();

        }
    }

}