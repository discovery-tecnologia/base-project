<?php
namespace Site\Controller;

use Zend\View\Model\ViewModel;
use Base\Controller\AbstractSiteController;


class QuemSomosController extends AbstractSiteController {
	
	/**
	 * (non-PHPdoc)
	 * @see \Base\Controller\AbstractCrudController::indexAction()
	 */
    public function indexAction() {

    	       
        try {
        
        	$this->flashMessenger()
	        	 ->setNamespace($this->namespace)
	        	 ->getMessages();
        
        	 return new ViewModel();

        } catch( \Exception $e ) {

            $this->flashMessenger()
                ->setNamespace( $this->namespace )
                ->addMessage( array(
                    'error' => $e->getMessage(),
                    'info' => $e->getTraceAsString()
                ) );

        }

    }
    
}