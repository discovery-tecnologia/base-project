<?php 

namespace Site\EventFactory;

use Zend\Mvc\MvcEvent;

use Site\Form\Busca as BuscaForm;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;

class BuscaEventFactory implements EventManagerAwareInterface {
	
    /**
     * @var EventManager
     */
	protected $events;
	

	/**
	 * @method setEventManager()
	 * seta o gerenciador de eventos
	 * (non-PHPdoc)
	 * @see \Zend\EventManager\EventManagerAwareInterface::setEventManager()
	 */
    public function setEventManager( EventManagerInterface $events ) {
    	
    	$events->setIdentifiers( array(
    		__CLASS__,
    		get_called_class()
    	) );
    	
        $this->events = $events;
        return $this;
    }

    
    /**
     * @method getEventManager()
     * Retorna a instância do gerenciador de eventos
     * (non-PHPdoc)
     * @see \Zend\EventManager\EventsCapableInterface::getEventManager()
     */
    public function getEventManager() {
    	
        if (null === $this->events) {
            $this->setEventManager(new EventManager());
        }
        return $this->events;
    }
    
    
    
    /**
     * @method createForm()
     * Retorna a instância do formulário de NewsLetter
     * @param MvcEvent $e
     */
    public function createForm( MvcEvent $e ) {

        $request = $e->getRequest();
        $data    = $e->getRequest()->getPost()->toArray();

        // obtem o nome inicial da rota
        $router = $e->getTarget()->getServiceLocator()->get('Router');
        $routeMatch = $router->match($request);
        $routeName = $routeMatch->getMatchedRouteName();
        $baseRouteName = strtok($routeName, '/');

        // instancia o form
        $form = new BuscaForm();
        $form->setAttribute('method', 'post');
        //$form->setAttribute('action', $helperUrl('site-noticia'));

        // seta a action do form e o placeholder dinamicamente
        /*if ($baseRouteName == 'site-produto') {*/

            $form->get('search')->setAttribute('placeholder', 'Buscar');

        /*} elseif ($baseRouteName == 'site-servico') {

            $form->get('search')->setAttribute('placeholder', 'Buscar por serviços');

        } elseif ($baseRouteName == 'site-noticia') {

                $form->get('search')->setAttribute('placeholder', 'Buscar por notícias');

        } elseif ($baseRouteName == 'site-anunciantes') {

            $form->get('search')->setAttribute('placeholder', 'Buscar por lojas');

        } else {

            $form->get('search')->setAttribute('placeholder', 'Produtos e serviços');
            $baseRouteName = 'site-produtoservico';

        }*/

        // verifica se o submit está vindo do formulário
    	if(isset($data['submit_search'])) {

	    	if ($request->isPost()) {

	    		$form->setData($request->getPost());

                //$e->getTarget()->redirect()->toRoute($baseRouteName.'/filter', array('busca' => $data['search']));
                $e->getTarget()->redirect()->toRoute('site-servico/filter', array('busca' => $data['search']));

	    	}
    	}

    	$viewModel = $e->getViewModel();
    	$viewModel->setVariables( array(
    			'form'      => $form
    	) );
    }
}