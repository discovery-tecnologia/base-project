<?php
namespace Site\Form;

use Zend\Form\Form;
use Zend\Form\Element\Text;
use Zend\Form\Element\Button;


class Busca extends Form {

	
	public function __construct($name = null) {
		
	parent::__construct('form_search');
			
		$this->setAttribute('method', 'post');
		
		// search ********************** SEARCH (TEXT) ***************************
		$search = new Text('search');
		$search
			->setLabel('Pesquisa: ')
			->setLabelAttributes(array('class'=>'control-label'))
			->setAttributes(array(
				//'class'			=> 'tool_tip',
				//'data-placement'=> 'left',
				'placeholder'	=>'O que procura?',
			   	'title'			=>'Busca',
				//'id'			=>'search',
				'maxlength'		=> '100'
			));
		$this->add($search);

        // submit_login **********BUTTON (SUBMIT) ********************************
        $button = new Button('submit_search');
        $button->setAttributes (array(
                'type' => 'submit'))
                //'id' => 'submit_search'))
            ->setValue('submit_search');
        $this->add($button);
		
	}
	
	
}