<?php
namespace Site\Form;

use Zend\Form\Form;
use Site\Form\InputFilter\ContatoFilter;
use Zend\Form\Element\Text;
use Zend\Form\Element\Select;
use Zend\Form\Element\Textarea;
use Zend\Form\Element\Captcha;
use Zend\Captcha\Image as CaptchaImage;
use Zend\Form\Element\Csrf;


class Contato extends Form {


    public function init() {

        parent::__construct('contato');

        $this->setAttributes(array(
            'method' => 'post',
            'class'  => 'form form-horizontal'
        ));

        $this->setInputFilter(new ContatoFilter());

        // nome ************************** NOME (TEXT) *************************
        $nome = new Text('nome');
        $nome->setLabel ( 'Nome: ' )->setLabelAttributes ( array (
            'class' => 'control-label required'
        ) )->setAttributes ( array (
                'class' => 'tool_tip',
                'data-placement' => 'left',
                'placeholder' => 'Nome',
                'title' => 'Seu nome',
                'id' => 'nome',
                'maxlength' => '100'
            ) );
        $this->add ( $nome );

        // telefone ************ TELEFONE (TEXT) *******************************
        $telefone = new Text('telefone');
        $telefone->setLabel('Telefone: ')->setLabelAttributes ( array (
            'class' => 'control-label'
        ) )->setAttributes ( array (
                'class'         => 'tool_tip',
                'data-placement'=> 'left',
                'placeholder'   => 'Telefone:',
                'title'         => 'Telefone com DDD.',
                'id'            => 'telefone'
            ) );
        $this->add($telefone);

        // email *******************EMAIL (TEXT) *******************************
        $email = new Text('email');
        $email->setLabel('Email: ')->setLabelAttributes ( array (
            'class' => 'control-label required'
        ))->setAttributes(array(
                'class'			=> 'tool_tip',
                'data-placement'=> 'left',
                'placeholder'	=> 'Email:',
                'title'			=> 'Email que será utilizado para nossa comunicação',
                'id'            => 'email',
                'maxlength'		=> '255'
            ));
        $this->add($email);

        // assunto ***************** ASSUNTO (TEXT) ***************************
        $assunto = new Text('assunto');
        $assunto->setLabel('Assunto: ')->setLabelAttributes ( array (
            'class' => 'control-label required'
        ))->setAttributes(array(
                'class'			=> 'tool_tip',
                'data-placement'=> 'left',
                'placeholder'	=>'Assunto:',
                'title'			=>'Assunto da mensagem',
                'id'			=>'assunto',
                'maxlength'		=> '100'
            ));
        $this->add($assunto);

        // mensagem *************** MENSAGEM (TEXTAREA) ***********************
        $mensagem = new Textarea('mensagem');
        $mensagem->setLabel('Mensagem: ')
            ->setLabelAttributes(array('class'=>'control-label required'))
            ->setAttributes(array(
                'placeholder'	=>'Sua mensagem',
                'class'			=> 'tool_tip',
                'data-placement'=> 'left',
                'placeholder'	=>'Mensagem:',
                'title'			=>'Sua mensagem',
                'id'			=>'mensagem'));
        $this->add($mensagem);

        // captcha ******************* CaptchaImage ****************************
        $dir = './public_html';

        $captchaImage = new CaptchaImage( array (
                'font' => $dir . '/fonts/Capture it 2.ttf',
                'width' => 275,
                'height' => 80,
                'dotNoiseLevel' => 40,
                'lineNoiseLevel' => 2 )
        );

        // captcha ******************* CAPTCHA (Captcha) ***********************
        $captcha = new Captcha('captcha');
        $captcha->setOptions(array (
            'label' => 'Verifique o código: ',
            'captcha' => $captchaImage))
            ->setLabelAttributes(array('class'=>'control-label required'))
            ->setAttributes(array(
                'class'			=> 'tool_tip',
                'data-placement'=> 'left',
                'placeholder'	=> 'Código:',
                'title'			=> 'Código de verificação',
                'id'			=> 'captcha',
                'maxlength'		=> '50'
            ));
        $this->add($captcha);

        // security_contato *********** CSRF (Csrf) ****************************
        $csrf = new Csrf('security_contato');
        $csrf->setOptions( array (
            'csrf_options'  =>  array (
                'timeout'  =>  900
            )) );
        $this->add($csrf);
    }

}