<?php
namespace Site\Form\InputFilter;

use Zend\InputFilter\InputFilter;
use Zend\Validator\StringLength;

class ContatoFilter extends InputFilter {
	
	public function __construct() {
		
		
		// nome ################### NOME (TEXT) ##############################
		$this->add(array(
			'name' => 'nome',
			'required' => true,
			'filters' => array(
				array('name' => 'StripTags'),
				array('name' => 'StringTrim'),
			),
			'validators' => array(
				array(
					'name'    => 'StringLength',
					'break_chain_on_failure' => true,
					'options' => array(
						'encoding' => 'UTF-8',
                        'min'      => 2,
						'max'      => 100
					),
				),
				array(
					'name' => 'NotEmpty'
				)
			)
		));

        // email ####################### EMAIL (TEXT) ##########################
        $this->add(array(
            'name' => 'email',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'EmailAddress',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min'      => 5,
                        'max'      => 255
                    )
                )
            )
        ));
		
		// assunto ################## ASSUNTO (TEXT) #########################
        $this->add(array(
            'name' => 'assunto',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min'      => 2,
                        'max'      => 100
                    ),
                ),
                array(
                    'name' => 'NotEmpty'
                )
            )
        ));
		
		// mensagem ################ MENSAGEM (TEXTAREA) ####################
		$this->add(array(
			'name' => 'mensagem',
			'required' => true,
			'filters' => array(
				array('name' => 'StripTags'),
				array('name' => 'StringTrim'),
			),
			'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min'      => 2,
                        'max'      => 600
                    ),
                ),
                array(
                    'name' => 'NotEmpty'
                )
			)
		));
		
	}
	
}