<?php
/**
 * Class AlterarSenhaFilterFactory
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Site\Form\InputFilter\Factory;

use Servidor\Entity\Entities;
use Site\Form\InputFilter\AlterarSenhaFilter;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AlterarSenhaFilterFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocator = $serviceLocator->getServiceLocator();
        $objectManager   = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $objectRepository= $objectManager->getRepository(Entities::ENTITY_CLIENTE);

        $clienteService = $serviceLocator->get('Site\Service\Cliente');
        $loja = $clienteService->getSite();

        $authService = $serviceLocator->get('Zend\Authentication\AuthenticationService');
        $cliente = $authService->getIdentity();

        return new AlterarSenhaFilter( $objectRepository, $loja, $cliente );
    }
} 