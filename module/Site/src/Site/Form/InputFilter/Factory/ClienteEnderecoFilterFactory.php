<?php
namespace Site\Form\InputFilter\Factory;

use Servidor\Entity\Entities;
use Site\Form\InputFilter\ClienteEnderecoFilter;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ClienteEnderecoFilterFactory
 * @author Andre Luiz Haag
 *
 */
class ClienteEnderecoFilterFactory implements FactoryInterface {


    /**
     * (non-PHPdoc)
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService( ServiceLocatorInterface $serviceLocator ) {

        $serviceLocator = $serviceLocator->getServiceLocator();

        $em = $serviceLocator->get('Doctrine\ORM\EntityManager');

        $repEndereco = $em->getRepository(Entities::ENTITY_ENDERECO);

        return new ClienteEnderecoFilter($repEndereco);

    }

}