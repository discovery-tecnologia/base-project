<?php
/**
 * Class ClienteFilterFactory
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Site\Form\InputFilter\Factory;


use Servidor\Entity\Entities;
use Site\Form\InputFilter\ClienteFilter;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ClienteFilterFactory implements FactoryInterface {

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocator = $serviceLocator->getServiceLocator();
        $objectManager   = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $objectRepository= $objectManager->getRepository(Entities::ENTITY_CLIENTE);

        $clienteService = $serviceLocator->get('Site\Service\Cliente');
        $loja = $clienteService->getSite();

        $router  = $serviceLocator->get('router');
        $request = $serviceLocator->get('request');
        $routeMatch = $router->match($request);

        return new ClienteFilter( $objectRepository, $loja, $routeMatch->getParam('id') );
    }

} 