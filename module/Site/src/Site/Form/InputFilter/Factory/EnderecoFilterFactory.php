<?php
/**
 * Class EnderecoFilterFactory
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Site\Form\InputFilter\Factory;

use Servidor\Entity\Entities;
use Site\Form\InputFilter\EnderecoFilter;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class EnderecoFilterFactory implements FactoryInterface
{

    /**
     * (non-PHPdoc)
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService( ServiceLocatorInterface $serviceLocator ) {

        $serviceLocator = $serviceLocator->getServiceLocator();
        $em = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $repEndereco = $em->getRepository(Entities::ENTITY_ENDERECO);

        return new EnderecoFilter($repEndereco);
    }

} 