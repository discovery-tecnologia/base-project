<?php
/**
 * Class AlterarDadosFilterFactory
 *
 * @author DiegoWagner <diegowagner4@gmail.com>
 */
namespace Site\Form\InputFilter\Factory;

use Servidor\Entity\Entities;
use Site\Form\InputFilter\MeusDadosFilter;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MeusDadosFilterFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocator = $serviceLocator->getServiceLocator();
        $objectManager   = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $objectRepository= $objectManager->getRepository(Entities::ENTITY_CLIENTE);

        $authService = $serviceLocator->get('Zend\Authentication\AuthenticationService');
        $exclude = null;
        if ($authService->hasIdentity()) {
            $exclude = $authService->getIdentity()->getId();
        }

        $clienteService = $serviceLocator->get('Site\Service\Cliente');
        $loja = $clienteService->getSite();

        return new MeusDadosFilter( $objectRepository, $loja, $exclude );
    }
} 