<?php
/**
 * Class PessoaJuridicaFilterFactory
 *
 * @author Diego Wagner <desenvolvimento@discoverytecnologia.com.br>
 * http://www.discoverytecnologia.com.br
 */
namespace Site\Form\InputFilter\Factory;

use Servidor\Entity\Entities;
use Site\Form\InputFilter\PessoaJuridicaFilter;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PessoaJuridicaFilterFactory implements FactoryInterface {
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocator = $serviceLocator->getServiceLocator();
        $objectManager   = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $objectRepository= $objectManager->getRepository(Entities::ENTITY_PESSOA_JURIDICA);

        $router  = $serviceLocator->get('router');
        $request = $serviceLocator->get('request');
        $routeMatch = $router->match($request);

        return new PessoaJuridicaFilter( $objectRepository, $routeMatch->getParam('id') );
    }

} 