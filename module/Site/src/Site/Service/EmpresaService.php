<?php
namespace Site\Service;

use Base\Service\AbstractService;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;


class EmpresaService extends AbstractService {


    public function __construct(ObjectManager $objectManager, ObjectRepository $objectRepository)
    {
        parent::__construct($objectManager, $objectRepository);
    }



    public function getInstance() {

        return $this->find(1);

    }

}
