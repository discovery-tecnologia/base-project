<?php
namespace Site\Service\Factory;

use Site\Service\EmpresaService;

use Servidor\Entity\Entities;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class Factory
 * @author André Luiz Haag
 *
 */
class EmpresaServiceFactory implements FactoryInterface {


    /**
     * (non-PHPdoc)
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService( ServiceLocatorInterface $serviceLocator ) {

        $objectManager    = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $objectRepository = $objectManager->getRepository(Entities::ENTITY_EMPRESA);

        return new EmpresaService( $objectManager, $objectRepository );
    }
}