<?php
namespace Site\Service\Factory;

use Site\Service\MailService;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class Factory
 * @author André Luiz Haag
 *
 */
class MailServiceFactory implements FactoryInterface {
	
	
	/**
	 * (non-PHPdoc)
	 * @see \Zend\ServiceManager\FactoryInterface::createService()
	 */
	public function createService( ServiceLocatorInterface $serviceLocator ) {

        $smtpTransport = $serviceLocator->get('Base\Mail\Transport');
        $view          = $serviceLocator->get('View');

        return new MailService( $smtpTransport, $view );

	}
}