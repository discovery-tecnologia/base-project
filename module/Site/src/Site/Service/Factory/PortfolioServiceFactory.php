<?php
namespace Site\Service\Factory;

use Site\Service\PortfolioService;

use Servidor\Entity\Entities;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class Factory
 * @author André Luiz Haag
 *
 */
class PortfolioServiceFactory implements FactoryInterface {


    /**
     * (non-PHPdoc)
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService( ServiceLocatorInterface $serviceLocator ) {

        $objectManager          = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $objectRepository       = $objectManager->getRepository(Entities::ENTITY_PORTFOLIO);

        return new PortfolioService( $objectManager, $objectRepository);
    }
}