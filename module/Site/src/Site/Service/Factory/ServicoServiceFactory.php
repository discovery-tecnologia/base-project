<?php
namespace Site\Service\Factory;

use Site\Service\ServicoService;

use Servidor\Entity\Entities;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class Factory
 * @author André Luiz Haag
 *
 */
class ServicoServiceFactory implements FactoryInterface {


    /**
     * (non-PHPdoc)
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService( ServiceLocatorInterface $serviceLocator ) {

        $objectManager          = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $objectRepository       = $objectManager->getRepository(Entities::ENTITY_SERVICO);

        return new ServicoService( $objectManager, $objectRepository);
    }
}