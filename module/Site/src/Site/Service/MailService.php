<?php
namespace Site\Service;

use Base\Service\AbstractSiteService;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Base\Mail\Mail;


class MailService  {
	
	
	/**
	 * 
	 * @var SmtpTransport
	 */
	protected $transport;
	
	
	/**
	 * 
	 * @var ViewModel
	 */
	protected $view;
	
	
	/**
	 * __construct()
	 * @param SmtpTransport $transport
	 * @param ViewModel $view
	 */
	public function __construct( SmtpTransport $transport, $view) {

		$this->transport   = $transport;
		$this->view        = $view;
	}


    /**
     * * @method sendMail()
     * Serviço para o envio de emails.
     *
     * @param $page
     * @param array $data
     * @param array $destinatarios
     * @param $subject
     * @return bool
     */
    public function sendMail($page, array $data, array $destinatarios, $subject) {

        $mail = new Mail( $this->transport, $this->view, $page);
        $mail->setSubject($subject)
            ->setTo($destinatarios)
            ->setData($data)
            ->prepare()
            ->send();   // envia o email.

        return true;

	}
}