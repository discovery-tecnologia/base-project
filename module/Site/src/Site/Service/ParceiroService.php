<?php
namespace Site\Service;

use Base\Service\AbstractService;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;

class ParceiroService extends AbstractService {


    public function __construct(ObjectManager $objectManager, ObjectRepository $objectRepository)
    {
        parent::__construct($objectManager, $objectRepository);
    }

    public function search($string = '', $tipo = null) {
        return $this->repository->search($string, $tipo);
    }
}