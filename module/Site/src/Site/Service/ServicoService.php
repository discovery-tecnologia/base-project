<?php
namespace Site\Service;

use Base\Service\AbstractService;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;

class ServicoService extends AbstractService {


    public function __construct(ObjectManager $objectManager, ObjectRepository $objectRepository)
    {
        parent::__construct($objectManager, $objectRepository);
    }

    public function search($string = '') {
        return $this->repository->search($string);
    }
}