<?php 
namespace Site\View\Helper\Factory;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;

use Site\View\Helper\HEmpresa;


class HEmpresaFactory implements FactoryInterface {


    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator) {

        $serviceLocator = $serviceLocator->getServiceLocator();
        $empresaService    = $serviceLocator->get('Site\Service\Empresa');

        return new HEmpresa( $empresaService );

    }
       
}