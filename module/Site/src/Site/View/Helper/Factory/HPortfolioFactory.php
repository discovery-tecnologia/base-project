<?php 
namespace Site\View\Helper\Factory;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;

use Site\View\Helper\HPortfolio;


class HPortfolioFactory implements FactoryInterface {


    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator) {

        $serviceLocator = $serviceLocator->getServiceLocator();
        $service    = $serviceLocator->get('Site\Service\Portfolio');

        return new HPortfolio( $service );

    }
       
}