<?php 
namespace Site\View\Helper\Factory;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;

use Site\View\Helper\HServico;


class HServicoFactory implements FactoryInterface {


    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator) {

        $serviceLocator = $serviceLocator->getServiceLocator();
        $produtoService    = $serviceLocator->get('Site\Service\Servico');

        return new HServico( $produtoService );

    }
       
}