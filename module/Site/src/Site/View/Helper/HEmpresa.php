<?php 
namespace Site\View\Helper;

use Site\Service\EmpresaService;
use Zend\View\Helper\AbstractHelper;

class HEmpresa extends AbstractHelper {

    /**
     * @var EmpresaService
     */
    protected $service;



    /**
     * Dependency Injection
     * @param SiteService $siteService
     */
    public function __construct( EmpresaService $empresaService ) {

        $this->service = $empresaService;

    }


    /**
     * @return mixed
     */
    public function __invoke() {

        return $this->service->getInstance();

    }
}