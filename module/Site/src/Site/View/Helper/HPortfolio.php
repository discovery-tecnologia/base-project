<?php 
namespace Site\View\Helper;

use Site\Service\PortfolioService;
use Zend\View\Helper\AbstractHelper;

class HPortfolio extends AbstractHelper {

    /**
     * @var PortfolioService
     */
    protected $service;



    /**
     * Dependency Injection
     * @param PortfolioService $potfolioService
     */
    public function __construct( PortfolioService $portfolioService ) {

        $this->service = $portfolioService;

    }


    /**
     * @return mixed
     */
    public function __invoke() {

        return $this->service->search();

    }
}