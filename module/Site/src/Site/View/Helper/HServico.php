<?php 
namespace Site\View\Helper;

use Site\Service\ServicoService;
use Zend\View\Helper\AbstractHelper;

class HServico extends AbstractHelper {

    /**
     * @var ProdutoService
     */
    protected $service;



    /**
     * Dependency Injection
     * @param SiteService $siteService
     */
    public function __construct( ServicoService $servicoService ) {

        $this->service = $servicoService;

    }


    /**
     * @return mixed
     */
    public function __invoke() {

        return $this->service->search();

    }
}