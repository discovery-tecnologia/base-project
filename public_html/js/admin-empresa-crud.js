tinymce.init({
    selector: "textarea.jmce",
    theme: "modern",
    entity_encoding : "ascii",
    language : "pt_BR",
    height: 300,
    browser_spellcheck : true,
    fontsize_formats: "8px 10px 12px 14px 16px 18px 24px 36px",

    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor responsivefilemanager youtube"
    ],

    content_css: '../../../css/tinymce.css',

    toolbar1: "insertfile undo redo | styleselect | bold italic | forecolor backcolor emoticons | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager",
    toolbar2: "media youtube | preview code",
    image_advtab: true,

    external_filemanager_path:"/plugins/filemanager/",
    filemanager_title:"Gerenciador de Arquivos",
    external_plugins: {"filemanager" : "/plugins/filemanager/plugin.min.js"}

});

$(function() {
    //bootstrap WYSIHTML5 - text editor
    $(".textarea-editor").wysihtml5(
        {
            "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
            "emphasis": true, //Italics, bold, etc. Default true
            "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
            "html": false, //Button which allows you to edit the generated HTML. Default false
            "link": true, //Button to insert a link. Default true
            "image": true, //Button to insert an image. Default true,
            "blockquote": false, //Blockquote
            //"size": <buttonsize>, //default: none, other options are xs, sm, lg
            locale: "pt-BR",
            "fa": true
        }
    );
});

$(document).ready( function() {
    var formChanged = false;

    $('form :input').change(function(){
        formChanged = true;
    });

    window.onbeforeunload = function (evt) {
        if(formChanged){
            var message = 'As alterações ainda não foram salvas!';
            if (typeof evt == 'undefined') {
                evt = window.event;
            }
            if (evt) {
                evt.returnValue = message;
            }
            return message;
        }
    };

    //bootstrap tolltips
    $('.tool_tip').tooltip({
        placement: 'bottom'
    });

    $('#save').click(function() {
        formChanged = false;
        $('form[name="cadastro-empresa"]').submit();
    });

    $('label[for=quemSomos]').click(function(){
        tinyMCE.get('empresa[quemSomos]').focus();
    });

    jQuery.validator.addMethod("requiredMCE", function(value, element) {
        return tinymceLenght(tinyMCE.get('empresa[quemSomos]').getContent());
    }, "Este campo é obrigatório.");

    $('form[name="cadastro-empresa"]').submit(function() {

    }).validate({
        ignore: '',
        ignore: ":hidden:not(textarea)",
        // Define as regras
        rules:{
            'empresa[nome]':{
                required: true,
                minlength: 2
            },
            'empresa[slogan]':{
                required: true,
                minlength: 2
            },
            'empresa[quemSomos]':{
                requiredMCE: true
            },
            'empresa[textoContato]':{
                requiredMCE: true
            },
            'empresa[textoRodape]':{
                required: true
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        errorPlacement: function(label, element) {
            // posiciona o label de erro
            if (element.is("input[type=checkbox]")){
                label.insertAfter($(element).closest('div.controls'));
            } else {
                label.insertAfter(element);
            }
        },
        success: function(element) {
            element
                .text('').addClass('valid')
                .closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        invalidHandler: function(form, validator) {

            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
                alert("Atenção, preencha corretamente todos os campos obrigatórios.");
            }
        }

    });

});// document.read

function tinymceLenght(content){
    if(content == ""){
        return false;
    }
    content = content.replace(/(<([^>]+)>)/ig, ''); // remove tags html
    content = content.replace(/&nbsp;/gi, '');		// remove quebras de linha
    content = content.replace(/^\s+|\s+$/g, '');	// remove espa�os em branco
    if(content == ""){
        return false;
    }
    return content.length;
}