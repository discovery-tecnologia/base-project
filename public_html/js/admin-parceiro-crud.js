$(document).ready( function() {

	var formChanged = false;
	
	$('form :input').change(function(){
		   formChanged = true;
	});
	
	window.onbeforeunload = function (evt) {
		if(formChanged){
			var message = 'As alterações ainda não foram salvas!';
			if (typeof evt == 'undefined') {
				evt = window.event;
			}
			if (evt) {
				evt.returnValue = message;
			}
		  return message;
		}
	};

	//bootstrap tolltips
	$('.tool_tip').tooltip({
		placement: 'bottom'
	});

	$('#save').click(function() {    
		formChanged = false;
		$('form[name="cadastro-parceiro"]').submit();
	});

	 $('form[name="cadastro-parceiro"]').submit(function() {
		
	 }).validate({ 	
			ignore: '',
     	// Define as regras
        rules:{
            'parceiro[nome]':{
                required: true, minlength: 2
            },
            'parceiro[link]':{
                required: true, minlength: 5
            }
        },
        highlight: function(element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        errorPlacement: function(label, element) {
            // posiciona o label de erro
            if (element.is("input[type=checkbox]") || element.is("input[type=radio]")){
                label.insertAfter($(element).closest('div.controls'));
            } else {
                label.insertAfter(element);
            }
        },
        success: function(element) {
            element
             .text('OK!').addClass('valid')
             .closest('.control-group').removeClass('error').addClass('success');
        },
        invalidHandler: function(form, validator) {

            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
                alert("Atenção, preencha corretamente todos os campos obrigatórios.");
            }
        }
    });

});// document.read