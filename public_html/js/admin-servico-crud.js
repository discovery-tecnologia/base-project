tinymce.init({
    selector: "textarea.jmce",
    theme: "modern",
    entity_encoding : "ascii",
    language : "pt_BR",
    height: 300,
    browser_spellcheck : true,
    fontsize_formats: "8px 10px 12px 14px 16px 18px 24px 36px",

    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor responsivefilemanager youtube"
    ],

    content_css: '../../../css/tinymce.css',

    toolbar1: "insertfile undo redo | styleselect | bold italic | forecolor backcolor emoticons | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager",
    toolbar2: "media youtube | preview code",
    image_advtab: true,

    external_filemanager_path:"/plugins/filemanager/",
    filemanager_title:"Gerenciador de Arquivos",
    external_plugins: {"filemanager" : "/plugins/filemanager/plugin.min.js"}

});


$(document).ready( function() {

	var formChanged = false;
	
	$('form :input').change(function(){
		   formChanged = true;
	});
	
	window.onbeforeunload = function (evt) {
		if(formChanged){
			var message = 'As alterações ainda não foram salvas!';
			if (typeof evt == 'undefined') {
				evt = window.event;
			}
			if (evt) {
				evt.returnValue = message;
			}
		  return message;
		}
	};

	//bootstrap tolltips
	$('.tool_tip').tooltip({
		placement: 'bottom'
	});

    $('.money').maskMoney({
        prefix: 'R$ ',
        thousands: '.',
        decimal: ','
    });

	$('#save').click(function() {    
		formChanged = false;
		$('form[name="cadastro-servico"]').submit();
	});
	
	$('label[for=descricao]').click(function(){
		tinyMCE.get('servico[descricao]').focus();
	});

	jQuery.validator.addMethod("requiredMCE", function(value, element) {
		return tinymceLenght(tinyMCE.get('servico[descricao]').getContent());
	}, "Este campo é obrigatório.");

	 $('form[name="cadastro-servico"]').submit(function() {
		
	 }).validate({ 	
			ignore: '',
     	// Define as regras
        rules:{
            'servico[nome]':{
                required: true, minlength: 2
            },
            'servico[descricao]':{
            	requiredMCE: true
            },
            'servico[categoria]':{
                required: true
            },
            'servico[subCategoria]':{
                required: true
            }
        },
        highlight: function(element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        errorPlacement: function(label, element) {
            // posiciona o label de erro
            if (element.is("input[type=checkbox]") || element.is("input[type=radio]")){
                label.insertAfter($(element).closest('div.controls'));
            } else {
                label.insertAfter(element);
            }
        },
        success: function(element) {
            element
             .text('OK!').addClass('valid')
             .closest('.control-group').removeClass('error').addClass('success');
        },
        invalidHandler: function(form, validator) {

            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
                alert("Atenção, preencha corretamente todos os campos obrigatórios.");
            }
        }
    });

});// document.read

function tinymceLenght(content){
	if(content == ""){
		return false;
	}
	content = content.replace(/(<([^>]+)>)/ig, ''); // remove tags html
	content = content.replace(/&nbsp;/gi, '');		// remove quebras de linha
	content = content.replace(/^\s+|\s+$/g, '');	// remove espaços em branco
	if(content == ""){
		return false;
	}
	return content.length;
}