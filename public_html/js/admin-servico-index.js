var oTable;
var giRedraw = false;

$(document).ready(function() {
	
	// click na linha do grid
	$(".datagrid tbody").click(function(event) {
		
		if($(event.target.parentNode).hasClass('selected')){
			$(event.target.parentNode).removeClass('selected');
    	}else{    		
    		$(event.target.parentNode).addClass('selected');
    	}
		
	});
	
	// duplo tap na linha do grid (especifico para tablets e afins)
	$(".datagrid tbody").doubletap(function() {
		dataTableDblClick();		
	});

	// duplo click na linha do grid
	$(".datagrid tbody").dblclick(function() {
		dataTableDblClick();
	});
	
	// DATAGRID
	oTable = $(".datagrid").dataTable({
        "bStateSave": false,
		"aLengthMenu": [[1,10, 20, 50, 100, -1], [1,10, 20, 50, 100, "Todos"]],
        "sPaginationType": "full_numbers",
        "aaSorting": [[ 0, "asc" ]],
        "aoColumnDefs": [
                        { "bSearchable": true, "bVisible": true, "aTargets": [ 0 ] },
                        { "bSearchable": true, "bVisible": true, "aTargets": [ 1 ] },
                        { "bSearchable": false, "bVisible": true, "aTargets": [ 2 ] }
                    ]        
    }).columnFilter({
    	sPlaceHolder: "head:after",
    	aoColumns: [ null,
    	             { type: "text" },
    	             { type: "text" },
    	             { type: "text" }
		]
	});
    
    // EDIT
    $('a#edit').click(function() {    	
    	edit();    	
    });

});// document.read

function edit() {   		

	var anSelected = fnGetSelected( oTable );
	
	var nTds = $('td', anSelected);
    var id = $(nTds[0]).text();		
					
    if (id){	
        var host = window.location.toString();
    	$(window.location).attr('href', host + '/edit/' + id);	
    	ev.stopPropagation();
	} else {
		
		$.msgBox({
		    title:"Atenção",
		    content:"Nenhum registro selecionado!",
		    type:"info"
		});
		
	}	
}

function fnGetSelected( oTableLocal ) {
	
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ ) {
		
		if ( $(aTrs[i]).hasClass('selected') ) {
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}

function dataTableDblClick() {
	
	$(oTable.fnSettings().aoData).each(function (){
		$(this.nTr).removeClass('selected');
	});
	    		
	$(event.target.parentNode).addClass('selected');
	
	edit();
}