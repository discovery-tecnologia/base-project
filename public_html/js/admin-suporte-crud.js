tinymce.init({

    selector: "textarea.jmce",
    theme: "modern",
    language : "pt_BR",
    height: 300,
    browser_spellcheck : true,
    fontsize_formats: "8px 10px 12px 14px 16px 18px 24px 36px",

    plugins: [
        "autolink link charmap print preview",
        "searchreplace wordcount code",
        "insertdatetime save directionality"
    ],

    content_css: '../../../css/tinymce.css',

    toolbar1: "undo redo | bold italic | link | code"

});


$(document).ready( function() {
    var formChanged = false;

    $('form :input').change(function(){
        formChanged = true;
    });

    window.onbeforeunload = function (evt) {
        if(formChanged){
            var message = 'As alterações ainda não foram salvas!';
            if (typeof evt == 'undefined') {
                evt = window.event;
            }
            if (evt) {
                evt.returnValue = message;
            }
            return message;
        }
    };


    //bootstrap tolltips
    $('.tool_tip').tooltip({
        placement: 'bottom'
    });

    $('#save').click(function() {
        formChanged = false;
        $('form[name="suporte"]').submit();
    });

    $('label[for=mensagem]').click(function(){
        tinyMCE.get('mensagem').focus();
    });

    jQuery.validator.addMethod("requiredMCE", function(value, element) {
        return tinymceLenght(tinyMCE.get('mensagem').getContent());
    }, "Este campo é obrigatório.");

    $('form[name="suporte"]').submit(function() {

    }).validate({
            ignore: '',
            // Define as regras
            rules:{
                'assunto':{
                    required: true,
                    minlength: 2
                },
                'mensagem':{
                    requiredMCE: true
                }
            },
            // Define as mensagens de erro para cada regra
            messages:{
                'assunto':{
                    required: "Assunto é obrigatório!",
                    minlength: "O assunto deve conter, no mínimo 2 caracteres!"
                },
                'mensagem':{
                    requiredMCE: "Mensagem é obrigatória!"
                }
            },
            highlight: function(element) {
                $(element).closest('.control-group').removeClass('success').addClass('error');
            },
            success: function(element) {
                element
                    .text('OK!').addClass('valid')
                    .closest('.control-group').removeClass('error').addClass('success');
            },
            invalidHandler: function(form, validator) {

                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                    alert("Atenção, preencha corretamente todos os campos obrigatórios.");
                }
            }

        });

});// document.read

function tinymceLenght(content){
    if(content == ""){
        return false;
    }
    content = content.replace(/(<([^>]+)>)/ig, ''); // remove tags html
    content = content.replace(/&nbsp;/gi, '');		// remove quebras de linha
    content = content.replace(/^\s+|\s+$/g, '');	// remove espaços em branco
    if(content == ""){
        return false;
    }
    return content.length;
}