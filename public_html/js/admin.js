$(document).ready(function() {

    // Themas **************************************************
	if($.cookie("theme")) {
		$('head').append('<link rel="stylesheet" href="/css/'+ $.cookie("theme") +'" type="text/css">');
	} else {
		$('head').append('<link rel="stylesheet" href="/css/admin-theme-verde.min.css" type="text/css">');
	}
	
    $("span.theme").click(function() {
    	$('link[href^="/css/admin-theme"]').attr("href", "/css/"+$(this).attr('id')+".min.css");
    	$.cookie("theme",$(this).attr('id')+".min.css", {expires: 365, path: '/'});
        return false;
    });
    
 // Configurações **********************************************
	if($.cookie("config_position")) {		
		var cookie = $.cookie("config_position");		
		$('span.' + cookie).addClass('active');    	
	} else {	
		$('span.fixo').addClass('active');	
	}
	
	if($.cookie("config_tamanho")) {		
		var cookie = $.cookie("config_tamanho");		
		$('span.' + cookie).addClass('active');		
		
		if(cookie == 'compacto') {
			$('div#site').addClass('menu_compacto');
		}    	
    	
	}else{	
		$('span.normal').addClass('active');	
	}
	
	$("span.config").click(function() {		
		if($(this).hasClass('active')) {			
			return false;			
		}else{		
			$(this).addClass('active');			
			if($(this).hasClass('fixo')) {			
				$('span.rolante').removeClass('active');
				$.cookie('config_position','fixo', {expires: 365, path: '/'});				
			} else if ($(this).hasClass('rolante')) {			
				$('span.fixo').removeClass('active');
				$.cookie('config_position','rolante', {expires: 365, path: '/'});				
			} else if ($(this).hasClass('normal')) {			
				$('span.compacto').removeClass('active');
				$('div#site').removeClass('menu_compacto');
				$.cookie('config_tamanho','normal', {expires: 365, path: '/'});				
			} else if ($(this).hasClass('compacto')) {			
				$('span.normal').removeClass('active');
				$('div#site').addClass('menu_compacto');
				$.cookie('config_tamanho','compacto', {expires: 365, path: '/'});				
			}			
		}		
        return false;
    });
	
	$('span.btn.logout').click(function() {		
		
		var href = $(this).attr('rel');
		
		$.msgBox({
		    title: "Sair?",
		    content: 'Deseja realmente sair?',
		    type: "info",
		    buttons: [{ value: "Sim" }, { value: "Cancelar"}],
		    success: function (result) {
		        if (result == "Sim") {
		        	window.location.href = href;
		        }
		    }
		});		
	});
	
	// ao clicar no link #back_top
	$('#back_top').click(function(){		
		$('html, body').animate({scrollTop:0}, '500');	
	});			
	
	/**
	 * Exibe o link TOPO na tela com base na
	 * posição do scroll.
	 */	
	$('#back_top').hide();
	
	$(function () {		
		$(window).scroll(function () {
			if ($(this).scrollTop() > 300) {
				$('#back_top').fadeIn();				
			} else {
				$('#back_top').fadeOut();			
			}
		});
	});

    // DELETE
    $('a#delete').click(function() {

        var ids = "";
        var count = 0;

        // percorre as linhas selecionadas
        oTable.$("tr").filter(".selected").each(function (index, row){
            var nTds = $('td', row);
            var id = $(nTds[0]).text();

            ids+="-" + id;
            count++;
        });

        if(count>=1) {
            ids = ids.substring(1, ids.length);
            idsMsg = ids.split("-");
            idsMsg = idsMsg.join(", ");
            idsMsg = idsMsg.replace(/,\s([^,]+)$/, ' e $1');

            var msg = "";
            if(count>1)
                msg = "Deseja realmente excluir os registros <b>" + idsMsg + "</b> ?";
            else
                msg = "Deseja realmente excluir o registro <b>" + idsMsg + "</b> ?";

            $.msgBox({
                title: "Excluir?",
                content: msg,
                type: "confirm",
                buttons: [{ value: "Sim" }, { value: "Não" }, { value: "Cancelar"}],
                success: function (result) {
                    if (result == "Sim") {
                        remove(ids);
                    }
                }
            });

        }else{

            $.msgBox({
                title:"Atenção",
                content:"Nenhum registro selecionado!",
                type:"info"
            });

        }

    });
    
});// document ready
	
	
/**
 * Funcao que adiciona a classe floating a div.buttons_toolbar
 * para que via css os botões tornen-se flutuantes quando
 * o scroll rolar mais que 160px.
 */	
$(function () {
		
	$(window).scroll(function () {
		
		if ($(this).scrollTop() > 60) {
			$('.buttons_toolbar').addClass('floating');				
		} else {
			$('.buttons_toolbar').removeClass('floating');				
		}
		
		if ($(this).scrollTop() > 32 && ($.cookie("config_position") == 'fixo')) {
			$('#left').addClass('floating');				
		} else {
			$('#left').removeClass('floating');				
		}
		
	});

});

function remove(ids) {
    var host = window.location.toString();
    $(window.location).attr('href', host + '/delete/' + ids);
}