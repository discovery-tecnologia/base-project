$(document).ready(function() {

	/**
	 * Exibe o link TOPO na tela com base na
	 * posição do scroll.
	 */
	$('#back_top').hide();


    $(window).scroll(function () {

        if ($(this).scrollTop() > 300) {
            $('#back_top').fadeIn();
        } else {
            $('#back_top').fadeOut();
        }

    });


	//ao clicar no link #back_top
	$('#back_top').click(function() {

		$('html, body').animate({scrollTop:0}, '500');

	});



    $('.menu_toogle').click(function() {

        if ( $(this).hasClass('active') ) {
            $(this).removeClass('active');
            toogleMenu(false);
        } else {
            $('.search_toogle').removeClass('active');
            $(this).addClass('active');
            toogleMenu(true);
            toogleSearch(false);
        }

    });

    $('.search_toogle').click(function() {

        if ( $(this).hasClass('active') ) {
            $(this).removeClass('active');
            toogleSearch(false);
        } else {
            $('.menu_toogle').removeClass('active');
            $(this).addClass('active');
            toogleMenu(false);
            toogleSearch(true);
        }

    });

});

function toogleMenu(active) {

    if ( active == true ) {
        $('ul.menu').addClass('aberto');
    } else {
        $('ul.menu').removeClass('aberto');
    }
}

function toogleSearch(active) {

    if ( active == true ) {
        $('div.search').addClass('aberto');
    } else {
        $('div.search').removeClass('aberto');
    }
}