/*
*mysql -u root
*CREATE USER 'fibraoptic6'@'localhost' IDENTIFIED BY '741qaz';
*GRANT ALL PRIVILEGES ON *.* TO 'fibraoptic6'@'localhost' WITH GRANT OPTION;
*/
DROP DATABASE IF EXISTS `silva_fundacoes`;
-- -----------------------------------------------------
-- Schema silva_fundacoes
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `silva_fundacoes` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `silva_fundacoes` ;

-- -----------------------------------------------------
-- Table `silva_fundacoes`.`empresa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `silva_fundacoes`.`empresa` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(70) NOT NULL,
  `slogan` VARCHAR(255) NULL DEFAULT NULL,
  `ddd` CHAR(2) NULL DEFAULT NULL,
  `telefone` VARCHAR(25) NULL DEFAULT NULL,
  `cep` VARCHAR(9) NOT NULL,
  `logradouro` VARCHAR(200) NOT NULL,
  `bairro` VARCHAR(60) NOT NULL,
  `cidade` VARCHAR(60) NOT NULL,
  `uf` CHAR(2) NOT NULL,
  `numero` VARCHAR(6) NOT NULL,
  `email` VARCHAR(70) NULL DEFAULT NULL,
  `analytics` VARCHAR(20) NULL DEFAULT NULL,
  `quem_somos` LONGTEXT NULL DEFAULT NULL,
  `mapa_localizacao` LONGTEXT NULL DEFAULT NULL,
  `facebook` VARCHAR(255) NULL DEFAULT NULL,
  `facebook_plugin` LONGTEXT NULL DEFAULT NULL,
  `twitter` VARCHAR(255) NULL DEFAULT NULL,
  `twitter_plugin` LONGTEXT NULL DEFAULT NULL,
  `youtube` VARCHAR(255) NULL DEFAULT NULL,
  `googlemais` VARCHAR(255) NULL DEFAULT NULL,
  `texto_contato` TEXT NULL DEFAULT NULL ,
  `texto_rodape` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_unicode_ci;


INSERT INTO `empresa` VALUES
  (1,
   'Silva Fundações',
   'Fundações em Geral',
   '48',
   '2866-6060',
   '88130-000',
   'Rua Geral',
   'Centro',
   'Palhoça',
   'SC',
   12,
   'contato@meusite.com.br',
   'UA-38174546-1',
   '<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>',
   '<iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt&amp;geocode=&amp;q=Silvio+Motos+-+Rua+C%C3%A2ndido+Ramos,+434+-+Capoeiras,+Florian%C3%B3polis+-+SC&amp;aq=0&amp;oq=silvio&amp;sll=-14.408749,-54.042208&amp;sspn=46.983338,86.044922&amp;t=h&amp;ie=UTF8&amp;hq=Silvio+Motos+-&amp;hnear=R.+C%C3%A2ndido+Ramos,+434+-+Capoeiras,+Florian%C3%B3polis+-+Santa+Catarina,+88090-800&amp;cid=5797198687170248975&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>',
   'www.facebook.com',
   '<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fsilviomotos%3Fref%3Dts%26fref%3Dts&amp;width=292&amp;height=315&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:292px; height:315px; background-color:#ccc;"></iframe>',
   'www.twitter.com.br',
   '',
   'www.youtube.com.br',
   'www.googleplus.com.br',
   '<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.</p>',
   '<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo.</p>');


-- -----------------------------------------------------
-- Table `silva_fundacoes`.`parceiro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `silva_fundacoes`.`parceiro` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(70) NOT NULL,
  `link` VARCHAR(200) NOT NULL,
  `is_cliente` SMALLINT(6) NOT NULL DEFAULT '0',
  `is_parceiro` SMALLINT(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_unicode_ci;

INSERT INTO `parceiro` VALUES
  (1, 'Parceiro 1', 'www.linkdaweb.com.br', 0, 1),
  (2, 'Parceiro 2', 'www.linkdaweb.com.br', 0, 1),
  (3, 'Parceiro 3', 'www.linkdaweb.com.br', 0, 1),

  (4, 'Cliente 1', 'www.linkdaweb.com.br', 1, 0),
  (5, 'Cliente 2', 'www.linkdaweb.com.br', 1, 0),
  (6, 'Cliente 3', 'www.linkdaweb.com.br', 1, 0),

  (7, 'Cliente e Parceiro 1', 'www.linkdaweb.com.br', 1, 1),
  (8, 'Cliente e Parceiro 2', 'www.linkdaweb.com.br', 1, 1),
  (9, 'Cliente e Parceiro 3', 'www.linkdaweb.com.br', 1, 1);


-- -----------------------------------------------------
-- Table `silva_fundacoes`.`portfolio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `silva_fundacoes`.`portfolio` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(100) NOT NULL,
  `descricao` LONGTEXT NOT NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_unicode_ci;

INSERT INTO `portfolio` VALUES
  (1, 'Portifolio 1', '<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.</p>'),
  (2, 'Portifolio 2', '<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.</p>'),
  (3, 'Portifolio 3', '<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.</p>'),
  (4, 'Portifolio 4', '<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.</p>'),
  (5, 'Portifolio 5', '<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.</p>');


-- -----------------------------------------------------
-- Table `silva_fundacoes`.`recurso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `silva_fundacoes`.`recurso` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `criado_em` DATETIME NOT NULL,
  `atualizado_em` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UNIQ_B2BB376454BD530C` (`nome` ASC))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_unicode_ci;

insert into recurso (`nome`, `criado_em`, `atualizado_em`) values
  ('aclcontrollerlogin', '2014-07-03 00:00:00', '2014-07-03 00:00:00'), -- 1

  ('sitecontrollerindex', '2014-07-03 00:00:00', '2014-07-03 00:00:00'), -- 2
  ('sitecontrollerfiliais', '2014-07-03 00:00:00', '2014-07-03 00:00:00'), -- 3
  ('sitecontrollerquemsomos', '2014-07-03 00:00:00', '2014-07-03 00:00:00'), -- 4
  ('sitecontrollerproduto', '2014-07-03 00:00:00', '2014-07-03 00:00:00'), -- 5
  ('sitecontrollercontato', '2014-07-03 00:00:00', '2014-07-03 00:00:00'), -- 6

  ('admincontrollerindex', '2014-07-03 00:00:00', '2014-07-03 00:00:00'), -- 7
  ('admincontrollerforbidden', '2014-07-03 00:00:00', '2014-07-03 00:00:00'), -- 8
  ('admincontrollerempresa', '2014-07-03 00:00:00', '2014-07-03 00:00:00'), -- 9
  ('admincontrollerparceiro', '2014-07-03 00:00:00', '2014-07-03 00:00:00'), -- 10
  ('admincontrollerservico', '2014-07-03 00:00:00', '2014-07-03 00:00:00'), -- 11
  ('admincontrollersuporte', '2014-07-03 00:00:00', '2014-07-03 00:00:00'), -- 12
  ('admincontrollerusuario', '2014-07-03 00:00:00', '2014-07-03 00:00:00'), -- 13
  ('admincontrollerportfolio', '2014-07-03 00:00:00', '2014-07-03 00:00:00'); -- 14


-- -----------------------------------------------------
-- Table `silva_fundacoes`.`regra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `silva_fundacoes`.`regra` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `parent_id` INT(11) NULL DEFAULT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `is_master` SMALLINT(6) NULL DEFAULT '0',
  `criado_em` DATETIME NOT NULL,
  `atualizado_em` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UNIQ_23ECD49C54BD530C` (`nome` ASC),
  UNIQUE INDEX `UNIQ_23ECD49C727ACA70` (`parent_id` ASC),
  CONSTRAINT `FK_23ECD49C727ACA70`
  FOREIGN KEY (`parent_id`)
  REFERENCES `silva_fundacoes`.`regra` (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_unicode_ci;

insert into regra (`parent_id`, `nome`, `is_master`, `criado_em`, `atualizado_em`)values
  (null, 'master', 1, '2014-07-03 00:00:00', '2014-07-03 00:00:00'),
  (null, 'visitante', 0, '2014-07-03 00:00:00', '2014-07-03 00:00:00'),
  (2, 'registrado', 0, '2014-07-03 00:00:00', '2014-07-03 00:00:00'),
  (3, 'administrador', 0, '2014-07-03 00:00:00', '2014-07-03 00:00:00');


-- -----------------------------------------------------
-- Table `silva_fundacoes`.`previlegio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `silva_fundacoes`.`previlegio` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `regra_id` INT(11) NOT NULL,
  `recurso_id` INT(11) NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `criado_em` DATETIME NOT NULL,
  `atualizado_em` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `IDX_874EFF1EDEA02ACF` (`regra_id` ASC),
  INDEX `IDX_874EFF1EE52B6C4E` (`recurso_id` ASC),
  CONSTRAINT `FK_874EFF1EE52B6C4E`
  FOREIGN KEY (`recurso_id`)
  REFERENCES `silva_fundacoes`.`recurso` (`id`),
  CONSTRAINT `FK_874EFF1EDEA02ACF`
  FOREIGN KEY (`regra_id`)
  REFERENCES `silva_fundacoes`.`regra` (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_unicode_ci;

INSERT INTO `silva_fundacoes`.`previlegio` (`regra_id`, `recurso_id`, `nome`, `criado_em`, `atualizado_em`) VALUES
-- REGRAS DE VISITANTES --
  (2,	2, 'index', '2014-07-03 00:00:00',	'2014-07-03 00:00:00'),
  (2,	3, 'index', '2014-06-08 18:32:50',	'2014-06-08 18:32:50'),
  (2,	4, 'index',	'2014-06-08 18:33:13',	'2014-06-08 18:33:13'),

-- REGRAS DE ADMINISTRADOR --
  (4,	7,	'index',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	9,	'edit',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	10,	'index',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	10,	'new',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	10,	'delete',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	10,	'edit',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	11,	'index',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	11,	'new',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	11,	'delete',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	11,	'edit',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	12,	'index',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	13,	'index',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	13,	'new',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	13,	'delete',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	13,	'alterar-senha',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	13,	'editar',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	14,	'index',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	14,	'new',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	14,	'delete',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00'),
  (4,	14,	'edit',	'2014-06-10 23:04:00',	'2014-06-10 23:04:00');


-- -----------------------------------------------------
-- Table `silva_fundacoes`.`servico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `silva_fundacoes`.`servico` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NULL DEFAULT NULL,
  `descricao` LONGTEXT NOT NULL,
  `ativo` SMALLINT(6) NOT NULL DEFAULT '1',
  `criado_em` DATETIME NOT NULL,
  `atualizado_em` DATETIME NOT NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_unicode_ci;

INSERT INTO `servico` VALUES
  (1, 'Serviço 1', '<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.</p>', 1, '2014-07-03 00:00:00',	'2014-07-03 00:00:00'),
  (2, 'Serviço 1', '<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.</p>', 1, '2014-07-03 00:00:00',	'2014-07-03 00:00:00'),
  (3, 'Serviço 1', '<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.</p>', 1, '2014-07-03 00:00:00',	'2014-07-03 00:00:00'),
  (4, 'Serviço 1', '<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.</p>', 1, '2014-07-03 00:00:00',	'2014-07-03 00:00:00'),
  (5, 'Serviço 1', '<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.</p>', 1, '2014-07-03 00:00:00',	'2014-07-03 00:00:00');


-- -----------------------------------------------------
-- Table `silva_fundacoes`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `silva_fundacoes`.`usuario` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `regra_id` INT(11) NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `senha` VARCHAR(50) NOT NULL,
  `salt` VARCHAR(255) NOT NULL,
  `email` VARCHAR(70) NOT NULL,
  `ativo` SMALLINT(6) NULL DEFAULT '1',
  `criado_em` DATETIME NOT NULL,
  `atualizado_em` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `IDX_2265B05DDEA02ACF` (`regra_id` ASC),
  CONSTRAINT `FK_2265B05DDEA02ACF`
  FOREIGN KEY (`regra_id`)
  REFERENCES `silva_fundacoes`.`regra` (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_unicode_ci;

INSERT INTO `usuario` (`nome`, `senha`, `salt`, `email`, `ativo`, `criado_em`, `atualizado_em`, `regra_id`) VALUES
  ('Desenvolvedor', 'Tg==', 'MQ+ESNZik6g=', 'desenvolvimento@discoverytecnologia.com.br', 1, '2013-09-15 17:33:50', '2013-09-15 17:33:50', 1),
  ('Diego Wagner', 'VUlj0l5r', 'MQ+ESNZik6g=', 'diegowagner4@gmail.com', 1, '2013-09-15 17:33:50', '2013-09-15 17:33:50', 4),
  ('André Luiz Haag', 'RfyrxvAd', 'Gigw5uZZTuk=', 'andreluizhaag@gmail.com', 1, '2013-09-15 17:33:50', '2013-09-15 17:33:50', 4),
  ('Rosa Maria da Silva', 'VUlj0l5r', 'MQ+ESNZik6g=', 'rosadasilva2001@hotmail.com', 1, '2013-09-15 17:33:50', '2013-09-15 17:33:50', 4);